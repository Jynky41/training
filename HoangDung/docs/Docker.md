## Docker Container
## Overview về docker container
1. Cài đặt docker (trên CentOS7)
- Tải xuống Docker sử dụng lệnh: 'curl -fsSL https://get.docker.com/ | sh'<br>
- Sau khi cài đặt hoàn tất, khởi động daemon Docker: 'sudo systemctl start docker'<br>
- Xác minh rằng Docker đang chạy bằng lệnh: 'sudo systemctl status docker'<br>
![installdocker](../image/docker1.png)
2. Cấu hình cho proxy
- Cho docker client
    - Sửa đổi file config.json ở đường dẫn /etc/docker/daemon.json <br>
        ![client](../image/docker2.png)
- Cho docker daemon
    - Thêm cấu hình proxy vào file /etc/docker/daemon.json<br>
        ![daemon](../image/docker5.png)
    - Tải lại cấu hình và khởi động lại Docker<br>
        sudo systemctl daemon-reload<br>
        sudo systemctl restart docker<br>
3. Cấu hình trusted registry trong daemon.json
- Sửa đổi file config.json ở đường dẫn /etc/docker/daemon.json <br>
    ![trusted](../image/docker3.png)
4. Hello World với Docker
- Để kiểm tra xem có thể truy cập và tải xuống các image từ Docker Hub hay không, sử dụng lệnh: 'docker run hello-world'
- Output có các thông tin như sau cho thấy Docker đang hoạt động bình thường:<br>
    ![dockerworked](../image/docker4.png)
## Các câu lệnh cơ bản
1. Docker tag
- Tạo một image tag từ 1 image đang có: 'docker image tag (tên image:tag1) (tên image:tag2)'
2. Docker run
- Đây là câu lệnh dùng để khởi tạo một container dựa vào Image có sẵn:
      'docker run (option) (image) (command) (arg...)'
    Ví dụ: 'docker run -d --name my-container nginx'
3. Docker build
- "docker build' là bước đầu để tạo ra file Dockerfile. Dockerfile được xây dựng trên một máy tính có cài đặt DockerEngine. Khi build xong, bạn sẽ có được container có chứa ứng dụng kèm với toàn bộ thư viện.
- Cú pháp cơ bản của lệnh 'docker build' là:
    'docker build (options) (Path) | (Url) | -'
4. Docker push
- Sau khi có Container, bước này sẽ đẩy Container lên Cloud và lưu trữ tại đó.
-  Cần tạo tag từ image và repositories: 'docker tag (tên image) (tên repo):(tên tag)'
-  Push lên docker hub: 'docker push (tên repo):(tên tag)'
5. Docker pull
- Trường hợp có máy tính khác muốn sử dụng Container thì máy buộc phải thực hiện Pull Container về máy và máy có cài sẵn Docker Engine. Để sử dụng Container cần phải thực hiện công việc Run Container.
- Cú pháp: 'docker image pull (tên image)'
## Kiến thức thêm
1. Docker CE/Docker EE
- Docker CE (Community Edition)
    - Là phiên bản miễn phí, sử dụng bởi các nhà phát triển, cá nhân, các tổ chức nhỏ.
    - Được hỗ trợ thông qua cộng đồng, diễn đàn Docker, GitHub và các tài liệu trực tuyến.
- Docker EE (Enterprise Edition)
    - Là phiên bản tính phí, được sử dụng bởi các doanh nghiệp, tập đoàn lớn, yêu cầu bảo mật cao.
    - Hỗ trợ các giải pháp bảo mật doanh nghiệp, nhận hỗ trợ kỹ thuật từ Docker, cập nhật bảo mật và bản vá lỗi thường xuyên.
2. Docker Engine
- Các thành phần chính của Docker Engine:
    - Docker Daemon (dockerd): 
        Là tiến trình nền chịu trách nhiệm quản lý các đổi tượng Docker như image, container, network và volume.
        Giao tiếp với Docker CLI và các API khác để xử lý các lệnh.
    - Docker Client (docker):
        Là giao diện dòng lệnh (CLI) mà người dùng sử dụng để tương tác với Docker Daemon
        Khi chạy lệnh CLI, nó sẽ gửi các lệnh đó đến Docker Daemon
    - Docker API:
        Là các API mà Docker Client và các ứng dụng khác có thể sử dụng để giao tiếp với Docker Daemon
        Cung cấp các điểm cuối để quản lý container, hình ảnh, mạng và volume.
- Các chức năng chính của Docker Engine:
    - Quản lý Containers
    - Quản lý Images
    - Quản lý Networks
    - Quản lý Volumes
3. Docker Compose
- Docker Compose là một công cụ giúp bạn định nghĩa và quản lý các ứng dụng Docker đa container. Docker Compose được sử dụng để dễ dàng cấu hình và chạy nhiều container Docker đồng thời.
- Để cài đặt plugin Compose CLI, sử dụng: <br>
    'DOCKER_CONFIG=$ {DOCKER_CONFIG:-$HOME/.docker}'<br>
    'mkdir -p $DOCKER_CONFIG/cli-plugins'<br>
    'curl -SL https://github.com/docker/compose/releases/download/v2.28.1/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose'<br>
- Áp dụng quyền thực thi: 
    'chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose'
- Hoặc áp dụng với tất cả users:
    'sudo chmod +x /usr/local/lib/docker/cli-plugins/docker-compose'
- Kiểm tra version docker compose:
    'docker compose version'<br>
    ![versioncompose](../image/docker6.png)
- Lệnh cơ bản của Docker Compose
    - Khởi động các dịch vụ:<br>
        'docker-compose up'
    - Chạy các dịch vụ ở chế độ nền:<br>
        'docker-compose up -d'
    - Dừng các dịch vụ:<br>
        'docker-compose down'
    - Xem logs của các dịch vụ:<br>
        'docker-compose logs'
    - Xem trạng thái của các dịch vụ:<br>
        'docker-compose ps'
4. Docker Registry
- Docker Registry là một hệ thống lưu trữ và phân phối các hình ảnh (image) Docker. Đây là nơi ta có thể lưu trữ các hình ảnh Docker của mình và chia sẻ chúng với các thành viên khác trong đội ngũ phát triển hoặc triển khai chúng lên các môi trường khác nhau. Docker Registry có thể là Public (ví dụ như Docker Hub) hoặc Private (self-hosted).
5. Docker Daemon
- Docker Daemon thường được gọi là 'dockerd', chịu trách nhiệm quản lý các đổi tượng Docker như container, image, network và volume. Docker Daemon chạy trên nền hệ thống và chờ đợi các lệnh từ Docker Client hoặc các API để thực hiện các thao tác.
6. Docker Network: Host, Bridge, Overlay, ....
- Docker cung cấp 3 loại Network chính:
    - Bridge Network: 
        Các container trên cùng một Bridge Network có thể giao tiếp với nhau qua địa chỉ IP, thích hợp cho các ứng dụng đang chạy trên cùng một máy chủ Docker<br>
        Tạo mạng Bridge Network mới: 'docker network create (tên mạng)'.
    - Host Network:
        Container sẽ sử dụng mạng của máy chủ Docker trực tiếp, không tạo ra lớp mạng riêng.
        Chỉ định mạng Host Network khi chạy container: <br>
        'docker run -d --name (tên container) --network host (image mà docker sẽ chạy)'<br>
        '-d' để chạy container ở chế độ nền (detached)
    - Overlay Network:<br>
        Cho phép các container chạy trên các máy chủ Docker khác nhau có thể giao tiếp với nhau.<br>
        Tạo mạng Overlay Network mới: 'docker network create -d overlay (tên mạng)'
7. Docker Volume, Mount
- Docker Volume và Mount là hai cách quản lý và lưu dữ liệu bền vững cho các container. Dữ liệu sẽ không bị mất khi container bị xóa hoặc khởi động lại.
- Docker Volume:
    - Tạo Docker Volume: 'docker volume create (tên volume)'
    - Sử dụng Docker Volume: 
    Khi chạy container, có thể gắn volume bằng tùy chọn '-v' hoặc '--mount'<br>
    + Sử dụng tùy chọn '-v':<br>
'docker run -d --name (tên container) -v (tên volume):(thư mục container) (image mà docker sẽ chạy)'
<br>
    + Sử dụng tùy chọn '--mount':<br>
'docker run -d --name (tên container) --mount source=(tên volume),target=/app nginx'<br>
    '/app': thư mục container mà volume được gắn vào<br>
    'nginx': image mà docker sẽ chạy<br>

## Thực hành
 - Chạy một ứng dụng sử dụng docker-compose<br>
    - Chạy ứng dụng 1:<br>
![ungdung1](../image/docker7.png)<br>
    - Chạy ứng dụng 2 (Grafana và Prometheus):<br>
![alt text](../image/docker8.png)
