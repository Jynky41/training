## Tìm hiểu tổng quan về Linux
## Chung
1. **Lịch sử phát triển**
- Linux là hệ điều hành tương tự với Microsoft Windows, nhưng Linux được tự do phát triển theo nhu cầu sử dụng và hoàn toàn miễn phí.
- Ngày 5/4/1991, Linus Torvalds, chàng sinh viên 21 tuổi của trường Đại học Helsinki - Phần Lan đã bắt tay vào viết những dòng lệnh đầu tiên của Linux.
2. **Các distro phổ biến của Linux**
- Debian
- Ubunto
- RedHat Enterprise Linux
- CenOS
- Kali Linux
3. **Kiến trúc tổng quan hệ thống Linux**
- Kernel (nhân)
- Shell
- Applications
4. **Cấu trúc hệ thống file**<br>
![file](../image/linux2.png)<br>

|Thư mục|Mô tả nội dung|
|-|-|
|/|Thư mục gốc|
|/bin|Chứa các file binary của các tập lệnh trong Linux|
|/sbin|Tương tự như /bin, nhưng là những lệnh chỉ được dùng bởi quản trị hệ thống - root user|
|/boot|Chứa các thư viện cần thiết cho quá trinh khởi động|
|/dev|Chứa thông tin chứa các file thiết bị|
|/etc|Chứa file cấu hình hệ thống và ứng dụng|
|/lib|Chứa thư viện chia sẻ được dùng bởi các tiến trình, các lệnh boot, lệnh hệ thống|
|/lib64|Tương tự như lib nhưng dành cho hệ thống 64bit|
|/opt|Nơi dành riêng cho các tiện ích chương trình được cài đặt|
|/media|Thư mục có vai trò như đích đến của quá trình mount point|
|/run|Thư mục chứa dữ liệu chạy tạm thời|
|/root|Thư mục home của user root|
|/home|Thư mục chứa các thư mục home của các user được tạo|
|/sys|Thư mục chứa thông tin về hệ thống|
|/srv|Chứa dữ liệu, các file của các dịch vụ trên hệ thống|
|/mnt|Thư mục này được dùng để gắn các hệ thống tập tin tạm thời|
|/proc|Lưu các thông tin về tình trạng của hệ thống|

## Các câu lệnh cơ bản
1. **Lệnh trợ giúp : man**
- Để xem hướng dẫn sử dụng lệnh cp (copy) có thể nhập lệnh 'man cp', 'q' để thoát
2. **Các lệnh kiểm tra performance**
- Lệnh xem thông tin RAM: 'cat /proc/meminfo'
- Lệnh cat: dùng để đọc nội dung của file text
- 'free -m' : xem ram free và used hiển thị theo megabytes
- Lệnh xem thông tin CPU: 'cat/proc/cpuinfo'
- Lệnh xem dung lượng ổ cứng: df-h
- Xem tên server: hostname
- Xem địa chỉ ip: ifconfig
3. **Các lệnh quản lý file và thư mục**
- 'ls (option)' (đường dẫn thư mục)
- option: -l: Liệt kê chi tiết; -a liệt kê tất cả các file ẩn
- Lệnh chuyển thư mục: 'cd (đường dẫn)'
- Lệnh cho biết thư mục hiện tại: 'pwd'
- Đổi mật khẩu đăng nhập của user đang login: 'passwd'
- Lệnh sao chép file: 'cp'
- Lệnh dùng để đổi tên / di chuyển thư mục hoặc file từ nơi này sang nơi khác: 'mv'
- Lệnh tạo thư mục: 'mkdir'
- Lệnh xóa thư mục rỗng: 'rmdir'
- Lệnh xóa file/ thư mục: 'rm'
- 'cat (file)': Hiển thị toàn bộ nội dung file văn bản
- 'sudo': Super user do, thực hiện câu lệnh với quyền cao nhất
- 'touch (file)': tạo file mới
4. **Các lệnh hệ thống**
- Lệnh shutdown: 'shutdown (option) (time) (wall)'
    Option:
    h: shutdown
    r: restart
    c: cancel pending shutdown
    Time:
    now: thực hiện ngay lập tức
    hh:mm: ấn định thời gian(giờ:phút)
    +m: sau m phút sẽ thực hiện
    Wall: Message thông báo
- Lệnh reboot: Khởi động lại Server
    'reboot'
- Lệnh date: Xem ngày giờ hệ thống
    'date'
## Quá trình khởi động của Server Linux<br>
![Boot](../image/linux1.png)
1. **System Startup**
- Đây là bước đầu tiên của quá trình khởi động, ở bước này BIOS (basic input/output system) thực hiện một công việc gọi là POST (Power-on Self-test). POST là một quá trình kiểm tra tính năng sẵn sàng phần cứng. Nếu quá trình POST kết thúc thành công, BIOS sẽ có gắng tìm kiếm và khởi chạy (boot) một hệ điều hành được chứa trong các thiết bị lưu trữ như ổ cứng, USB, CD/DVD, ... Nếu hệ điều hành Linux được cài trên ổ cứng thì nó sẽ tìm đến MBR tại sector đầu tiên của ổ đĩa đầu tiên
2. **MBR loading**
- MBR (Master Boot Record) được lưu trữ tại Sector đầu tiên của một thiết bị lưu trữ dữ liệu, ví dụ /dev/hda hoặc dev/dsa/.
  MBR chứa thông tin:
  - Primary boot loader code: Cung cấp thông tin boot loader và vị trí boot loader trên ổ cứng
  - Partition table information: Lưu trữ thông tin của các partition
  - Magic number: Sử dụng để kiểm tra MBR, nếu MBR bị lỗi nó sẽ phục hồi lại
3. **Bootloader stage 2 (Grub Loader)**
- Sau khi xác định vị trí BootLoader, bước này sẽ thực hiện loading BootLoader vào bộ nhớ và đọc thông tin cấu hình sau đó hiển thị GRUB boot menu để user  lựa chọn
4. **Kernel**
- Kernel của hệ điều hành sẽ được nạp vào trong RAM. Khi kernel hoạt động thì việc đầu tiên đó là thực thi quá trình INIT
5. **INIT**
- Đây là giai đoạn chính của quá trình BOOT. Quá trình này bắt đầu bằng việc đọc file /etc/inittab để xác định run-level. Sau đó sẽ thực thi các script tương ứng với run-level
6. **User prompt**
- Người đăng nhập và sử dụng
7. **Run Levels**
- Run levels là chế độ sử dụng của Server, thường là 2 chế độ Multi-user.target và Graphical.target
- Thiết lập Multi-user.target mặc định khi khởi động: 
    #systemctl set-default multi-user.target
- Thiết lập Graphical.target mặc định khi khởi động: 
    #systemctl set-default graphical.target
## Cấu hình Network <br>
1. **Các file cấu hình network**
- /etc/hosts: Dùng phân giải hostname không thể phân giải được. Có thể dùng thay DNS trong hệ thống mạng LAN
- /etc/resolves: Chỉ định DNS server để phân giải tên miền
- /etf/sysconfig/network-scripts/ifcfg(tên card mạng): trong thư mục này chứa thông tin cấu hình của từng card mạng
2. **Các lệnh network**
- Xem địa chỉ ip:
    'ifconfig'
- Tắt mở card mạng:
    'ifup (têncard)': bật card mạng
    'ifdown (têncard)': tắt card mạng
- Khởi động lại dịch vụ
    'service network restart'
    hoặc
    'etc/init.d/network restart'
    hoặc
    'systemctl restart network.service'
- Xem thông tin gateway
    'route'
3. **Cấu hình ip**
**Cấu hình bằng lệnh**
- 'ifconfig (tên card mạng) (ip) netmask (netmask) up'<br>
    Đây là cách đặt ip tạm thời, sẽ mất khi tắt mở card mạng hay restart dịch vụ network.<br>
**Cấu hình bằng file** <br>
- Thay đổi nội dung trong file cấu hình của card mạng:
    'vi etc/sysconfig/network-scripts/ifcfg (tên card mạng)'
- Các thông số cần chú ý trong file:

|||
|-|-|
|DEVICE|Tên của card mạng|
|ONBOOT|yes: Khởi động khi restart dịch vụ network; <br>no: Không khởi động khi restart dịch vụ network|
|BOOTPROTO|static: dùng địa chỉ IP tĩnh;<br> dhcp: nhận IP từ DHCP server|
|IPADDR|Địa chỉ IP của card mạng (dùng khi cấu hình ip tĩnh)|
|NETMASK|Địa chỉ subnet mask (dùng khi cấu hình ip tĩnh)|
|GATEWAY|Địa chỉ gateway (dùng khi cấu hình ip tĩnh)|
|DNS{1,2}|Tên DNS server|
|USERCTL|yes: cho phép user thường cấu hình card mạng này;<br> no: không cho phép user thường cấu hình card mạng này|

<br>

**Cấu hình ip bằng giao diện**
- Sử dụng lệnh: 'nmtui'<br>
![nmtui1](../image/linux3.png) <br>
![nmtui2](../image/linux4.png) <br>
![nmtui3](../image/linux5.png) <br>
Sau khi chỉnh sửa xong thực hiện active card mạng <br>
![nmtui4](../image/linux6.png) 
<br>hoặc sử dụng lệnh:
    'service network restart'<br>

**Thay đổi tên card mạng về dạng ethx** <br>
- Kiểm ra thông tin interface hiện tại
    'ifconfig'<br>
![checkcard](../image/linux7.png) <br>
-Edit "/etc/sysconfig/grub"
    'vi /etc/sysconfig/grub'
    Tìm dòng bắt đầu bằng GRUB_CMDLINE_LINUX và thêm 'net.ifnames=0 biosdevname=0' <br>
![thaydoicf](../image/linux8.png) <br>
    Cập nhất cấu hình grub:
    'sudo grub2-mkconfig -o /boot/grub2/grub.cfg' <br>
![updategrub](../image/linux9.png) <br>
- Xác định tên card mạng hiện tại bằng lệnh 'ip link'
- Giả sử tên card mạng hiện tại là ens33
- Đổi tên tệp cấu hình mạng hiện tại (ifcfg-ens33) sang dạng ifcfg-eth0:
    'sudo mv /etc/sysconfig/network-scripts/ifcfg-ens33 /etc/sysconfig/network-scripts/ifcfg-eth0'
- Mở tệp cấu hình vừa đổi tên và thay đổi nội dung dòng DEVICE=ens33 thành DEVICE=eth0
- Reboot server và kiểm tra lại kết quả
    'reboot'
    'ifconfig' <br>
![ketqua](../image/linux10.png) <br>
4. **Cài đặt chương trình**<br>
**Các lệnh nén và giải nén**
- Nén file '.gz'
    'gzip'
- Giải nén '.gz'
    'gzip-d/etc/passwd'
    'gunzip/etc/passwd'
- Nén file '.tar'
    'tar -cvf/home/file.tar /home/abc'
    'tar -cvf/home/file.tar.gz /home/abc'
- Giải nén file '.tar'
    'tar -xvf/home/file.tar'
    'tar -xvf/home/file.tar.gz'
- Nén file '.bz2'
    'tar -cif filenen.tar.bz2 file
- Giải nén file '.bz2'
    'tar -xif file.tar.bz2'<br>
**Cài đặt từ rpm**
- RedHat Package Manager (RPM) là một hệ thống quản lý những package được Linux hỗ trợ cho người dùng.
- Những package được đóng gói có dạng: 'Tên package-phiên bản-số hiệu.kiến trúc.rpm'
- Ví dụ: app-1.0-1.i386.rpm
- Cú pháp rpm:
    rpm -option (tên phần mềm)
    option:
    -i: cài đặt
    -e: xóa
    -U: update
    --nodeps: không kiểm tra các gói phụ thuộc
    -qa: tìm phần mềm
    -ql: tìm nơi cài đặt
- Cài đặt phần mềm bằng rpm:
    'rpm -ivh (gói phần mềm)'<br>
**Cài đặt từ source**
- Giải nén file (tar-xzvf file)
- Cài đặt file: 'make install'<br>
**Cài đặt từ yum**<br>
    'yum options (tên/nhóm phần mềm)'
    option:
    - Install: cài đặt phần mềm
    - Remove: xóa phần mềm
    - List installed: xem các phần đã cài
    - Groupinstall: Cài 1 nhóm phần mềm
    - Groupremove: gỡ bỏ 1 nhóm phần mềm
    - Clean: Xóa các cache, plugin, meta-data
5. **Kỹ thuật Logical volume manager (LVM)**
- LVM là một phương pháp cho phép ấn định không gian đĩa cứng thành những Logical Volume khiến cho việc thay đổi kích thước trở nên dễ dàng<br>
    **Các khái niệm**
- Physical Volume: Là một cách gọi khác của partition trong kỹ thuật LVM, nó là những thành phần cơ bản được sử dụng LVM
- Logical Volume Group: nhiều Physical Volume trên những ổ đĩa khác nhau được kết hợp lại thành một Logical Volume Group, LVM được xem như một ổ đĩa ảo.
- Physic Extent: Là một đại lượng thể hiện một khối dữ liệu dùng làm đơn vị tính dung lượng của Logical Volume<br>
    **Một số lệnh của LVM**

||creat|view|remove|resize|change|scan|
|-|-|-|-|-|-|-|
|PV|pvcreate|pvs,pvdisplay|pvremove||pvchange|pvscan|
|VG|vgcreate|vgs,vgdisplay|vgremove|vgextend,vgreduce|vgchange|vgscan|
|LV|lvcreate|lvs,lvdisplay|lvremove|lvextend, lvreduce|lvchange|lvscan|

- Tạo physical volume<br>
    ![physicalvolume](../image/linux11.png)
- Tạo một Group volume<br>
    ![groupvolume](../image/linux12.png)
- Tạo một Logical volume<br>
    ![logicalvolume](../image/linux13.png)
