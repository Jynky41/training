## **Tổng quan Prometheus Server**
## **Giới thiệu cơ bản về Prometheus**
1. **Monitoring là gì? Khác gì với Logging và Tracing**
- Monitoring (Giám sát): Là hoạt động thu thập, phân tích và sử dụng dữ liệu để theo dõi hiệu suất và tình trạng của các ứng dụng, cơ sở hạ tầng và dịch vụ theo thời gian. Trong Prometheus, giám sát thường bao gồm:
   - Thu thập số liệu: Thu thập dữ liệu định lượng như mức sử dụng CPU, mức tiêu thụ bộ nhớ, tỷ lệ yêu cầu, tỷ lệ lỗi và thời gian phản hồi
   - Cảnh báo: Thiết lập ngưỡng và cảnh báo để thông báo cho người quản trị hoặc người vận hành khi một số liệu vượt quá giới hạn được xác định trước.
   - Hình ảnh hóa: Sử dụng các công cụ như Grafana để tạo bảng thông tin và biểu đồ cung cấp hình ảnh trực quan về số liệu đã thu thập.
- Logging (Ghi nhật ký): Là việc ghi lại các sự kiện riêng biệt xảy ra trong một ứng dụng hoặc hệ thống. Nhật ký cung cấp thông tin chi tiết về những gì ứng dụng hoặc hệ thống đang làm tại các thời điểm cụ thể. Chúng thường bao gồm:
   - Nhật ký lỗi: Thông tin về lỗi và ngoại lệ
   - Nhật ký truy cập: Chi tiết về các yêu cầu truy cập và phản hồi
   - Nhật ký kiểm tra: Ghi lại các sự kiện liên quan đến bảo mật và hoạt động của người dùng
- Tracing (theo dõi): Là việc theo dõi luồng yêu cầu hoặc giao dịch khi chúng đi qua nhiều dịch vụ và thành phần khác nhau của một hệ thống phân tán. Tracing giúp hiểu được vòng đời của một yêu cầu và xác định các điểm nghẽn hoặc lỗi hiệu suất trong kiến trúc vi dịch vụ.
2. **Prometheus là gì?**
- Prometheus là một dịch vụ theo dõi và cảnh báo về hệ thống. Đây là một dịch vụ mã nguồn mở hoàn toàn miễn phí. SoundCloud đã khởi đầu xây dựng Prometheus từ năm 2012. Giờ đây Prometheus đã được tách khỏi SoundCloud và là một dự án mã nguồn mở độc lập
- Tính năng quan trọng nhất của Prometheus là thu thập thông số, dữ liệu từ các mục tiêu (dịch vụ) được nhắm đến theo khoảng thời gian nhất định đã được cài đặt trước. Ngoài ra còn các API khác thể hiện được kết quả, đánh giá bằng biểu thức quy tắc và đưa ra cảnh báo. Prometheus còn cung cấp một ngôn ngữ truy vấn rất mạnh PromQL, cực kì hữu ích khi giao tiếp với các dịch vụ monitor khác
3. **Các khái niệm cơ bản của Prometheus**
- Data models
   - Prometheus lưu trữ tất cả dữ liệu của mình dưới dạng chuỗi thời gian, bao gồm các điểm dữ liệu số liệu theo thời gian. Mỗi chuỗi thời gian được xác định duy nhất bằng tên số liệu (metric name) và một tập hợp các cặp khóa-giá trị được gọi là nhãn (labels).
   - Tên số liệu (metric name): Biểu thị tính năng chung của hệ thống được đo lường (ví dụ: 'http_requests_total')
   - Nhãn (Labels): Cặp khóa-giá trị (key-value) phân biệt các chiều của một số liệu (ví dụ: 'method="GET"', 'handler="/api"')
   - Dấu thời gian (Timestamp): Thời gian điểm dữ liệu được ghi lại
   - Giá trị (Value): Giá trị đo được của số liệu tại một dấu thời gian cụ thể
- Metric types
   - Prometheus hỗ trợ bốn loại số liệu cốt lõi, mỗi loại phục vụ một mục đích khác:
      - Bộ đếm (Counter): Một số liệu tích lũy chỉ tăng hoặc về 0 khi khởi động lại. Sử dụng để đếm các sự kiện như yêu cầu được phục vụ, nhiệm vụ đã hoàn thành, ...
      - Gauge: Biểu thị một giá trị số duy nhất có thể tăng hoặc giảm. Hữu ích để đo lường những thứ như mức sử dụng bộ nhớ (memory) hoặc nhiệt độ.
      - Biểu đồ Histogram: Lấy mẫu các quan sát và đếm chúng trong các nhóm có thể cấu hình. Hữu ích để đo lường sự phân bố của các giá trị (ví dụ: thời lượng yêu cầu).
      - Tóm tắt (Summary): Tương tự như biểu đồ histogram nhưng cung cấp các phân vị phát trực tuyến. Thích hợp để đo các phần trăm cao.
- Jobs and instances
   - Jobs: Một nhóm các trường hợp thực hiện cùng một chức năng. Công việc biểu thị một tập hợp các tác vụ hoặc dịch vụ tương tự. Ví dụ: Một tác vụ 'api_servers' có thể bao gồm tất cả các phiên bản của máy chủ API
   - Instance: Một tiến trình duy nhất mà bạn đang theo dõi, thường được xác định bằng tên máy chủ và cổng của nó. Một instance là một thực thể duy nhất cung cấp số liệu
4. **Kiến trúc của Prometheus stack và các thành phần cơ bản**
- Prometheus thực hiện quá trình kéo (pull) các thông số/số liệu (metric) từ các job (exporter). Sau đấy Prometheus sẽ lưu trữ các dữ liệu thu thập được ở local máy chủ. Tiếp đến sẽ chạy các rule để xử lý các dữ liệu theo nhu cầu cũng như kiểm tra thực hiện các cảnh báo mà bạn mong muốn.
- Kiến trúc của Prometheus stack <br>
![alt text](../image/pro1.png) <br>
- Các thành phần cơ bản:
   - Prometheus Server: 
      - Thu thập dữ liệu (Data Scraping): Máy chủ Prometheus định kỳ thu thập số liệu từ các mục tiêu được cấu hình (điểm cuối HTTP).
      - Lưu trữ (Storage): Lưu trữ tất cả các mẫu đã thu thập cục bộ trong cơ sở dữ liệu chuỗi thời gian.
      - Truy vấn (Querying): Cung cấp ngôn ngữ truy vấn mạnh mẽ (PromQL) để khám phá và trực quan hóa số liệu.
   - Exporters:
      - Node Exporter: Thu thập số liệu ở cấp độ phần cứng và hệ điều hành
      - Application Exporters: Có nhiều trình xuất cho nhiều ứng dụng khác nhau như MySQL, PostgreSQL, Redis, ...
      - Custom Exporters: Người dùng có thể viết công cụ xuất dữ liệu của riêng mình cho các số liệu tùy chỉnh
   - Alertmanager: 
      - Xử lý cảnh báo do Prometheus tạo ra dựa trên các quy tắc được cấu hình sẵn.
      - Loại bỏ trùng lặp, nhóm và định tuyến cảnh báo đến nhiều kênh thông báo khác nhau (email, slack, ...)
   - Push gateway:
      - Cho phép thực hiện các tác vụ tạm thời hoặc hàng loạt để đẩy số liệu lên Prometheus
5. **Trường hợp nào sử dụng Prometheus? Trường hợp nào không nên sử dụng**
- Trường hợp sử dụng Prometheus:
   - Giám sát hệ thống và dịch vụ: 
      - Đo lường hiệu suất và tình trạng của máy chủ, cơ sở hạ tầng mạng, và các dịch vụ như cơ sở dữ liệu, hệ thống lưu trữ, ...
      - Sử dụng các exporter như Node Exporter, MySQL Exporter, PostgreSQL Exporter để thu thập dữ liệu từ các hệ thống khác nhau.
   - Giám sát ứng dụng:
      - Thu thập và phân tích các chỉ số hiệu suất ứng dụng như độ trễ, tỷ lệ lỗi, số lượng yêu cầu, ...
      - Các ứng dụng có thể được tích hợp trực tiếp với Prometheus bằng cách sử dụng các thư viện client có sẵn.
   - Cảnh báo và tự động hóa:
      - Định nghĩa các quy tắc cảnh báo dựa trên dữ liệu giám sát và sử dụng Alertmanager để gửi thông báo qua email, Slack, ...
      - Tự động hóa các hành động phản hồi dựa trên các cảnh báo nhận được.
   - Giám sát Kubernetes:
      - Prometheus Operator giúp triển khai và quản lý Prometheus trên Kubernetes dễ dàng hơn.
   - Giám sát hệ thống phân tán:
      - Thanos mở rộng Prometheus cho phép lưu trữ lâu dài và truy vấn dữ liệu từ nhiều Prometheus server.
- Trường hợp không nên sử dụng Prometheus:
   - Giám sát logs:
      - Prometheus không phải là công cụ tốt cho việc thu thập và phân tích logs. Các công cụ như Elasticsearch, Logstash, và Kibana (ELK stack) hoặc Grafana Loki phù hợp hơn cho mục đích này.
   - Giám sát dữ liệu sư kiện không liên tục
   - Yêu cầu lưu trữ lâu dài và phức tạp
   - Quy mô cực lớn với yêu cầu phức tạp 
## **Cấu hình Prometheus**
1. Cấu hình Prometheus server
- Tệp 'prometheus.yml' chứa cấu hình chính cho Prometheus server<br>
<pre>global:
scrape_interval: 15s # Tần suất thu thập dữ liệu từ các target
evaluation_interval: 15s # Tần suất đánh giá các quy tắc
scrape_configs:
  - job_name: 'prometheus'
static_configs:
      - targets: ['localhost:9090']
  - job_name: 'node_exporter'
static_configs:  
      - targets: ['localhost:9100']</pre>
2. Recording rules
- Recording rules được sử dụng để tính toán và lưu trữ các chỉ số tổng hợp. <br>
   Ví dụ, tính toán tỷ lệ yêu cầu HTTP thành công: Tạo tệp 'recording_rules.yml':
<pre>
groups:
- name: example
  rules:
  - record: job:http_in_requests:rate5m
    expr: rate(http_in_requests_total[5m])
</pre>
- Cập nhật 'prometheus.yml' để bao gồm recording rules:
<pre>
rule_files:
  - 'recording_rules.yml'
</pre>
3. Alerting rules
- Alerting rules được sử dụng để định nghĩa các cảnh báo dựa trên dữ liệu giám sát. <br>
   Ví dụ, cảnh báo nếu tỷ lệ lỗi HTTP cao hơn 1% trong 5 phút: Tạo tệp 'alerting_rules.yml':
<pre>
groups:
- name: canhbao
  rules:
  - alert: HighErrorRate
    expr: rate(http_requests_total{status="500"}[5m]) / rate(http_requests_total[5m]) > 0.01
    for: 10m
    labels:
      severity: page
    annotations:
      summary: "Phát hiện tỉ lệ lỗi cao"
      description: "Tỷ lệ lỗi > 1% trong 5 phút"
</pre>
- Cập nhật 'prometheus.yml' để bao gồm alerting rules:
<pre>
rule_files:
  - 'alerting_rules.yml'
</pre>
- Cấu hình Prometheus để sử dụng Alertmanager. Cập nhật 'prometheus.yml':
<pre>
alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - 'localhost:9093'
</pre>
- Cấu hình Alermanager('alertmanager.yml'):
<pre>
global:
  smtp_smarthost: 'smtp.gmail.com:587'
  smtp_from: 'mailgui@gmail.com'
  smtp_auth_username: 'alertmanager'
  smtp_auth_password: 'password'

route:
  receiver: 'email'

receivers:
- name: 'email'
  email_configs:
  - to: 'mailnhan@gmail.com'
</pre>
4. Templating
<pre>
receivers:
- name: 'email'
  email_configs:
  - to: 'mailnhan@gmail.com'
    subject: 'Alert: {{ Tên nhãn cảnh báo }}'
    text: |
      {{ range .Alerts }}
      *Alert:* {{ Chú thích cảnh báo }}
      *Description:* {{ Miêu tả cảnh báo }}
      *Details:*
      {{ range .Labels.SortedPairs }} - {{ .Name }}: {{ .Value }}
      {{ end }}
      {{ end }}
</pre>
## **Tìm hiểu về PromQL**
- PromQL (Prometheus Query Language) là ngôn ngữ truy vấn mạnh mẽ được sử dụng để truy vấn dữ liệu trong Prometheus. Nó hỗ trợ các toán tử, hàm và các loại dữ liệu khác nhau để phân tích và hiện thị dữ liệu giám sát.
- Các loại dữ liệu trong PromQL:
   - Instant Vector: Một tập hợp các mẫu dữ liệu tại một thời điểm cụ thể
   - Range Vector: Một tập hợp các mẫu dữ liệu trong một khoảng thời gian
   - Scalar: Một giá trị số duy nhất
   - String: Một chuỗi văn bản duy nhất
- Truy vấn cơ bản:
   - Truy vấn tất cả các chỉ số:
   <pre>
   node_cpu_seconds_total
   </pre>
   - Truy vấn với nhãn:
   <pre>
   node_cpu_seconds_total{job="node_exporter"}
   </pre>
   - Truy vấn các chỉ số trong 5 phút gần đây:
   <pre>
   node_cpu_seconds_total[5m]
   </pre>
## **Tìm hiểu về Labels**
1. Labels là gì và để làm gì?
- Trong Prometheus, Labels (nhãn) là các cặp khóa- giá trị được gắn vào mỗi mẫu dữ liệu để cung cấp thông tin bổ sung về mẫu đó. Labels đóng vai trò rất quan trong trong việc tổ chức, truy vấn và tổng hợp dữ liệu giám sát.
2. Instrumentation & Target labels
- Instrumentation labels là các nhãn được thêm vào các chỉ số trong mã nguồn của ứng dụng hoặc dịch vụ bạn đang giám sát. Chúng được sử dụng để cung cấp ngữ cảnh bổ sung cho các chỉ số, giúp phân loại và phân tích dữ liệu chi tiết hơn.
- Targer Labels là các nhãn được thêm vào các mục tiêu (targets) giám sát trong tệp cấu hình 'prometheus.yml'. Chúng giúp tổ chức và phân loại các mục tiêu giám sát dựa trên các thuộc tính cụ thể như môi trường, vùng, vai trò, ...
- Cấu hình targer labels được định nghĩa trong phần 'scrape_configs' của tệp cấu hình Prometheus. Ví dụ:
<pre>
scrape_configs:
  - job_name: 'node_exporter'
    static_configs:
      - targets: ['localhost:9100']
        labels:
          environment: 'production'
          region: 'us-west'

  - job_name: 'app_server'
    static_configs:
      - targets: ['app1:8080', 'app2:8080']
        labels:
          environment: 'staging'
          region: 'us-east'
</pre>
3. Relabel 
- Target Relabeling: Được sử dụng để thay đổi nhãn của các mục tiêu trước khi chúng được thu thập bởi Prometheus
- Metric Relabeling: Được sử dụng để thay đổi nhãn của các mẫu dữ liệu trước khi chúng được lưu trữ trong cơ sở dữ liệu Prometheus
- Ví dụ cấu hình Target Relabeling:
<pre>
scrape_configs:
  - job_name: 'example'
    static_configs:
      - targets: ['localhost:9090']

    relabel_configs:
      - source_labels: [__address__]
        target_label: instance
        regex: (.*):9090
        replacement: $1
        action: replace

      - source_labels: [__address__]
        regex: localhost:9090
        action: drop
</pre>
   - Config đầu tiên thay đổi nhãn `'__address__'` thành nhãn `'instance'`
   - Config thứ hai loại bỏ các mục tiêu có địa chỉ `'localhost:9090'`.
## **Lưu trữ**
1. Các loại lưu trữ Prometheus
- Local Storage (Lưu trữ cục bộ): Prometheus mặc định sử dụng lưu trữ cục bộ để lưu trữ dữ liệu trên đĩa của máy chủ Prometheus. Dữ liệu được lưu trữ trong các tệp ở định dạng TSDB (Time Series Database).
- Remote Storage (Lưu trữ từ xa): Prometheus hỗ trợ remote storage để lưu trữ dữ liệu giám sát trên các hệ thống lưu trữ bên ngoài, giúp mở rộng khả năng lưu trữ và cải thiện độ bền của dữ liệu. Một số hệ thống lưu trữ từ xa phổ biến: Thanos, Cortex, VictoriaMetrics
2. Làm thế nào để tính toán sizing?
- Tính toán sizing trong Prometheus liên quan đến việc ước lượng tài nguyên cần thiết như CPU, bộ nhớ, và không gian đĩa để triển khai Prometheus dựa trên khối lượng dữ liệu giám sát và yêu cầu của hệ thống. Để tính toán sizing, cần xem xét một số yếu tố chính:
   - Ước lượng số lượng metrics:
      - Số lượng metrics: Tổng số lượng các metrics mà Prometheus sẽ thu thập.
      - Số lượng time series: Mỗi metric có thể có nhiều time series, tùy thuộc vào số lượng nhãn (labels).
      - Tần suất thu thập: Tần suất thu thập dữ liệu (scrape interval)
   - Kích thước dữ liệu:
      - Kích thước mẫu (Sample size): Mỗi mẫu dữ liệu (sample) thường có kích thước khoảng 2 byte khi được nén.
      - Tần suất lưu trữ: Số lượng mẫu được lưu trữ mỗi giây
      - Dung lượng lưu trữ mỗi ngày: Dung lượng lưu trữ cần thiết mỗi ngày.
   - Ước lượng tài nguyên:
      - CPU: Tài nguyên CPU cần thiết để thu thập, nén và lưu trữ dữ liệu
      - Bộ nhớ: Bộ nhớ cần thiết để lưu trữ dữ liệu trong bộ nhớ đệm (in-memory)
      - Không gian đĩa: Bộ nhớ cần thiết để lưu trữ dữ liệu lịch sử
## **Tìm hiểu về Alertmanager**
1. Tổng quan và cách thức hoạt động
- Alertmanager là một thành phần quan trọng của hệ thống giám sát Prometheus, chịu trách nhiệm xử lý các cảnh báo (alert) do Prometheus gửi tới. Nó giúp quản lý cảnh báo theo nhiều cách khác nhau như nhóm các cảnh báo lại, định tuyến cảnh báo tới các kênh thông báo khác nhau, và xử lý các cảnh báo trùng lặp.
- Cách thức hoạt động của Alertmanager:
   Alertmanager nhận các cảnh báo từ Prometheus qua HTTP API và xử lý chúng theo các bước sau:
   - Nhận cảnh báo (Receiving Alerts): Prometheus gửi các cảnh báo đến Alertmanager khi các điều kiện cảnh báo được kích hoạt.
   - Nhóm cảnh báo (Grouping Alerts): Các cảnh báo được nhóm lại với nhau dựa trên cấu hình nhóm (grouping rules) trong tệp cấu hình.
   - Định tuyến cảnh báo (Routing Alerts): Các cảnh báo được định tuyến đến các kênh thông báo dựa trên các quy tắc định tuyến (routing rules).
   - Xử lý trùng lặp (Silencing Alert): Các cảnh báo trùng lặp được xử lý và loại bỏ để tránh gửi quá nhiều thông báo.
   - Xác nhận cảnh báo (Inhibition): Các cảnh báo không cần thiết được ngăn chặn dựa trên các điều kiện xác nhận (inhibition rules).
   - Gửi thông báo (Sending Notifications): Các thông báo được gửi đến các hệ thống bên ngoài như email, Slack, ...
2. Cấu hình
- Alertmanager được cấu hình thông qua một tệp YAML ('alertmanager.yml').<br>
Ví dụ: <br>
<pre>
global:
  resolve_timeout: 5m

route:
  receiver: 'default-receiver'
  group_by: ['alertname']
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 1h

  routes:
    - match:
        alertname: 'node_down'
      receiver: 'team-X-pager'
      group_wait: 10s
      group_interval: 2m
      repeat_interval: 30m

    - match_re:
        severity: 'critical|warning'
      receiver: 'team-Y-slack'

receivers:
  - name: 'default-receiver'
    email_configs:
      - to: 'default@example.com'

  - name: 'team-X-pager'
    pagerduty_configs:
      - service_key: '<team-X-service-key>'

  - name: 'team-Y-slack'
    slack_configs:
      - api_url: 'https://hooks.slack.com/services/XXXXX/YYYYY/ZZZZZ'
        channel: '#alerts'
</pre>
3. Silence Alerts
- Silencing là một tính năng hữu ích trong Alertmanager, giúp giảm thiểu các cảnh báo không mong muốn trong thời gian bảo trì hoặc khi xử lý sự cố đã biết. Có thể tạo, quản lý và xóa silences thông qua giao diện web của Alertmanager hoặc qua API.
## **Tìm hiểu về Service discovery**
- Service Discovery là một thành phần quan trọng trong hệ thống giám sát Prometheus, giúp tự động phát hiện và theo dõi các dịch vụ và Endpoints mà không cần cấu hình thủ công. Prometheus hỗ trợ nhiều cơ chế Service Discovery khác nhau để phù hợp với nhiều môi trường khác nhau như Kubernetes, Consul, EC2, GCE, ...
1. Tại sao phải có service discovery
Cần có Service Discovery vì:
- Tự động hóa: Giảm thiểu việc cấu hình thủ công khi có sự thay đổi trong hệ thống
- Tính động: Dễ dàng theo dõi các dịch vụ khi chúng thay đổi hoặc mở rộng
- Độ tin cậy: Giảm nguy cơ bỏ sót hoặc cấu hình sai các endpoints cần giám sát
2. Một số loại service discovery cơ bản
- File-base Service Discorvery
   - Ví dụ cấu hình file-based Service Discovery
<pre>
      scrape_configs:
  - job_name: 'file-based'
    file_sd_configs:
      - files:
        - '/path/to/targets.json'
</pre>
   - Ví dụ nội dung tệp `targets.json`:
<pre>
[
  {
    "targets": ["localhost:9090"],
    "labels": {
      "env": "prod",
      "job": "prometheus"
    }
  },
  {
    "targets": ["localhost:8080"],
    "labels": {
      "env": "prod",
      "job": "app"
    }
  }
]
</pre>
- Consul Service Discovery
   - Consul service discovery cho phép Prometheus phát hiện các dịch vụ được đăng ký trong Consul. Consul là một hệ thống quản lý dịch vụ và khám phá dịch vụ mạnh mẽ. 
   - Ví dụ về cấu hình consul:
<pre>
scrape_configs:
  - job_name: 'consul-services'
    consul_sd_configs:
      - server: 'localhost:8500'
        services: []
    relabel_configs:
      - source_labels: [__meta_consul_service]
        action: keep
        regex: .*
</pre>

## **Cài đặt và cấu hình Prometheus + exporters**
1. Cài đặt Prometheus stack
- Cài đặt Prometheus trên CentOS 7:
<pre>wget https://github.com/prometheus/prometheus/releases/download/v2.46.0/prometheus-2.46.0.linux-amd64.tar.gz</pre>
  - Giải nén file vừa tải về:
<pre>tar -xvf prometheus-2.46.0.linux-amd64.tar.gz
cd prometheus-2.46.0.linux-amd64 </pre>
  - Tạo người dùng Prometheus:
<pre>
useradd --no-create-home --shell /bin/false prometheus
</pre>
  - Tạo các thư mục cần thiết và đặt quyền truy cập:
<pre>
mkdir /etc/prometheus
mkdir /var/lib/prometheus
chown prometheus:prometheus /etc/prometheus
chown prometheus:prometheus /var/lib/prometheus
</pre>
  - Di chuyển các file cần thiết và đặt quyền truy cập:
<pre>
mv prometheus /usr/local/bin/
mv promtool /usr/local/bin/
chown prometheus:prometheus /usr/local/bin/prometheus
chown prometheus:prometheus /usr/local/bin/promtool
mv prometheus.yml /etc/prometheus/
mv consoles /etc/prometheus/
mv console_libraries /etc/prometheus/
chown -R prometheus:prometheus /etc/prometheus
</pre>
  - Tạo file dịch vụ systemd:
<pre>
nano /etc/systemd/system/prometheus.service
</pre>
  - Thêm nội dung vào file `prometheus.service`:
<pre>
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
  --config.file /etc/prometheus/prometheus.yml \
  --storage.tsdb.path /var/lib/prometheus/ \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
</pre>
  - Tải lại daemon và khởi động Prometheus:
<pre>
systemctl daemon-reload
systemctl start prometheus
systemctl enable prometheus
</pre>

**Chạy Prometheus, Node Exporter, Grafana, Alertmanager trong Docker**
- Tạo file cấu hình `prometheus.yml`, `docker-compose.yml`, `alertmanager.yml`, `alert.rules.yml`:
- Nội dung file `prometheus.yml`:
<pre>
global:
  scrape_interval: 15s

rule_files:
  - "alert.rules.yml"

alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - 'alertmanager:9093'  # Địa chỉ Alertmanager

scrape_configs:
  - job_name: prometheus
    scrape_interval: 5s
    static_configs:
    - targets:
      - prometheus:9090
      - cadvisor:8080

  - job_name: docker
    scrape_interval: 5s
    static_configs:
    - targets:
      - 10.10.10.128:9323

  - job_name: nodeexporter
    scrape_interval: 5s
    static_configs:
    - targets:
      - 10.10.10.128:9100
</pre>
- Nội dung file `docker-compose.yml`:
<pre>
version: '3.7'
services:
  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    ports:
      - 9090:9090
    command:
      - --config.file=/etc/prometheus/prometheus.yml
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml:ro
      - ./alert.rules.yml:/etc/prometheus/alert.rules.yml:ro
    depends_on:
      - cadvisor
  cadvisor:
    image: google/cadvisor:latest
    container_name: cadvisor
    ports:
      - 8080:8080
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
  node-exporter:
    image: prom/node-exporter:latest
    container_name: node-exporter
    command:
      - '--path.rootfs=/host'
    network_mode: host
    pid: host
    restart: unless-stopped
    ports:
      - 9100
    volumes:
      - '/:/host:ro,rslave'
  grafana:
    image: grafana/grafana
</pre>
- Nội dung file `alertmanager.yml`:
<pre>
global:
  smtp_smarthost: 'smtp.gmail.com:587'  # Địa chỉ SMTP server
  smtp_from: 'mail@gmail.com'  # Địa chỉ email người gửi
  smtp_auth_username: 'mailg@gmail.com'  # Tên đăng nhập SMTP
  smtp_auth_password: 'xxxx xxxx xxxx xxxx'  # Mật khẩu SMTP

route:
  receiver: 'mail@gmail.com'

receivers:
  - name: 'mail@gmail.com'
    email_configs:
      - to: 'mail@gmail.com'  # Địa chỉ email người nhận
        send_resolved: true  # Gửi email khi cảnh báo được giải quyết
</pre>
- Nội dung file `alert.rules.yml`:
<pre>
groups:
- name: canhbao
  rules:
  - alert: HighMemoryUsage
    expr: node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes * 100 < 10
    for: 1m
    labels:
      severity: critical
    annotations:
      summary: "Instance {{ $labels.instance }} memory usage is high"
      description: "Memory usage is above 90% on instance {{ $labels.instance }}"
- name: exporter-status
  rules:
  - alert: ExporterDown
    expr: up == 0
    for: 1m
    labels:
      severity: critical
    annotations:
      summary: "Exporter {{ $labels.instance }} is down"
      description: "Exporter on {{ $labels.instance }} has been down for more than 1 minutes."
</pre>
- Chạy Dockercompose:
  `docker-compose up -d` <br>
![alt text](../image/pro3.png)
2. Mục tiêu giám sát: metric của host vật lý, metric của container, hiển thị trên grafana
- Giám sát metric của host vật lý (Sử dụng Node_exporter)
![nodeexporter](../image/pro4.png)
- Giám sát metric của container (Sử dụng Cadvisor)
![alt text](../image/pro5.png)
3. Alertmanager để gửi alert đến Mail. Alert rules có thể về memory, cpu, disk hoặc đơn giản check monitoring service bị chết, container bị stop
- Giao diện trên Prometheus hiển thị các Alerts:
![alt text](../image/pro6.png)
- Thông báo về mail:
![alt text](../image/pro7.png)

## **Prometheus high-availability**
- Prometheus high-availability bao gồm việc cấu hình Prometheus để đảm bảo giám sát và cảnh báo liên tục ngay cả khi một hoặc nhiều phiên bản Prometheus bị lỗi. Các cách để thiết lập Prometheus high-availability :
  - Chạy nhiều phiên bản Prometheus song song. Các phiên bản này phải được cấu hình để thu thập cùng một mục tiêu
  - Sử dụng giải pháp hỗ trợ tích hợp đọc ghi file từ xa với các hệ thống như Thanos, Cortex hoặc VictoriaMetrics hay sử dụng lưu trữ cloud
  - Đảm bảo tất cả các phiên bản Prometheus đều có cấu hình service discovery nhất quán.
  - Sử dụng bộ cân bằng tải để phân phối lưu lượng trên các phiên bản Prometheus. Có thể sử dụng các công cụ như HAProxy, Nginx hoặc bộ cân bằng tải gốc trên nền tảng đám mây
  - Liên kết Prometheus để tổng hợp dữ liệu từ nhiều máy chủ Prometheus.
## **Alert manager high-availability**
- Alert manager high-availability liên quan đến việc chạy nhiều phiên bản Alertmanager trong một cụm. Các phiên bản này chia sẻ cùng một trạng thái và phối hợp các thông báo cảnh báo để đảm bảo rằng các cảnh báo không bị trùng lặp và các thông báo được phân phối một cách đáng tin cậy. Cách thiết lập Alert manager high-availability:
  - Chạy nhiều phiên bản Alertmanager
  - Các phiên bản Alertmanager sử dụng cùng một tệp cấu hình
## **Prometheus federation**
- Prometheus federation cho phép mở rộng và tổng hợp dữ liệu từ nhiều phiên bản Prometheus thành một chế độ xem toàn cục nhất. Thiết lập này đặc biệt hữu ích dể quản lý các môi trường quy mô lớn
## **Tìm hiểu về các exporters cơ bản**
1. Node-exporter
- Etrics một node (hiểu là một server) như CPU, RAM của node, dung lượng ổ đĩa, số lượng request tới node đấy, ...
2. Cadvisor
- Export các metrics của các docker service, các process trên server.
3. Haproxy-exporter
- Là công cụ được sử dụng để xuất các chỉ số và thông tin từ HAProxy sang cho Prometheus. Nó là một exporter của Prometheus, cho phép bạn thu thập các metrics từ HAProxy và đưa chúng vào Prometheus để giám sát và phân tích hiệu suất của HAProxy.
4. Mysql-exporter
- Mysql-exporter là một công cụ được sử dụng để xuất các metrics từ MySQL database vào Prometheus. 