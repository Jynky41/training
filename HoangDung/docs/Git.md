## Tìm hiểu tổng quan về Git
## Chung
1. **Git là gì?**
- Git là tên gọi của một Hệ thống quan lý phiên bản phân tán (Distributed Version Control System – DVCS).
2. **Git làm được gì?**
- Git giúp mỗi máy tính có thể lưu trữ nhiều phiên bản khác nhau của một mã nguồn được nhân bản (clone) từ một kho chứa mã nguồn (repository), mỗi thay đổi vào mã nguồn trên máy tính sẽ có thể ủy thác (commit) rồi đưa lên máy chủ nơi đặt kho chứa chính. Và một máy tính khác (nếu họ có quyền truy cập) cũng có thể clone lại mã nguồn từ kho chứa hoặc clone lại một tập hợp các thay đổi mới nhất trên máy tính kia.
3. **Các khái niệm cơ bản**
- Repository: Kho lưu trữ, là nơi lưu trữ mã nguồn và một người khác có thể sao chép (clone) lại mã nguồn đó nhằm làm việc
- Có hai loại Repo là Local Repo và Remote Repo  
4. **Object Storage là gì?**
- Object Storage hay còn gọi là lưu trữ đối tượng, là hình thức lưu trữ dữ liệu dưới dạng các đơn vị riêng biệt. Các đối tượng được lưu trữ trong một kho duy nhất, và sẽ không được tập hợp lại thành tệp bên trong một thư mục nằm trong các thư mục khác. Object Storage sử dụng các khung logic để chứa Đối tượng theo cơ chế ngang hàng. Mỗi object bao gồm dữ liệu của chính nó, metadata và ID định danh.
- Use cases: Lưu trữ, truyền tải website tĩnh, lưu trữ media (Video, hình ảnh, audio), ...
5. **Index**
- Là một khu vực trung gian giữa các thay đổi bạn đã thực hiện và việc commit chúng vào kho lưu trữ. Tất cả các thay đổi đều được thêm vào Index trước khi commit. Index cho phép xác định những thay đổi nào sẽ được commit bằng cách sử dụng lệnh git add. Các thay đổi đã sẵn sàng để commit sẽ được hiển thị với màu xanh lá cây trong index, trong khi những thay đổi chưa sẵn sàng sẽ có màu đỏ
6. **Branch**
- Nhánh là một hướng phát triển riêng biệt của dự án, cho phép phát triển các tính năng mới, sửa lỗi hoặc thử nghiệm mà không làm ảnh hướng đến mã nguồn chính
7. **Git work-flow**
- Git work-flow chỉ ra cách thức setup các loại branchs khác nhau và cách thức để merge chúng lại với nhau
8. **Staging area**
- Như phần Index
9. **Git & Github & Gitlab giống/khác gì nhau?**
- Git là tên gọi của một mô hình hệ thống.
- Github và Gitlab là tên của một công ty cung cấp dịch vụ máy chủ repository công cộng, mỗi người có thể truy cập vào website trang chủ để tạo tài khoản trên đó và tạo ra kho chứa source của riêng mình khi làm việc.
10. **Các trạng thái có thể có trong git repository**
- Untracked: file không được tracking (theo dõi) bởi git
- Modified: file đã được sửa đổi hay commit
- Unmodified: file chưa được sửa đổi
- Stage: đánh dấu để chuẩn bị lưu file vào cơ sở dữ liệu
## Config cho git với git config
1. **Config thông tin cá nhân**
- Đặt tên người dùng: git config --global user.name <tên>
- Đặt email: git config --user.email <địa chỉ mail>
2. **Config default/prefered editor**
- git config --global core.editor "code --wait" (Dùng VS Code làm git text editor)
- git config --global core.editor "vim" (Dùng Vim làm git text editor)
3. **Config global.gitignore**
- Tạo file .gitignore
- Thêm đường dẫn của các tệp ở file .gitignore, Git sẽ không quản lý những tệp đó nữa.
## Các thao tác ban đầu
1. **Khởi tạo repo với git init**<br>
![khoitaorepo](HoangDung/image/image.png)
2. **Clone repo với git repo**<br>
![clonerepo](HoangDung/image/image-1.png)
## Git commit
![gitcommit](HoangDung/image/image-2.png)
1. **Commit trong git là gì**
- Commit là lệnh trong Git giúp lưu lại "snapshot" của các thay đổi trong thư mục làm việc, làm cho chúng trở thành một phần của lịch sử phiên bản của dự án. Điều này giúp theo dõi lịch sử thay đổi, quay lại các phiên bản trước đó và làm cho quy trình làm việc nhóm trở nên dễ dàng hơn.
2. **Một commit chứa những thông tin gì**
- SHA-1 Hash: Mỗi commit có một mã định danh duy nhất được tạo bằng thuật toán SHA-1.
- Author: Thông tin về người tạo ra commit, bao gồm tên và địa chỉ email.
- Committer: Thông tin về người thực hiện commit, có thể giống hoặc khác với tác giả.
- Timestamp: Thời gian tạo ra commit.
- Commit Message: Mô tả ngắn gọn về những thay đổi trong commit.
- Parent Commits: Tham chiếu đến commit trước đó.
- Tree Object: Chỉ đến thư mục gốc của dự án tại thời điểm commit.
- Changes (Diffs): Sự khác biệt giữa trạng thái hiện tại và trạng thái của commit trước đó.
3. **Amending**
- Sửa đổi commit message: git commit --amend
- Thêm hoặc bớt các thay đổi vào commit: 
   Thêm các thay đổi vào staging area: git add (file1) (file2)
   Sử dụng lệnh git commit --amend để thêm các thay đổi vào commit cuối cùng
## Làm việc với branch
1. **Tạo mới branch**
- Để tạo một nhánh mới, có thể sử dụng lệnh 'git branch (tên nhánh)' hoặc tạo và nhảy đến nhánh vừa tạo luôn:'git checkout -b (tên nhánh)'<br>
![taonhanh](HoangDung/image/image-3.png)
2. **Nhảy từ branch này sang branch khác**
- Sử dụng lệnh 'git checkout (tên nhánh)'<br>
![doinhanh](HoangDung/image/image-4.png)
3. **Đổi tên branch**
- Khi đang trong branch, có thể đổi tên branch bằng lệnh: 'git branch -m (tên nhánh mới)'
- Đổi tên branch khác (không phải branch hiện tại): 'git branch -m old-branch-name new-branch-name'<br>
![doiten](HoangDung/image/image-5.png)
## Git checkout
**Checkout đến commit ID**
- Checkout đến commit 'hd123': 'git checkout hd123'
## Các thao tác liên quan khác
1. **Git Merge**
- Chuyển tới branch muốn merge rồi sử dụng lệnh: 'git merge (branch muốn merge)'.<br>
![merge](HoangDung/image/image-6.png)
2. **Git Rebase**
- Lệnh 'git rebase' được sử dụng để di chuyển (rebase) các commit của branch hiện tại lên trên các branch khác, thường là branch mục tiêu mà bạn muốn hợp nhất vào.
- Để rebase branch hiện tại lên trên branch 'main':
   'git checkout (tên nhánh hiện tại)'
   'git rebase main'
3. **Git Rebase interactive**
- Lệnh 'git rebase --interactive' cho phép bạn chỉnh sửa lịch sử commit trước khi áp dụng chúng lên branch mục tiêu. Điều này cho phép bạn thay đổi thứ tự commit, gộp commit lại với nhau, sửa commit message, hoặc thậm chí loại bỏ các commit mà bạn không muốn áp dụng. 
- Lệnh: 'git rebase -i (tên branch muốn rebase lên)'
4. **Git Rebase khi remote có cập nhật mới**
- Khi đang rebase và remote repository có cập nhật mới (có thêm commit mới trên branch mà mình đang cố gắng rebase lên), sẽ có một số vấn đề cần xử lý:
- Xung đột commit: Nếu các commit trên branch mục tiêu (vào đầu của nó) bị thay đổi hoặc xóa bởi các commit từ remote repository, sẽ gặp phải xung đột khi thực hiện rebase. Git sẽ dừng lại và yêu cầu giải quyết xung đột trước khi tiếp tục.
- Cách giải quyết: Có thể giải quyết xung đột bằng cách sử dụng git rebase --continue, git rebase --skip (để bỏ qua commit gây xung đột), hoặc git rebase --abort (để hủy bỏ rebase và quay trở lại trạng thái trước khi rebase).
- Sử dụng 'git rebase' khi remote có cập nhật mới:
   Lấy và cập nhật remote repository:
   'git fetch origin'
   'git checkout (nhánh cần rebase)'
   'git rebase main'
- Nếu có xung đột, Git sẽ dừng lại và yêu cầu giải quyết chúng, sử dụng 'git rebase --continue' để tiếp tục rebase sau khi giải quyết xung đột

5. **Git Cherry-pick**
- Lệnh 'git cherry-pick' được sử dụng để chọn một commit cụ thể từ một branch và áp dụng nó vào branch hiện tại.
- 'git cherry-pick hd123' với hd123 là commit-hash
6. **Git Pull**
- 'git pull (remote) (branch)'
- Ví dụ: 'git pull origin main': kéo và hợp nhất các thay đổi từ branch main của remote origin vào branch hiện tại<br>
![Pull](HoangDung/image/image-7.png)
7. **Git Push**
- 'git push (remote) (branch)'
- Ví dụ: 'git push origin main'<br>
![Push](HoangDung/image/image-8.png)
8. **Git Reset**
- Lệnh git reset được sử dụng để thay đổi vị trí HEAD và/hoặc thay đổi lịch sử commit.
- Các tùy chọn thường dùng:
'--soft': Giữ nguyên thay đổi trong working directory, thay đổi chỉ được loại bỏ khỏi staged area.
'--mixed': Loại bỏ các thay đổi trong staged area, thay đổi chỉ giữ nguyên trong working directory.
'--hard': Xóa thay đổi trong cả staged area và working directory.
- Ví dụ: 'git reset (commit-hash) --hard'
- 'git reset hd123 --hard'
## Các thao tác với git remote
1. **Add remote**
- 'git remote add (tên remote) (url của remote)'
- 'git remote -v' để xem danh sách các remote repo đã được thêm <br>
![remote-v](HoangDung/image/image-9.png)
2. **Rename remote**
-'git remote rename (tên cũ) (tên mới)'
3. **Update remote**
## Các thao tác với Stash
1. **Tổng quan về git stash**
- Git stash là một tính năng trong Git cho phép bạn tạm thời lưu trữ các thay đổi chưa được commit và chưa được stage (staged) mà không cần phải commit chúng vào lịch sử commit của repository.
- git stash list: Xem danh sách các stash
- git stash clear: Xóa tất cả các stash
- git stash --help: Trợ giúp lệnh stash
## Git với giao diện đồ họa, Git GUI
1. **Git Kraken**
- GitKraken là một công cụ GUI Git phổ biến với giao diện đẹp và nhiều tính năng hữu ích.
- Hỗ trợ đồ họa thân thiện, giúp quản lý branches, commit, merge, và các hoạt động Git khác một cách trực quan.
- Cung cấp tính năng hợp nhất (integration) với GitHub, GitLab, Bitbucket và các dịch vụ Git khác.
![GITKRAKEN](HoangDung/image/image-10.png)