## **Tổng quan về Ansible**

## **Giới thiệu về Ansible**
- Ansible giúp cấu hình "nhiều" server theo tùy biến rất đa dạng, giảm thiểu thời gian thao tác trên từng server được cài đặt<br>
![alt text](../image/ansible1.png)
1. **Khái niệm cơ bản**
- Agentless: Ansible không yêu cầu cài đặt bất kỳ phần mềm phụ thuộc nào trên các máy mục tiêu. Thay vào đó, nó sử dụng SSH để kết nối và quản lý các máy này.
- Declarative: Người dùng định nghĩa trạng thái mong muốn của hệ thống, và Ansible sẽ thực hiện các tác vụ cần thiết để đạt được trạng thái đó.
- YAML: Các tác vụ và cấu hình trong Ansible được định nghĩa bằng các tệp YAML, giúp dễ đọc và dễ hiểu.
2. **Các thành phần chính**
- Playbooks: Là tập hợp các kịch bản (scripts) được viết bằng YAML. Playbooks định nghĩa các tác vụ cần thực hiện trên các máy mục tiêu.
- Inventories: Là danh sách các máy mục tiêu (hosts) mà Ansible sẽ quản lý. Inventories có thể được định nghĩa tĩnh bằng các tệp hoặc động bằng các scripts.
- Modules: Là các đơn vị chức năng mà Ansible sử dụng để thực hiện các tác vụ như cài đặt phần mềm, sao chép tệp, quản lý người dùng, và nhiều hơn nữa. Ansible có hàng trăm modules tích hợp sẵn.
- Roles: Là cách tổ chức các playbook, variables, và các file khác liên quan thành các đơn vị có thể tái sử dụng.
- Tasks: Là các tác vụ cụ thể được định nghĩa trong một playbook.
3. **Cách hoạt động**
- Ansible hoạt động theo cơ chế push-based, tức là Ansible server sẽ đẩy các cấu hình và tác vụ tới các máy mục tiêu thông qua SSH. Dưới đây là cách hoạt động cơ bản của Ansible:
- Kết nối đến các máy mục tiêu: Ansible sử dụng SSH để kết nối tới các máy mục tiêu.
- Thực thi các modules: Ansible chuyển các modules (được viết bằng Python) tới các máy mục tiêu và thực thi chúng.
- Thu thập kết quả: Ansible thu thập kết quả thực thi từ các máy mục tiêu và báo cáo lại cho người dùng.
4. **Ưu điểm của Ansible**
- Dễ sử dụng: Ansible sử dụng YAML để định nghĩa cấu hình, giúp dễ đọc và viết.
- Agentless: Không cần cài đặt phần mềm phụ thuộc trên các máy mục tiêu.
- Mạnh mẽ và linh hoạt: Hỗ trợ nhiều nền tảng và có thể thực hiện nhiều loại tác vụ khác nhau.
- Mã nguồn mở: Ansible là công cụ mã nguồn mở, miễn phí và có cộng đồng hỗ trợ lớn.

## **Cài đặt Ansible**
1. Cài đặt ansible trên centos:
<pre>
# Cài đặt trên centos
yum install epel-release
yum install ansible
</pre>
2. Tạo file `hosts`:
`vi /etc/ansible/hosts` <br>
- Cấu hình file:
<pre>
[local]
127.0.0.1
</pre>
3. Chạy thử
<pre>
$ ansible all -m ping
127.0.0.1 | success >> {
    "changed": false,
    "ping": "pong"
}
</pre>
`ansible all -m ping -s -k -u vagrant`
- all: Sử dụng máy chủ được liệt kê trong inventory file
- -m ping: Sử dụng lệnh ping
- -s: sử dụng "sudo" để chạy lệnh
- -k: Yêu cầu mật khẩu thay vì sử dụng xác thực dựa trên khóa
- -u vagrant: Đăng nhập vào máy chủ bằng user vagrant

## **Modules**
- Ansible sử dụng modules để thực hiện hầu hết các tác vụ của mình. Modules có thể thực hiện những việc như cài đặt phần mềm, sao chép tệp, sử dụng mẫu, ...
- Mô-đun là cách sử dụng Ansible vì chúng có thể sử dụng ngữ cảnh có sẵn để xác định hành động nào, nếu có, cần thực hiện để hoàn thành nhiệm vụ.

## **Playbook**
- Playbooks có thể chạy multiple tasks - nhiều tác vụ và cung cấp một số chức năng nâng cao hơn. Có thể chuyển các tác vụ trên vào 'playbook'
- Tạo file nginx.yml:
<pre>
---
- hosts: local
  tasks:
   - name: Install Nginx
     yum: pkg=nginx state=installed update_cache=true
</pre>
- Chạy nó bằng lệnh `ansible-playbook`:
<pre>
$ ansible-playbook -s nginx.yml

PLAY [local] ******************************************************************

GATHERING FACTS ***************************************************************
ok: [127.0.0.1]

TASK: [Install Nginx] *********************************************************
ok: [127.0.0.1]

PLAY RECAP ********************************************************************
127.0.0.1                  : ok=2    changed=0    unreachable=0    failed=0
</pre>

## **Handlers**
- Handlers giống như một task, nhưng nó sẽ chạy khi một task khác gọi, Handler sẽ thực hiện một hành động khi được gọi bởi một sự kiện mà nó lắng nghe.
- Sửa đổi file nginx.yml:
<pre>
---
- hosts: local
  tasks:
   - name: Install Nginx
     yum: pkg=nginx state=installed update_cache=true
     notify:
      - Start Nginx

  handlers:
   - name: Start Nginx
     service: name=nginx state=started
</pre>
- Chạy lại playbook: 
<pre>
$ ansible-playbook -s nginx.yml

PLAY [local] ******************************************************************

GATHERING FACTS ***************************************************************
ok: [127.0.0.1]

TASK: [Install Nginx] *********************************************************
ok: [127.0.0.1]

NOTIFIED: [nginx | Start Nginx] ***********************************************
ok: [127.0.0.1]

PLAY RECAP ********************************************************************
127.0.0.1                  : ok=2    changed=0    unreachable=0    failed=0
</pre>

**Thêm các tasks mới**
- Thêm tasks vào playbook để tìm hiểu:
<pre>
---
- hosts: local
  vars:
   - docroot: /var/www/serversforhackers.com/public
  tasks:
   - name: Add Nginx Repository
     yum_repository: repo='ppa:nginx/stable' state=present
     register: ppastable

   - name: Install Nginx
     yum: pkg=nginx state=installed update_cache=true
     when: ppastable|success
     register: nginxinstalled
     notify:
      - Start Nginx

   - name: Create Web Root
     when: nginxinstalled|success
     file: dest={{ '{{' }} docroot {{ '}}' }} mode=775 state=directory owner=www-data group=www-data
     notify:
      - Reload Nginx

  handlers:
   - name: Start Nginx
     service: name=nginx state=started

    - name: Reload Nginx
      service: name=nginx state=reloaded
</pre>
- Qua file cấu hình trên ta thấy có 3 nhiệm vụ:
    - Add Nginx Repository: Thêm PPA ổn định của Ngĩn để có phiên bản ổn định mới nhất của Nginx bằng cách sử dụng module yum_repository
    - Instal Nginx: Cài đặt Nginx bằng module yum
    - Create Web Root: Tạo thư mục gốc của web

## **Roles** ##
- Vai trò (Roles) hữu ích cho việc tổ chức nhiều tasks và đóng gói dữ liệu cần thiết để hoàn thành các tasks đó.
- Phần cấu hình thường yêu cầu dữ liệu bổ sung như biến, tệp, mẫu động và nhiều hơn nữa. Các công cụ này có thể được sử dụng với Playbook, nhưng chúng ta có thể làm tốt hơn ngay lập tức bằng cách sắp xếp các Nhiệm vụ và dữ liệu liên quan thành một cấu trúc thống nhất
- Roles có cấu trúc thư mục như sau:
<pre>
rolename
 - files
 - handlers
 - meta
 - templates
 - tasks
 - vars
</pre>
- Trong mỗi thư mục, Ansible sẽ tự động tìm kiếm và đọc bất kỳ tệp yaml nào có tên `main.yml`
