# Linux là gì?

Linux là một dòng hệ điều hành mạnh mẽ và linh hoạt, được sử dụng và chia sẻ miễn phí. Nó được tạo ra bởi một người tên là Linus Torvalds vào năm 1991. Điều thú vị là bất kỳ ai cũng có thể xem hệ thống hoạt động như thế nào vì mã nguồn của nó được mở cho mọi người khám phá và sửa đổi. Sự cởi mở này khuyến khích mọi người từ khắp nơi trên thế giới làm việc cùng nhau và làm cho Linux ngày càng tốt hơn.

# Kiến trúc của Linux
<p align="center">
  <img src="../Image/image-16.png" alt="Các thành phần trong kiến trúc linux" />
</p>

## 1. Kernel
Kernel là cốt lõi của hệ điều hành dựa trên Linux. Nó ảo hóa các tài nguyên phần cứng chung của máy tính để cung cấp cho mỗi tiến trình các tài nguyên ảo của nó. Điều này làm cho quá trình này có vẻ như là quá trình duy nhất đang chạy trên máy. Kernel cũng chịu trách nhiệm ngăn ngừa và giảm thiểu xung đột giữa các tiến trình khác nhau.

## 2. System Library
Linux sử dụng các thư viện hệ thống, còn được gọi là thư viện dùng chung, để triển khai các chức năng khác nhau của hệ điều hành. Các thư viện này chứa mã viết sẵn mà ứng dụng có thể sử dụng để thực hiện các tác vụ cụ thể. Bằng cách sử dụng các thư viện này, nhà phát triển có thể tiết kiệm thời gian và công sức vì họ không cần phải viết lại cùng một mã. Thư viện hệ thống hoạt động như một giao diện giữa các ứng dụng và kernel, cung cấp một cách thức chuẩn hóa và hiệu quả để các ứng dụng tương tác với hệ thống cơ bản.

## 3. Shell
Shell là giao diện người dùng của Hệ điều hành Linux. Nó cho phép người dùng tương tác với hệ thống bằng cách nhập các lệnh mà shell sẽ diễn giải và thực thi. Shell đóng vai trò là cầu nối giữa người dùng và kernel, chuyển tiếp các yêu cầu của người dùng đến kernel để xử lý. Nó cung cấp một cách thuận tiện để người dùng thực hiện các tác vụ khác nhau, chẳng hạn như chạy chương trình, quản lý tệp và định cấu hình hệ thống.

## 4. Hardware Layer
Lớp phần cứng bao gồm tất cả các thành phần vật lý của máy tính, chẳng hạn như RAM (Bộ nhớ truy cập ngẫu nhiên), HDD (Ổ đĩa cứng), CPU (Bộ xử lý trung tâm) và các thiết bị đầu vào/đầu ra. Lớp này chịu trách nhiệm tương tác với Hệ điều hành Linux và cung cấp các tài nguyên cần thiết để hệ thống và ứng dụng hoạt động bình thường. Nhân Linux và các thư viện hệ thống cho phép giao tiếp và kiểm soát các thành phần phần cứng này, đảm bảo rằng chúng hoạt động hài hòa với nhau.

# Help with Commands

## On using `man` with `less`
- Space, f: Page forward
- b: Page backward
- <: Go to first line of file
- >: Go to last line of file
- /: Search forward (n to repeat)
- ?: Search backward (N to repeat)
- h: Display help
- q: Quit help

## I/O redirection with pipes
Nhiều lệnh Linux in ra “đầu ra tiêu chuẩn”, mặc định là màn hình thiết bị đầu cuối. Ký tự `|` (pipe) có thể được sử dụng để chuyển hướng hoặc “chuyển hướng” đầu ra sang chương trình hoặc bộ lọc khác:
- `w`: Show who’s logged on
- `w | less`: Pipe into the ‘less’ pager
- `w | grep 'tuta'`: Pipe into grep, print lines containing ‘tuta’
- `w | grep -v 'tuta'`: Print only lines not containing ‘tuta’
- `w | grep 'tuta' | sed s/tuta/scholar/g`: Replace all ‘tuta’ with ‘scholar’

# The Filesystem

## The Linux File System
<p align="center">
  <img src="../Image/image-29.png" alt="The linux filesystem" />
</p>

Các lệnh điều hướng cần thiết:
- `pwd`: In thư mục hiện tại
- `ls`: Danh sách tập tin
- `cd`: Thay đổi thư mục

Chúng ta sử dụng tên đường dẫn để chỉ các tệp và thư mục trong hệ thống tệp Linux.
Có hai loại tên đường dẫn:
- Tuyệt đối: Đường dẫn đầy đủ tới một thư mục hoặc tập tin; bắt đầu với `/`
- Tương đối: Một phần đường dẫn có liên quan đến thư mục làm việc hiện tại; không bắt đầu bằng `/`

Các ký tự đặc biệt:
- `~`: Thư mục chính của bạn (ví dụ: `/usr1/tutorial/tuta1`)
- `.`: Thư mục hiện tại
- `..`: Thư mục mẹ
- `*`: Ký tự đại diện khớp với bất kỳ tên tệp nào
- `?`: Ký tự đại diện khớp với bất kỳ ký tự nào
- `TAB`: Cố gắng hoàn thành tên tệp (được gõ một phần)

## The `ls` command
- `ls -a`: Liệt kê tất cả các tệp, bao gồm các tệp ẩn bắt đầu bằng “.”
- `ls -ld *`: Liệt kê chi tiết về một thư mục chứ không phải nội dung của nó
- `ls -F`: Đặt ký tự chỉ báo ở cuối mỗi tên
- `ls -l`: Danh sách dài đơn giản
- `ls -lR`: Danh sách dài đệ quy
- `ls -lh`: Cung cấp kích thước tệp mà con người có thể đọc được
- `ls -lS`: Sắp xếp tệp theo kích thước tệp
- `ls -lt`: Sắp xếp tập tin theo thời gian sửa đổi (rất hữu ích!)

## Một số câu lệnh hữu ích
- `cp [file1] [file2]`: Sao chép tập tin
- `mkdir [tên]`: Tạo thư mục
- `rmdir [tên]`: Xóa thư mục (trống)
- `mv [file] [đích]`: Di chuyển/đổi tên tập tin
- `rm [file]`: Xóa (-r đối với đệ quy)
- `file [file]`: Xác định loại tập tin
- `less [tệp]`: Trang qua tệp
- `head -n N [file]`: Hiển thị N dòng đầu tiên
- `tail -n N [file]`: Hiển thị N dòng cuối cùng
- `ln -s [file] [mới]`: Tạo liên kết tượng trưng
- `cat [file] [file2…]`: Hiển thị (các) tệp
- `tac [file] [file2…]`: Hiển thị tập tin theo thứ tự ngược lại
- `touch [file]`: Thời gian sửa đổi cập nhật
- `od [tập tin]`: Hiển thị nội dung tập tin, đặc biệt là nhị phân

# Editors

## Regular Expressions
Nhiều công cụ Linux, chẳng hạn như `grep` và `sed`, sử dụng các chuỗi mô tả chuỗi ký tự. Những chuỗi này được gọi là Regular expressions. Dưới đây là một số ví dụ:
- `^foo`: Dòng bắt đầu bằng “foo”
- `bar$`: Dòng kết thúc bằng “bar”
- `[0-9]\{3\}`: Số có 3 chữ số
- `.*a.*e.*i.*o.*u.*`: Từ có nguyên âm theo thứ tự*

## File Editors
- **gedit**: Trình soạn thảo giống Notepad với một số tính năng lập trình (ví dụ: tô sáng cú pháp). Yêu cầu X-Windows.
- **nano**: Trình chỉnh sửa nhẹ. Non-Xwindows.
- **vim**: Phiên bản tốt hơn của ‘vi’ (trình chỉnh sửa toàn màn hình sớm). Rất nhanh chóng, hiệu quả. Phổ biến trong số các lập trình viên hệ thống. Thiết bị đầu cuối hoặc X-Windows.
