# Openstack là gì ?
OpenStack là nền tảng mã nguồn mở miễn phí cung cấp một Frame để xây dựng và quản lý cơ sở hạ tầng public cloud và private cloud.
OpenStack cung cấp cơ sở hạ tầng như một dịch vụ, cấu hình và quản lý một lượng lớn tài nguyên điện toán, storage và network. Các tài nguyên này, bao gồm metal hardware, máy ảo (VM), được quản lý thông qua giao diện lập trình ứng dụng (API) và bảng thông tin OpenStack.
Các tổ chức và nhà cung cấp dịch vụ có thể triển khai OpenStack tại chỗ (tạo private cloud trong data center), OpenStack. Trong cloud để kích hoạt hoặc cung cấp năng lượng cho các nền tảng đám mây và các hệ thống network.

OpenStack không phải là một ứng dụng theo nghĩa truyền thống, nhưng nó là một nền tảng được tạo thành từ hàng chục thành phần độc lập được gọi là projects, hoạt động cùng nhau thông qua các API. Các tổ chức chỉ có thể cài đặt các thành phần được chọn để tạo ra các tính năng và chức năng mong muốn trong môi trường đám mây. OpenStack cũng dựa vào hai công nghệ nền tảng: hệ điều hành cơ bản (như Linux) và nền tảng ảo hóa (như VMware hoặc Citrix), một hệ điều hành xử lý các hướng dẫn và dữ liệu được trao đổi từ OpenStack, trong khi công cụ ảo hóa quản lý các tài nguyên phần cứng ảo được sử dụng bởi projects OpenStack.
# Các thành phần trong openstack
<p align="center">
  <img src="../Image/image-15.png" alt="Các thành phần trong openstack" />
</p>

Một số yếu tố chính: compute (Nova), VM images (Glance), networking (neutron), storage (Cinder hoặc Swift), identity management (Keystone) và resource management (vị trí).

## 1.1 Openstack networking 
OpenStack Networking xử lý việc tạo và quản lý cơ sở hạ tầng mạng ảo trong đám mây OpenStack. Các thành phần cơ sở hạ tầng bao gồm mạng, mạng con và bộ định tuyến. Bạn cũng có thể triển khai các dịch vụ nâng cao như tường lửa hoặc mạng riêng ảo (VPN).
Các lợi thế của Mạng OpenStack bao gồm:
*Người dùng có thể tạo mạng, kiểm soát lưu lượng và kết nối máy chủ và thiết bị với một hoặc nhiều mạng.
*Các mô hình mạng linh hoạt có thể thích ứng với khối lượng và thời gian thuê mạng.
*Địa chỉ IP có thể được dành riêng hoặc nổi, trong đó IP nổi có thể được sử dụng để định tuyến lại lưu lượng truy cập động.
*Nếu sử dụng mạng Vlan, bạn có thể sử dụng tối đa 4094 Vlan (4094 mạng), trong đó 4094 = 2^12 (trừ 2 không sử dụng được), địa chỉ này bị áp đặt bởi giới hạn tiêu đề 12 bit.
*Nếu sử dụng mạng dựa trên đường hầm VXLAN, VNI (Mã định danh mạng ảo) có thể sử dụng tiêu đề 24 bit, về cơ bản sẽ cho phép khoảng 16 triệu địa chỉ/mạng duy nhất.
## 1.2 Storage
### 1.2.1 Openstack block storage (cinder)
OpenStack Block Storage cung cấp khả năng quản lý lưu trữ khối liên tục cho ổ cứng ảo. Block Storage cho phép người dùng tạo và xóa các thiết bị khối cũng như quản lý việc đính kèm các thiết bị khối vào máy chủ.
Block Storage được cấu hình bằng API Block Storage REST.
<p align="center">
  <img src="../Image/image-17.png" alt="Cinder storage overview" />
</p>
Thành phần chi tiết của Cinder:

1. cinder-api:
Chức năng: Đóng vai trò là điểm đầu vào cho các yêu cầu API. Nó xử lý các lệnh gọi API REST và định tuyến chúng đến thành phần thích hợp.
Giao thức: Hỗ trợ HTTP/HTTPS.
Điểm cuối: Cung cấp điểm cuối để tạo, xóa, đính kèm, tách khối, quản lý ảnh chụp nhanh, v.v.
2. cinder-scheduler:
Chức năng: Xác định storage backend tối ưu để tạo volumes dựa trên một bộ bộ lọc (filters) và weighers có thể định cấu hình.
3. cinder-volume
Chức năng: Quản lý việc tạo, xóa volume và các hoạt động khác trong vòng đời. Nó tương tác trực tiếp với các phụ trợ lưu trữ.
4. cinder-backup
Chức năng: Quản lý sao backups of volumes. Có thể lưu trữ các bản sao lưu trong nhiều hệ thống lưu trữ khác nhau (ví dụ: Swift, Ceph, NFS).
### 1.2.2 Object storage (swift)
The Object Storage (swift) (swift) triển khai một kho lưu trữ object/blob nhất quán, được phân phối và có tính sẵn sàng cao, có thể truy cập được thông qua HTTP/HTTPS.
<p align="center">
  <img src="../Image/image-18.png" alt="Swift storage overview" />
</p>
Dịch vụ proxy swift được khách hàng truy cập thông qua bộ cân bằng tải trên mạng quản lý (br-mgmt). Dịch vụ proxy swift giao tiếp với các dịch vụ Account, Container, and Object trên máy chủ Object Storage thông qua mạng lưu trữ (br-storage). Việc sao chép giữa các máy chủ Object Storage được thực hiện thông qua mạng sao chép (br-repl).

### 1.2.3 Glance
• Glance là dịch vụ quản lý hình ảnh
• Mỗi VM được khởi tạo từ một hình ảnh
bao gồm một hệ điều hành cụ thể được cài đặt sẵn
• Glance quản lý bộ sưu tập các mẫu VM như vậy
• Hình ảnh có thể được tùy chỉnh, ví dụ: một máy chủ web
hình ảnh đã cài đặt sẵn gói máy chủ web
• Các thành phần phụ của Glance là: Glance (đối với hình ảnh
quản lý) và lưu trữ lướt qua (để lưu trữ
sự quản lý)
• Lưu trữ nhanh hỗ trợ các tùy chọn lưu trữ khác nhau
<p align="center">
  <img src="../Image/image-19.png" alt="Swift storage overview" />
</p>

### 1.2.4 OpenStack Database-as-a-Service (trove)
Cơ sở dữ liệu dưới dạng dịch vụ OpenStack cho phép người dùng lựa chọn, cung cấp và vận hành nhiều loại cơ sở dữ liệu quan hệ và phi quan hệ, đồng thời xử lý ngay các tác vụ quản trị cơ sở dữ liệu phức tạp hơn.

*Các lợi ích của Cơ sở dữ liệu dưới dạng dịch vụ OpenStack bao gồm:

* Người dùng và quản trị viên cơ sở dữ liệu có thể cung cấp và quản lý nhiều phiên bản cơ sở dữ liệu trên đám mây.
* Cách ly tài nguyên hiệu suất cao đồng thời tự động hóa các tác vụ quản trị phức tạp như triển khai, cấu hình, vá lỗi, sao lưu, khôi phục và giám sát.
Trove components: 
| Thành phần                       | Mô tả |
|----------------------------------|-------|
| openstack-trove-api              | API RESTful hỗ trợ JSON và XML để cung cấp và quản lý các phiên bản Cơ sở dữ liệu dưới dạng dịch vụ. |
| openstack-trove-conductor        | Chạy trên máy chủ để nhận tin nhắn từ các phiên bản khách có yêu cầu cập nhật thông tin trên máy chủ. Các yêu cầu có thể bao gồm trạng thái của một phiên bản hoặc trạng thái hiện tại của bản sao lưu. Với openstack-trove-conductor, các phiên bản khách không cần kết nối trực tiếp tới cơ sở dữ liệu máy chủ. Dịch vụ lắng nghe các tin nhắn RPC thông qua bus tin nhắn và thực hiện thao tác được yêu cầu. |
| openstack-trove-guestagent       | Chạy trên phiên bản khách để quản lý và thực hiện các thao tác trực tiếp trên cơ sở dữ liệu máy chủ. Openstack-trove-guestagent lắng nghe các tin nhắn RPC thông qua bus tin nhắn và thực hiện thao tác được yêu cầu. |
| openstack-trove-taskmanager      | Chịu trách nhiệm về các tác vụ như cung cấp phiên bản, quản lý vòng đời của phiên bản và các hoạt động trên phiên bản cơ sở dữ liệu. |
| trove                            | Ứng dụng khách dòng lệnh để truy cập API Cơ sở dữ liệu dưới dạng dịch vụ. |

<p align="center">
  <img src="../Image/image-20.png" alt="The relationship between the Compute services and other OpenStack components." />
</p>

## 1.3 Virtual Machines, Images, and Templates
### 1.3.1 OpenStack Compute (nova)
OpenStack Computing đóng vai trò là cốt lõi của đám mây OpenStack bằng cách cung cấp các máy ảo theo yêu cầu. Tính toán lên lịch cho các máy ảo chạy trên một tập hợp các nút bằng cách xác định trình điều khiển tương tác với các cơ chế ảo hóa cơ bản và bằng cách hiển thị chức năng cho các thành phần OpenStack khác.
Tính toán hỗ trợ trình điều khiển libvirt libvirtd sử dụng KVM làm trình ảo hóa (the hypervisor). Trình ảo hóa tạo các máy ảo và cho phép di chuyển trực tiếp từ nút này sang nút khác.
Điện toán tương tác với dịch vụ Nhận dạng để xác thực quyền truy cập phiên bản và cơ sở dữ liệu, với dịch vụ Hình ảnh để truy cập hình ảnh và khởi chạy phiên bản, cũng như với dịch vụ bảng điều khiển để cung cấp giao diện quản trị và người dùng.
Nova components:
| Thành phần                       | Mô tả |
|----------------------------------|-------|
| openstack-nova-api               | Xử lý các yêu cầu và cung cấp quyền truy cập vào các dịch vụ Điện toán, chẳng hạn như khởi động một phiên bản. |
| openstack-nova-cert              | Cung cấp trình quản lý chứng chỉ. |
| openstack-nova-compute           | Chạy trên mỗi nút để tạo và chấm dứt các phiên bản ảo. Dịch vụ điện toán tương tác với bộ ảo hóa để khởi chạy các phiên bản mới và đảm bảo rằng trạng thái phiên bản được duy trì trong cơ sở dữ liệu Điện toán. |
| openstack-nova-conductor         | Cung cấp hỗ trợ truy cập cơ sở dữ liệu cho các nút điện toán để giảm rủi ro bảo mật. |
| openstack-nova-consoleauth       | Xử lý xác thực bảng điều khiển. |
| openstack-nova-network           | Các dịch vụ mạng có thể đóng vai trò thay thế cho Mạng OpenStack và xử lý lưu lượng mạng cơ bản để truy cập riêng tư và công cộng. Để so sánh Mạng OpenStack và Mạng điện toán, hãy xem Chương 2, Mạng chuyên sâu. |
| openstack-nova-novncproxy        | Cung cấp proxy VNC cho trình duyệt để cho phép bảng điều khiển VNC truy cập vào máy ảo. |
| openstack-nova-scheduler         | Gửi yêu cầu cho các máy ảo mới đến đúng nút dựa trên trọng số và bộ lọc được định cấu hình. |
| nova                             | Ứng dụng khách dòng lệnh để truy cập API điện toán. |
<p align="center">
  <img src="../Image/image-21.png" alt="The relationship between Database-as-a-Service and the other OpenStack services." />
</p>

### 1.3.2 OpenStack Bare Metal Provisioning (ironic)
Bare Metal Provisioning tích hợp với dịch vụ Điện toán để cung cấp the bare metal machines như cách cung cấp máy ảo và cung cấp giải pháp cho trường hợp sử dụng từ bare metal machines cho người thuê đáng tin cậy.
| Thành phần                       | Mô tả |
|----------------------------------|-------|
| openstack-ironic-api             | Xử lý các yêu cầu và cung cấp quyền truy cập vào tài nguyên Điện toán trên nút kim loại trần. |
| openstack-ironic-conductor       | Tương tác trực tiếp với cơ sở dữ liệu phần cứng và Ironic, đồng thời xử lý các hành động được yêu cầu và định kỳ. Bạn có thể tạo nhiều conductor để tương tác với các trình điều khiển phần cứng khác nhau. |
| ironic                           | Ứng dụng khách dòng lệnh để truy cập API cấp phép kim loại trần. |

<p align="center">
  <img src="../Image/image-22.png" alt="how Ironic and the other OpenStack services interact when a physical server is being provisioned" />
</p>
<p align="center">
  <img src="../Image/image-23.png" alt="The Ironic API" />
</p>

### 1.3.4. OpenStack Orchestration (heat)
OpenStack Orchestration cung cấp các mẫu để tạo và quản lý tài nguyên đám mây như bộ nhớ, mạng, phiên bản hoặc ứng dụng. Mẫu được sử dụng để tạo ngăn xếp, là tập hợp các tài nguyên.
| Thành phần                       | Mô tả |
|----------------------------------|-------|
| openstack-heat-api               | API REST gốc OpenStack xử lý các yêu cầu API bằng cách gửi yêu cầu đến dịch vụ openstack-heat-engine qua RPC. |
| openstack-heat-api-cfn           | API AWS-Query tùy chọn tương thích với AWS CloudFormation xử lý các yêu cầu API bằng cách gửi yêu cầu đến dịch vụ openstack-heat-engine qua RPC. |
| openstack-heat-engine            | Điều phối việc khởi chạy mẫu và tạo sự kiện cho người dùng API. |
| openstack-heat-cfntools          | Gói tập lệnh trợ giúp như cfn-hup, xử lý các bản cập nhật siêu dữ liệu và thực thi các hook tùy chỉnh. |
| heat                             | Công cụ dòng lệnh giao tiếp với API điều phối để thực thi API AWS CloudFormation. |

<p align="center">
  <img src="../Image/image-24.png" alt="the main interfaces that the Orchestration service uses to create a new stack of two new instances and a local network." />
</p>

### 1.3.5. OpenStack Data Processing (sahara)
OpenStack Data Processing cho phép cung cấp và quản lý các cụm Hadoop trên OpenStack. Hadoop lưu trữ và phân tích một lượng lớn dữ liệu có cấu trúc và phi cấu trúc theo cụm.
Cụm Hadoop là các nhóm máy chủ có thể hoạt động như máy chủ lưu trữ chạy Hệ thống tệp phân tán Hadoop (HDFS), máy chủ điện toán chạy khung MapReduce (MR) của Hadoop hoặc cả hai.
Các máy chủ trong cụm Hadoop cần nằm trong cùng một mạng nhưng chúng không cần chia sẻ bộ nhớ hoặc ổ đĩa. Do đó, ta có thể thêm hoặc xóa máy chủ và cụm mà không ảnh hưởng đến khả năng tương thích của các máy chủ hiện có.
| Thành phần                       | Mô tả |
|----------------------------------|-------|
| openstack-sahara-all             | Gói kế thừa xử lý các dịch vụ API và công cụ. |
| openstack-sahara-api             | Xử lý các yêu cầu API và cung cấp quyền truy cập vào các dịch vụ Xử lý dữ liệu. |
| openstack-sahara-engine          | Công cụ xử lý các yêu cầu cụm và phân phối dữ liệu. |
| sahara                           | Ứng dụng khách dòng lệnh để truy cập API xử lý dữ liệu. |

<p align="center">
  <img src="../Image/image-25.png" alt="the Data Processing service uses to provision and manage a Hadoop cluster." />
</p>

# 1.4. Identity Management
### 1.4.1. OpenStack Identity (keystone)
• Keystone được OpenStack sử dụng cho
xác thực và ủy quyền cấp cao
• Nó đảm bảo an ninh bằng cách cấp/từ chối quyền truy cập
tới các đối tượng (ví dụ: Máy ảo hoặc Virtual
Networks) cho những Người dùng khác nhau
• Các đối tượng được nhóm thành các dự án,
ủy quyền có thể được cấp cho mỗi dự án
• Keystone được cài đặt trong nút Bộ điều khiển ( Controller node)
• Lần đầu tiên người dùng tương tác với keystone
sử dụng xác thực dựa trên người dùng/pass
• Nếu thành công sẽ nhận được mã thông báo
• Mã thông báo được sử dụng để truy cập tất cả OpenStack
dịch vụ
• Mỗi dịch vụ đảm nhiệm việc xác nhận
mã thông báo
<p align="center">
  <img src="../Image/image-26.png" alt="the Data Processing service uses to provision and manage a Hadoop cluster." />
</p>

# 1.5. User Interfaces
### 1.5.1. OpenStack Dashboard (horizon)
Bảng điều khiển OpenStack cung cấp giao diện người dùng để người dùng và quản trị viên thực hiện các hoạt động như tạo và khởi chạy phiên bản, quản lý mạng và cài đặt kiểm soát truy cập.
| Thành phần                       | Mô tả |
|----------------------------------|-------|
| Bảng điều khiển OpenStack        | Ứng dụng Web Django cung cấp quyền truy cập vào bảng điều khiển từ bất kỳ trình duyệt Web nào. |
| Máy chủ HTTP Apache (dịch vụ httpd) | Lưu trữ ứng dụng. |

<p align="center">
  <img src="../Image/image-27.png" alt="The dashboard architecture." />
</p>

### 1.5.2. OpenStack Telemetry (ceilometer)
OpenStack Telemetry cung cấp dữ liệu sử dụng ở cấp độ người dùng cho các đám mây dựa trên OpenStack. Dữ liệu có thể được sử dụng để thanh toán cho khách hàng, giám sát hệ thống hoặc cảnh báo. Đo từ xa có thể thu thập dữ liệu từ các thông báo được gửi bởi các thành phần OpenStack hiện có như các sự kiện sử dụng tính toán hoặc bằng cách thăm dò các tài nguyên cơ sở hạ tầng OpenStack như libvirt.
• Giám sát tất cả các thành phần của
Ví dụ, đo lường tài nguyên đang được
được sử dụng bởi mỗi người dùng
• Dữ liệu được thu thập bởi Ceilometer có thể được
được sử dụng cho mục đích thanh toán
• Ceilometer  cũng thu thập dữ liệu từ xa
số liệu thống kê có thể được sử dụng để kiểm tra
trạng thái của hệ thống


| Thành phần                               | Mô tả |
|------------------------------------------|-------|
| openstack-ceilometer-alarm-evaluator     | Kích hoạt chuyển đổi trạng thái trên báo động. |
| openstack-ceilometer-alarm-notifier      | Thực hiện các hành động khi cảnh báo được kích hoạt. |
| openstack-ceilometer-api                 | Chạy trên một hoặc nhiều máy chủ quản lý trung tâm để cung cấp quyền truy cập vào dữ liệu trong cơ sở dữ liệu. |
| openstack-ceilometer-central             | Chạy trên máy chủ quản lý trung tâm để thăm dò số liệu thống kê sử dụng về các tài nguyên độc lập với các phiên bản hoặc nút Điện toán. Tác nhân không thể được chia tỷ lệ theo chiều ngang, do đó bạn chỉ có thể chạy một phiên bản duy nhất của dịch vụ này tại một thời điểm. |
| openstack-ceilometer-collector           | Chạy trên một hoặc nhiều máy chủ quản lý trung tâm để giám sát hàng đợi tin nhắn. Mỗi bộ thu thập xử lý và dịch các tin nhắn thông báo thành các tin nhắn Đo từ xa và gửi tin nhắn trở lại bus tin nhắn với chủ đề liên quan. Tin nhắn đo từ xa được ghi vào kho dữ liệu mà không sửa đổi. Bạn có thể chọn nơi chạy các tác nhân này vì tất cả hoạt động liên lạc trong nội bộ tác nhân đều dựa trên lệnh gọi AMQP hoặc REST tới dịch vụ ceilometer-api, tương tự như dịch vụ máy đo độ cao-cảnh báo-đánh giá. |
| openstack-ceilometer-compute             | Chạy trên mỗi nút Điện toán để thăm dò số liệu thống kê sử dụng tài nguyên. Mỗi nút tính toán nova phải triển khai và chạy một tác nhân tính toán ceilometer. |
| openstack-ceilometer-notification        | Đẩy số liệu đến dịch vụ thu thập từ nhiều dịch vụ OpenStack khác nhau. |
| ceilometer                              | Ứng dụng khách dòng lệnh để truy cập API từ xa. |

