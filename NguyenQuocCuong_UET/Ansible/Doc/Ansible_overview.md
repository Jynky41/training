# Ansible là gì ? 
<p align="center">
  <img src="../Image/image-14.png" alt="Ansible tool" />
</p>

Đây là một công cụ mã nguồn mở IaC (Infrastructure as Code) phổ biến trên thị trường hiện nay, hỗ trợ người dùng trong việc tự động hóa việc cài đặt, quản lý các server / thiết bị từ xa:

# Chức năng của Ansible: 
- Provisioning: Ansible cũng có thể khởi tạo VM (Virtual Machine), container hàng loạt trong môi trường cloud thông qua các API (OpenStack, AWS, Google Cloud, Azure…) tương tự như Terraform.
- Configuration Management: Quản lý cấu hình tập trung. Từ đó giúp người dùng không cần phải tốn thời gian cho việc chỉnh sửa cấu hình trên từng server / thiết bị.
- Application Deployment: Deploy ứng dụng hàng loạt, quản lý hiệu quả vòng đời của ứng dụng từ giai đoạn dev cho tới production.
- Security & Compliance: Quản lý các chính sách về an toàn thông tin một cách đồng bộ trên nhiều môi trường và sản phẩm khác nhau (deploy policy, cấu hình firewall,…).

# Cài đặt và thực hành ansible
# Cách xác thực qua SSH:

## 1. Sử dụng user và mật khẩu:
<p align="center">
  <img src="../Image/image.png" alt="Ansible tool" />
</p>

## 2. Sử dụng SSH-key

### 2.1 Không có passphare 

Tạo ssh-key public:
<p align="center">
  <img src="../Image/image-1.png" alt="Tạo ssh-key public" />
</p>

Copy public key từ máy chủ linux chạy ansible (192.168.114.128) sang máy cần remote (192.168.114.130):
<p align="center">
  <img src="../Image/image-2.png" alt="Copy public key" />
</p>

Máy chạy linux với ip: 192.168.114.130:
<p align="center">
  <img src="../Image/image-3.png" alt="Máy chạy linux" />
</p>

### 2.2 Có passphare:
<p align="center">
  <img src="../Image/image-4.png" alt="Có passphare" />
</p>

## 3. Keyconcept trong Ansible

### 3.1 Inventory

Inventory là một file lưu danh sách các host được quản lý bởi ansible, file này nằm tại đường dẫn `/etc/ansible/hosts`. Các hosts có thể được viết tắt ở dạng pattern, hoặc có thể được gom nhóm lại với nhau thành một Group. Ngoài việc lưu các hosts thì file còn lưu một loạt các biến cấu hình cho việc hoạt động của ansible. Và định dạng ini hoặc yaml.
Cấu hình ví dụ:
<p align="center">
  <img src="../Image/image-5.png" alt="Cấu hình Inventory" />
</p>

Trong cấu hình cách khai báo trên với [webserver], [database]. Đây là cách khai báo cho 1 group các server với nhau. [webserver] là tên group.
Test lệnh ping tất cả các host:
<p align="center">
  <img src="../Image/image-6.png" alt="Test lệnh ping" />
</p>

### 3.2 YAML và làm quen với YAML

YAML là một ngôn gnữ cấu hình định dạng dữ liệu. Hầu như tất cả playbooks trong Ansible được viết bằng YAML. Playbook Ansible được viết theo định dạng cụ thể được gọi là YAML. Anh so sánh nhanh dữ liệu mẫu ở ba định dạng khác nhau.
<p align="center">
  <img src="../Image/image-7.png" alt="So sánh YAML" />
</p>

Trong YAML có 3 dạng biểu diễn như sau:
<p align="center">
  <img src="../Image/image-8.png" alt="Biểu diễn YAML" />
</p>

### 3.3 Playbook

Trong playbooks sẽ chứa một tập hợn các activities (hoạt động) hay các tasks (nhiệm vụ) sẽ được chạy trên một hay một nhóm servers. Trong đó task là một hành động duy nhất được thực hiện trên server, ví dụ như cài gói service nào đó, hay bật tắt service. Playbook mẫu trong hình:
<p align="center">
  <img src="../Image/image-9.png" alt="Playbook mẫu" />
</p>

Kết quả chạy playbook (dùng câu lệnh `ansible-playbook <tên file định dạng yml hoặc yaml>`):
<p align="center">
  <img src="../Image/image-10.png" alt="Kết quả chạy playbook" />
</p>

### 3.4 Modules

Một số modules đơn giản và phổ biến thường dùng:
* System: Bao gồm các module như User, Group, Hostname, Systemd, Service, v.v…
* Commands: Thường có module con như Command, Expect, Raw, Script, Shell, v.v…
* Files: Các module làm việc với file như Copy, Find, Lineinfile, Replace, v.v…
* Database: Ansbile cũng support mạnh mẽ những module làm việc với DB như Mongodb, Mssql, Mysql, Postgresql, Proxysql, v.v…
* Cloud: Ansible cũng không quên kết hợp với các dịch vụ cloud nổi tiếng như Amazon, Google, Docker, Linode, VMware, Digital Ocean, v.v…
* Windows: Mạnh mẽ với những module như win_copy, win_command, win_domain, win_file, win_shell

Và còn hàng trăm module khác đã được ansible cung cấp sẵn.

## 4. Role trong ansible

Ansible Roles cung cấp một framework và cấu trúc để thiết lập các tasks, variables, handlers, metadata, templates, và các files khác. Chúng cho phép ta sử dụng lại và chia sẻ Ansible code một cách hiệu quả. Bằng cách này, chúng ta có thể tham chiếu và gọi đến roles ở nhiều playbook với chỉ một vài dòng code trong khi chúng ta có thể sử dụng lại cùng roles trên nhiều projects mà không phải lặp đi lặp lại code.

Ansible sẽ kiểm tra các file main.yaml, các biến trong vars, và nội dung liên quan trong mỗi đường dẫn con. Việc thêm vào những file YAML trong một vài đường dẫn là hoàn toàn có thể. Ví dụ, bạn có thể nhóm các tasks của bạn trong những file YAML riêng biệt tùy theo một vài đặc tính:

* defaults: Bao gồm các giá trị mặc định cho các biến của role. Ở đây chúng ta định nghĩa một vài biến mặc định, nhưng chúng có độ ưu tiên thấp nhất và cũng thường bị ghi đè bởi các phương thức khác để customize role
* files: Chứa các file tĩnh và custom mà role sử dụng để thực hiện một vài tasks nhất định
* handlers: Một tập hợp các handlers mà có thể kích hoạt bởi các tasks của role
* meta: Bao gồm thông tin metadata cho role, nó có thể là các dependencies (phụ thuộc), tác giả, license, nền tảng khả dụng,…
* tasks: Một danh sách các tasks để thực hiện bởi role. Phần này có thể hiểu tương tự như task section trong một playbook
* templates: Bao gồm các file template Jinja2 sử dụng bởi các tasks của role
* tests: Bao gồm các file cấu hình liên quan đến kiểm thử role
* vars: Chứa các biến được định nghĩa cho role (các biến ở đây có độ ưu tiên cao hơn defaults)

Dưới đây là hình ảnh tạo file main.yaml trong file tasks: với hai tasks là ping các hosts và in ra biến password (tạo biến `password=test` password trong file vars/main.yaml):
<p align="center">
  <img src="../Image/image-11.png" alt="Tạo file main.yaml" />
</p>

Chạy file test.yaml, chạy trên host đặt tên database (192.168.144.130) và thực hiện các tasks yêu cầu là ping và in ra biến password:
<p align="center">
  <img src="../Image/image-12.png" alt="Chạy file test.yaml" />
</p>

<p align="center">
  <img src="../Image/image-13.png" alt="Kết quả chạy file test.yaml" />
</p>

