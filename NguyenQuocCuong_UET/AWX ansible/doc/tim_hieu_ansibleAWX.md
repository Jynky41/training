Ansible đã trở thành một lựa chọn được ưa chuộng để quản lý cơ sở hạ tầng và sắp xếp các tác vụ phức tạp trong lĩnh vực tự động hóa CNTT. Ansible AWX, cung cấp giao diện người dùng đồ họa (GUI) và chức năng bổ sung để cải thiện trải nghiệm tự động hóa, là một trong những phần chính của Ansible.
# Ansible AWX là gì?
Ansible AWX là một nền tảng web nguồn mở đóng vai trò là hệ thống điều khiển tập trung cho Ansible. Nó cho phép người dùng quản lý hàng tồn kho, tạo và lên lịch mẫu công việc, và giám sát sổ tay hướng dẫn Ansible từ giao diện web. AWX cung cấp GUI trực quan, giúp người dùng mới bắt đầu và người dùng nâng cao dễ dàng tương tác với Ansible hơn.
# Các lợi ích khác khi sử dụng Ansible AWX so với phiên bản CLI của Ansible là:
* Cung cấp một vị trí trung tâm để chạy sổ tay hướng dẫn Ansible thay vì các máy tính xách tay của kỹ sư khác nhau. Điều này đảm bảo các hoạt động sổ tay hướng dẫn được kiểm tra và nhất quán.
* Nút điều khiển tập trung có thể ghi lại các sự kiện tự động hóa và cũng theo dõi các lần chạy sổ tay hướng dẫn thành công và thất bại.
* Quản lý bảo mật tốt hơn, bạn có thể kết nối AWX với AD hoặc hệ thống quản lý thông tin xác thực khác và cũng phân quyền cho những ai có thể chạy sổ tay hướng dẫn trong khi ẩn bí mật. tức là bạn có thể cấp cho nhân viên bộ phận trợ giúp quyền chạy sổ tay hướng dẫn mà không để họ có thể chỉnh sửa sổ tay hướng dẫn hoặc xem bất kỳ bí mật nào.
* Nó cũng cung cấp REST API để bạn có thể tích hợp Ansible AWX với các công cụ khác.
  
# Ansible AWX Terms
Sau khi bạn đã cài đặt và đăng nhập vào Ansible AWX, bạn sẽ thấy một màn hình trống trông giống như thế này:
<p align="center">
  <img src="../images/image.png"/>
</p>
<p align="center">
  <img src="../images/dashboard.png"/>
</p>

# AWX Inventory
Bước 1: Vào mục Inventory, chọn Add:
<p align="center">
  <img src="../images/image5.png"/>
</p>
Bước 2: Điền các thông tin cần thiết. Về cơ bản thì ta chỉ cần điền Name là đủ:
<p align="center">
  <img src="../images/image6.png"/>
</p>
Bước 3: Tạo group mới
<p align="center">
  <img src="../images/image7.png"/>
</p>
<p align="center">
  <img src="../images/image8.png"/>
</p>
Bước 4: Tạo host. 
<p align="center">
  <img src="../images/host_awx.png"/>
</p>

# AWX Credentials
Credentials là nơi bạn tạo thông tin xác thực GitHub để tải xuống playbooks và cũng là nơi thêm thông tin xác thực để đăng nhập vào thiết bị mạng hoặc đăng nhập vào Galaxy.
Trong mô hình V2 CredentialType, các mối quan hệ được định nghĩa như sau:
<p align="center">
  <img src="../images/image4.png"/>
</p>
Bước 1: Tạo Credential mới
<p align="center">
  <img src="../images/image2.png"/>
</p>
Bước 2: Khai báo tên cho Credential. Do chúng ta tạo Credential là private key để SSH đến host, nên chọn Type là Machine, sau đó ta paste key (thường đặt tại /root/.ssh/id_rsa) vào phần SSH Private Key
<p align="center">
  <img src="../images/image3.png"/>
</p>

# Kiểm tra kết nối
Truy cập vào danh sách các Groups rồi chọn Run Command. Khi thao tác Run Command sẽ giống như việc ta sử dụng Adhoc Commands
<p align="center">
  <img src="../images/image9.png"/>
</p>
Chọn Module sẽ sử dụng rồi chọn Next
<p align="center">
  <img src="../images/image10.png"/>
</p>

# AWX Projects
Khái niệm Project trong AWX đơn giản là một nơi lưu trữ các tài nguyên của Ansible-playbook như các roles, playbooks. Một 1 project có thể lưu nhiều playbook sử dụng cho một mục đích hoặc đối tượng khác nhau.
<p align="center">
  <img src="../images/image11.png"/>
</p>
Điền các thông tin cần thiết. Ở đây ta cần lưu ý với type Manual thì ta sẽ phải copy thư mục chứa playbook vào đường dẫn /var/lib/awx/projects, còn với type Git thì ta sẽ clone repository chứa playbook trên Github về, loại này có phần tiện lợi hơn nhờ các tính năng của Github.
<p align="center">
  <img src="../images/image12.png"/>
</p>
<p align="center">
  <img src="../images/project_awx.png"/>
</p>

# AWX Templates
Job template trong Ansible AWX bao gồm nhiều tham số và thiết lập khác nhau chỉ định cách thực thi playbook Ansible cụ thể. Các thiết lập này có thể bao gồm:
* Inventory: Chỉ định các hosts hoặc nhóm tập hợp các host mà playbook được chi định. AWX cho phép quản lý inventory một cách linh hoạt, cho phép sử dụng các tập lệnh hoặc nguồn bên ngoài để tạo inventory.
* Credentials: Xác định thông tin xác thực cần thiết để xác thực với các máy chủ mục tiêu, chẳng hạn như khóa SSH, tên người dùng và mật khẩu.
* Playbook: Chỉ định playbook Ansible cần được thực thi. Đây có thể là playbook được lưu trữ trong dự án AWX hoặc playbook được lấy từ nguồn bên ngoài, chẳng hạn như kho lưu trữ Git.
* Extra Variables: Cho phép truyền các biến bổ sung vào playbook, có thể được sử dụng để tùy chỉnh hành vi của nó một linh động.
* Verbosity Level: Chỉ định mức độ chi tiết cho đầu ra được tạo trong quá trình thực thi playbook.
* Timeouts: Xác định thời lượng tối đa được phép thực thi playbook trước khi được coi là không thành công. 
* Notification Settings: Cấu hình cách xử lý thông báo khi playbook hoàn tất, bao gồm thông báo qua email hoặc kích hoạt webhook.
  ==> Bằng cách tạo và cấu hình mẫu công việc trong Ansible AWX, người dùng có thể xác định các quy trình chuẩn hóa để thực thi sổ tay hướng dẫn Ansible trên toàn bộ cơ sở hạ tầng của họ. Điều này giúp hợp lý hóa quy trình làm việc tự động hóa, cải thiện khả năng lặp lại và tạo điều kiện cho sự cộng tác giữa các thành viên trong nhóm làm việc trên các tác vụ tự động hóa cơ sở hạ tầng. Ngoài ra, AWX cung cấp các tính năng để lên lịch thực thi sổ tay hướng dẫn, theo dõi trạng thái công việc và kiểm tra các lần chạy sổ tay hướng dẫn, tăng cường khả năng hiển thị và kiểm soát các quy trình tự động hóa.

<p align="center">
  <img src="../images/template.png"/>
</p>
Ta có thể khởi chạy, chỉnh sửa và sao chép job template. Để xóa một job template, ta chọn một hoặc nhiều mẫu và nhấp vào nút Xóa.

## Khởi tạo Template
Bước 1: Add Template
<p align="center">
  <img src="../images/template1.png"/>
</p>
Bước 2: Điền các thông tin cần thiết. Các tham số ta cần chú ý là:
* Name: Tên template
* Job Type: có 2 giá trị là Run hoặc Check. Nếu chọn Run thì template sẽ thực sự thực hiện playbook, tỏng khi Check sẽ chỉ phỏng đoán kết quả của playbook thôi.
* Inventory: chọn inventory mong muốn
* Project: chọn project lưu trữ playbook muốn thực hiện
* Playbook: chọn playbook
* Credential: credential sẽ sử dụng để kết nối đến các host client
* Variables: các biến nâng cao
* Limit: tùy chọn host sẽ thực hiện hoặc không thực hiện, nếu không điền gì thì mặc định là all
<p align="center">
  <img src="../images/template2.png"/>
</p>
Bước 3: Khởi chạy Template
<p align="center">
  <img src="../images/template3.png"/>
</p>
<p align="center">
  <img src="../images/template4.png"/>
</p>

# Job slicing
Trong AWX (Ansible AWX), "job slicing" là một tính năng cho phép bạn chia nhỏ một công việc (job) thành nhiều phần nhỏ hơn và thực hiện chúng song song (parallel execution). Tính năng này rất hữu ích khi bạn có một tập hợp lớn các mục tiêu (hosts) và muốn giảm thời gian hoàn thành công việc bằng cách phân phối tải công việc cho nhiều tiến trình chạy đồng thời.

# AWX Schedule
Trong AWX, "Schedule" là tính năng cho phép bạn lên lịch tự động thực thi các công việc (jobs) hoặc dự án (projects) vào những thời điểm cụ thể. Tính năng này rất hữu ích trong việc tự động hóa các tác vụ định kỳ, chẳng hạn như chạy playbook Ansible để kiểm tra cấu hình hệ thống, sao lưu dữ liệu, hoặc cập nhật phần mềm vào các thời điểm nhất định mà không cần sự can thiệp thủ công.
## Các tính năng
* Lên lịch công việc định kỳ: Bạn có thể thiết lập lịch trình cho các job templates hoặc workflow templates để chúng chạy tự động hàng ngày, hàng tuần, hàng tháng hoặc theo bất kỳ chu kỳ thời gian nào bạn xác định.
* Quản lý lịch trình: AWX cung cấp một giao diện quản lý dễ dàng cho phép bạn xem, chỉnh sửa, hoặc xóa các lịch trình đã thiết lập. Bạn có thể theo dõi lịch sử thực thi của các công việc đã được lên lịch.
* Lập lịch theo thời gian cụ thể: Ngoài việc thiết lập các chu kỳ định kỳ, bạn cũng có thể lên lịch một công việc để chạy vào một thời điểm cụ thể trong tương lai.
* Thiết lập Job Slicing: Khi kết hợp với tính năng job slicing, bạn có thể lên lịch các công việc được chia nhỏ và thực thi song song theo thời gian định trước.
* Tùy chọn điều kiện và biến: Khi lên lịch một công việc, bạn có thể chỉ định các điều kiện đặc biệt hoặc các biến (variables) để điều chỉnh hành vi của công việc khi nó được thực thi.
<p align="center">
  <img src="../images/schedules-sample-list.png"/>
</p>

## Thêm Schedule
1. Nhấp vào tab Schedules  của tài nguyên bạn đang cấu hình (template, project, or inventory source).
2. Nhấp vào nút Add , nút này sẽ mở Create Schedule window.
<p align="center">
  <img src="../images/generic-create-schedule.png"/>
</p>
<p align="center">
  <img src="../images/generic-create-schedule-details.png"/>
</p>

# Working with webhook
Trong AWX, webhook là một công cụ cho phép bạn kích hoạt các công việc (jobs) tự động từ các sự kiện hoặc từ các ứng dụng khác bên ngoài AWX thông qua HTTP POST requests. Webhooks trong AWX thường được sử dụng để tích hợp AWX với các hệ thống CI/CD, như Jenkins, GitLab CI, hoặc GitHub Actions.
## Cách thiết lập và sử dụng webhook trong AWX:
1. Tạo Webhook: Khi bạn tạo hoặc chỉnh sửa một Job Template hoặc Workflow Template trong AWX, bạn sẽ có tùy chọn tạo webhook. Mỗi webhook sẽ có một URL duy nhất mà hệ thống bên ngoài sẽ gửi HTTP POST request đến.
2. Cấu hình hệ thống bên ngoài: Bạn cần cấu hình hệ thống bên ngoài (ví dụ: GitHub, GitLab, Jenkins) để gửi một POST request tới URL của webhook khi xảy ra sự kiện mong muốn.
3. Xác thực (Optional): AWX hỗ trợ xác thực webhook thông qua token. Điều này giúp đảm bảo rằng chỉ những yêu cầu hợp lệ từ các nguồn đáng tin cậy mới có thể kích hoạt job.

# AWX Workflow
Trong AWX, workflow là một khái niệm quan trọng cho phép bạn liên kết nhiều job template lại với nhau để tạo thành một chuỗi các bước thực thi tự động. Workflow giúp bạn định nghĩa các quy trình phức tạp và quản lý chúng một cách linh hoạt trong môi trường tự động hóa.
## Các thành phần chính của một workflow trong AWX:
* Nodes (Nút):
Mỗi bước trong workflow được biểu diễn dưới dạng một nút (node). Mỗi nút có thể đại diện cho một job template, một project sync, hoặc một inventory sync. Nút này sẽ thực thi các tác vụ cụ thể như cài đặt phần mềm, cập nhật cấu hình, kiểm tra trạng thái hệ thống, v.v.
* Liên kết giữa các nút:
Các nút trong workflow có thể được liên kết với nhau dựa trên các điều kiện, chẳng hạn như khi một nút thành công, thất bại, hoặc đạt một số điều kiện cụ thể khác. Điều này cho phép bạn điều hướng qua các luồng công việc khác nhau tùy thuộc vào kết quả của các bước trước đó.
* Conditional Logic (Logic điều kiện):
Workflow cho phép bạn thêm logic điều kiện vào quy trình, ví dụ: nếu một bước thành công thì tiếp tục bước này, nếu thất bại thì chuyển sang một bước khác. Điều này mang lại sự linh hoạt trong việc quản lý các kịch bản phức tạp, đảm bảo rằng chỉ những hành động phù hợp mới được thực hiện.
* Workflow Approval (Phê duyệt workflow):
Bạn có thể cấu hình workflow yêu cầu sự phê duyệt thủ công tại một số điểm cụ thể trước khi tiếp tục thực thi các bước tiếp theo. Điều này thường được sử dụng trong các quy trình nhạy cảm, nơi cần sự can thiệp của con người trước khi triển khai thay đổi.

## Workflow scenarios and considerations
A root node được đặt thành ALWAYS theo mặc định và không thể chỉnh sửa
<p align="center">
  <img src="../images/wf-root-node-always.png"/>
</p>
Nếu ta xóa job hoặc workflow template trong quy trình công việc, các ndoes trước đó được kết nối với nodes đã xóa sẽ tự động được kết nối ngược lại và giữ nguyên.
<p align="center">
  <img src="../images/wf-node-delete-scenario.png"/>
</p>
<p align="center">
  <img src="../images/wf-node-delete-scenario_1.png"/>
</p>
Ta có thể có một quy trình làm việc hội tụ, trong đó nhiều jobs hội tụ thành một.
<p align="center">
  <img src="../images/wf-node-convergence.png"/>
</p>

# Notification 
Trong Ansible AWX, Notification là một tính năng cho phép bạn gửi thông báo khi các sự kiện nhất định xảy ra trong hệ thống. Những thông báo này có thể được gửi qua nhiều kênh khác nhau như email, Slack, Microsoft Teams, webhook, v.v. Tính năng này rất hữu ích để theo dõi và quản lý các tác vụ và dự án trong AWX.
<p align="center">
  <img src="../images/notification.png"/>
</p>

## Email
Loại thông báo email hỗ trợ nhiều loại máy chủ SMTP và hỗ trợ kết nối SSL/TLS và thời gian chờ.
* Kiểm tra xác thực thuần túy.
* Kiểm tra xác thực SSL và TLS.
* Xác minh người nhận đơn lẻ và nhiều người nhận.
* Xác minh chủ đề và nội dung tin nhắn được định dạng hợp lý. Chúng phải là văn bản thuần túy nhưng có thể đọc được.

# Slack
Slack rất dễ cấu hình; nó yêu cầu một mã thông báo, bạn có thể nhận được mã thông báo này bằng cách tạo bot trong phần cài đặt tích hợp cho nhóm Slack.
* Kiểm tra kênh đơn và nhiều kênh và định dạng tốt của tin nhắn. Lưu ý rằng thông báo slack chỉ chứa thông tin tối thiểu.

# Mattermost
Tích hợp thông báo Mattermost sử dụng Incoming Webhooks. Không cần mật khẩu vì URL webhook chính là bí mật. Webhooks phải được bật trong System Console của Mattermost. Nếu người dùng muốn cho phép thông báo AWX sửa đổi Icon URL và tên người dùng của thông báo, thì họ cũng phải bật các tùy chọn này.
Để bật những cài đặt này trong Mattermost:
1. Go to System Console > Integrations > Custom Integrations. Check "Enable Incoming Webhooks".
2. Optionally, go to System Console > Integrations > Custom Integrations. Check "Enable integrations to override usernames" and Check "Enable integrations to override profile picture icons".
3. Go to Main Menu > Integrations > Incoming Webhook. Click "Add Incoming Webhook".
4. Choose a "Display Name", "Description", and Channel. This channel will be overridden if the notification uses the channel option.

* url: Đây là URL webhook đến được cấu hình trong Mattermost. Thông báo sẽ sử dụng URL này để POST.
* username: Tùy chọn. Tên người dùng để hiển thị thông báo.
* channel: Tùy chọn. Ghi đè kênh để hiển thị thông báo. Theo mặc định, webhook đến Mattermost được liên kết với một kênh, vì vậy nếu để trống thì kênh này sẽ sử dụng kênh webhook đến. Lưu ý, nếu kênh không tồn tại, thì thông báo sẽ báo lỗi.
* icon_url: Tùy chọn. URL trỏ đến biểu tượng để sử dụng cho thông báo.

# Rocket.Chat
Tích hợp thông báo Rocket.Chat sử dụng Incoming Webhooks. Không cần mật khẩu vì URL webhook chính là bí mật. Tích hợp phải được tạo trong phần Quản trị của cài đặt Rocket.Chat.
* url: URL webhook đến được cấu hình trong Rocket.Chat. Thông báo sẽ sử dụng URL này để POST.
* username: Tùy chọn. Thay đổi tên người dùng được hiển thị từ Rocket.Chat thành tên người dùng được chỉ định.
* icon_url: Tùy chọn. URL trỏ đến một biểu tượng để sử dụng cho thông báo.

# Pagerduty
Pagerduty là một tích hợp khá đơn giản. Người dùng sẽ tạo một Khóa API trong hệ thống Pagerduty (đây sẽ là mã thông báo được cấp cho AWX) và sau đó tạo một "Service" sẽ cung cấp một "Integration Key" cũng sẽ được cấp cho AWX. Các tùy chọn khác cần lưu ý là:
* subdomain: Khi bạn đăng ký tài khoản Pagerduty, bạn sẽ nhận được một subdomain duy nhất để giao tiếp. Ví dụ, nếu bạn đăng ký là "towertest", bảng điều khiển web sẽ ở towertest.pagerduty.com và bạn sẽ cung cấp API AWX "towertest" làm subdomain (không phải là tên miền đầy đủ).
* client_name: Nội dung này sẽ được gửi cùng với nội dung cảnh báo đến dịch vụ Pagerduty để giúp xác định dịch vụ đang sử dụng khóa API/dịch vụ. Điều này hữu ích nếu nhiều tích hợp đang sử dụng cùng một khóa API và dịch vụ.

# IRC
Thông báo IRC của AWX có dạng bot IRC sẽ kết nối, gửi tin nhắn đến kênh(các kênh) hoặc người dùng cá nhân(các kênh), sau đó ngắt kết nối. Bot thông báo AWX cũng hỗ trợ xác thực SSL. Bot AWX hiện không hỗ trợ nhận dạng Nickserv. Nếu kênh hoặc người dùng không tồn tại hoặc không trực tuyến, thì Thông báo sẽ không bị lỗi; tình huống lỗi được dành riêng cho kết nối.
* server: Tên máy chủ hoặc địa chỉ của máy chủ IRC.
* port: Cổng máy chủ IRC.
* nickname: Biệt danh của bot sau khi kết nối với máy chủ.
* password: Máy chủ IRC có thể yêu cầu mật khẩu để kết nối. Nếu máy chủ không yêu cầu, thì đây phải là chuỗi rỗng.
* use_ssl: Nếu bạn muốn bot sử dụng SSL khi kết nối.
* targets: Danh sách người dùng và/hoặc kênh để gửi thông báo.

# Webhook
Kiểu thông báo webhook trong AWX cung cấp giao diện đơn giản để gửi POST đến một dịch vụ web được xác định trước. AWX sẽ POST đến địa chỉ này bằng kiểu nội dung application/json với dữ liệu tải trọng chứa tất cả các chi tiết có liên quan ở định dạng JSON.
* url: URL đầy đủ sẽ được POST đến
* headers: Headers ở dạng JSON trong đó các khóa và giá trị là chuỗi. Ví dụ: {"Authentication": "988881adc9fc3655077dc2d4d757d480b5ea0e11", "MessageType": "Test"}

# Grafana
Loại thông báo Grafana cho phép bạn tạo chú thích Grafana. Chi tiết về tính năng này của Grafana có tại http://docs.grafana.org/reference/annotations/. Để cho phép AWX thêm chú thích, cần tạo Khóa API trong Grafana. Lưu ý rằng các chú thích đã tạo là các sự kiện vùng có thời gian bắt đầu và kết thúc của Công việc AWX được liên kết.
Các tùy chọn có thể cấu hình của loại thông báo Grafana là:

* Grafana URL: Required. The base URL of the Grafana server. Note: the /api/annotations endpoint will be added automatically to the base Grafana URL.
* API Key: Required. The Grafana API Key to authenticate.
* ID of the Dashboard: Optional. To create annotations in a specific Grafana dashboard, enter the ID of the dashboard.
* ID of the Panel: Optional. To create annotations in a specific Panel, enter the ID of the panel. Note: Nếu không cung cấp dashboardId hoặc panelId, thì chú thích toàn cục sẽ được tạo và có thể được truy vấn trong bất kỳ bảng điều khiển nào thêm nguồn dữ liệu chú thích Grafana.
* Annotations tags: The list of tags to add to the annotation. One tag per line.
* Disable SSL Verification: Disable the verification of the SSL certificate, e.g., when using a self-signed SSL certificate for Grafana.