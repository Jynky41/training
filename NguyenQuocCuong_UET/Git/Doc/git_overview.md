# Git là gì ?
Git là một hệ thống quản lý phiên bản phân tán (Distributed Version Control System – DVCS), nó là một trong những hệ thống quản lý phiên bản phân tán phổ biến nhất hiện nay. Git cung cấp cho mỗi lập trình viên kho lưu trữ (repository) riêng chứa toàn bộ lịch sử thay đổi.
## VCS là gì ?
VCS là viết tắt của Version Control System là hệ thống kiểm soát các phiên bản phân tán mã nguồn mở. Các VCS sẽ lưu trữ tất cả các file trong toàn bộ dự án và ghi lại toàn bộ lịch sử thay đổi của file. Mỗi sự thay đổi được lưu lại sẽ được và thành một version (phiên bản).
VCS nghĩa là hệ thống giúp lập trình viên có thể lưu trữ nhiều phiên bản khác nhau của một mã nguồn được nhân bản (clone) từ một kho chứa mã nguồn (repository), mỗi thay đổi vào mã nguồn trên local sẽ có thể ủy thác (commit) rồi đưa lên server nơi đặt kho chứa chính.
## Git hoạt động như thế nào?
Các hệ thống này (CVS, Subversion, Perforce, Bazaar, v.v.) coi thông tin chúng lưu giữ dưới dạng một tập hợp các file và những thay đổi được thực hiện đối với mỗi file theo thời gian.
<p align="center">
  <img src="../Image/image-30.png"/>
</p>
Git không nghĩ đến hoặc lưu trữ dữ liệu của mình theo cách này. Thay vào đó, Git coi thông tin được lưu trữ là một tập hợp các snapshot – ảnh chụp toàn bộ nội dung tất cả các file tại thời điểm.
Mỗi khi bạn “commit”, Git sẽ “chụp” và tạo ra một snapshot cùng một tham chiếu tới snapshot đó. Để hiệu quả, nếu các tệp không thay đổi, Git sẽ không lưu trữ lại file — chỉ là một liên kết đến tệp giống file trước đó mà nó đã lưu trữ. 
Đây là điểm khác biệt quan trọng giữa Git và gần như tất cả các VCS khác. Nó khiến Git phải xem xét lại hầu hết mọi khía cạnh của kiểm soát phiên bản mà hầu hết các hệ thống khác đã sao chép từ thế hệ trước.
<p align="center">
  <img src="../Image/image-31.png"/>
</p>

## Các thuật ngữ Git quan trọng
### 1. Branch
Các Branch (nhánh) đại diện cho các phiên bản cụ thể của một kho lưu trữ tách ra từ project chính của bạn. Branch cho phép bạn theo dõi các thay đổi thử nghiệm bạn thực hiện đối với kho lưu trữ và có thể hoàn nguyên về các phiên bản cũ hơn.

### 2. Commit
Một commit đại diện cho một thời điểm cụ thể trong lịch sử dự án của bạn. Sử dụng lệnh commit kết hợp với lệnh git add để cho git biết những thay đổi bạn muốn lưu vào local repository.
### 3. Checkout
Sử dụng lệnh git checkout để chuyển giữa các branch. Chỉ cần nhập git checkout theo sau là tên của branch bạn muốn chuyển đến hoặc nhập git checkout master để trở về branch chính (master branch).
### 4. Fetch
Lệnh git fetch tìm nạp các bản sao và tải xuống tất cả các tệp branch vào máy tính của bạn. Sử dụng nó để lưu các thay đổi mới nhất vào kho lưu trữ của bạn. Nó có thể tìm nạp nhiều branch cùng một lúc.
### 5. Fork
Một fork là một bản sao của một kho lưu trữ (repository). Các lập trình viên thường tận dụng lợi ích của fork để thử nghiệm các thay đổi mà không ảnh hưởng đến dự án chính.
### 6. Head
Các commit ở đầu của một branch được gọi là head. Nó đại diện cho commit mới nhất của repository mà bạn hiện đang làm việc.
### 7. Master
Master là nhánh chính của tất cả các repository của bạn. Nó nên bao gồm những thay đổi và commit gần đây nhất.
<p align="center">
  <img src="../Image/image-32.png"/>
</p>

### 8. Merge 
Lệnh git merge kết hợp với các yêu cầu kéo (pull requests) để thêm các thay đổi từ nhánh này sang nhánh khác.
### 9. Origin
Origin là phiên bản mặc định của repository. Origin cũng đóng vai trò là bí danh hệ thống để liên lạc với nhánh chính. Lệnh git push origin master để đẩy các thay đổi cục bộ đến nhánh chính.
<p align="center">
  <img src="../Image/image-33.png"/>
</p>

# Phân biệt gitlab và github
GitLab và GitHub là hai nền tảng phổ biến để quản lý mã nguồn và cộng tác trong các dự án phát triển phần mềm. Mặc dù cả hai đều cung cấp các tính năng cơ bản giống nhau như lưu trữ mã nguồn, quản lý phiên bản, và hỗ trợ cộng tác, nhưng chúng có một số điểm khác biệt chính. Dưới đây là so sánh chi tiết:

1. Triển khai và Lưu trữ
GitHub:
Chủ yếu cung cấp dịch vụ lưu trữ đám mây.
Không có phiên bản tự quản lý (self-hosted) chính thức, nhưng có thể sử dụng GitHub Enterprise để triển khai nội bộ trong một số trường hợp.
GitLab:
Cung cấp cả hai phiên bản: lưu trữ đám mây (GitLab.com) và phiên bản tự quản lý (GitLab self-hosted).
GitLab tự quản lý thường được sử dụng bởi các doanh nghiệp muốn kiểm soát hoàn toàn dữ liệu của mình.

2. Tính năng CI/CD (Continuous Integration/Continuous Deployment)
GitHub:
GitHub Actions cung cấp khả năng CI/CD tích hợp, cho phép tự động hóa các quy trình xây dựng, kiểm thử, và triển khai.
GitLab:
GitLab CI/CD là một phần tích hợp sẵn của GitLab và được đánh giá cao về tính năng và khả năng cấu hình linh hoạt.
Tích hợp trực tiếp với GitLab pipelines và có khả năng quản lý CI/CD phức tạp hơn.

3. Quản lý dự án và công việc
GitHub:
GitHub Projects và GitHub Issues được sử dụng để quản lý công việc và theo dõi lỗi.
Tính năng Projects sử dụng các bảng Kanban để quản lý các công việc.
GitLab:
Cung cấp các công cụ quản lý dự án toàn diện hơn như issues, boards (tương tự Kanban), milestones, và roadmaps.
Tích hợp chặt chẽ với các tính năng CI/CD và DevOps khác.

4. Mô hình nhánh (Branching Model)
GitHub:
Phổ biến với mô hình nhánh Git Flow và hỗ trợ Pull Requests cho quy trình làm việc cộng tác.
GitLab:
Cung cấp Merge Requests, tương tự như Pull Requests, nhưng với các tính năng quản lý chi tiết hơn như quy tắc duyệt mã, pipeline trạng thái, và bảo mật.

5. Bảo mật và Phân quyền
GitHub:
GitHub cung cấp các tính năng bảo mật và kiểm soát truy cập mạnh mẽ thông qua GitHub Advanced Security và phân quyền chi tiết.
GitLab:
GitLab có nhiều tùy chọn bảo mật và phân quyền chi tiết hơn cho các phiên bản tự quản lý.
Tính năng kiểm soát truy cập dựa trên vai trò (RBAC) và bảo mật DevSecOps tích hợp.


# Thực hành
## Push project 
1. Git config 
<p align="center">
  <img src="../Image/image-34.png"/>
</p>

2. Git add
<p align="center">
  <img src="../Image/image-35.png"/>
</p>

3. Git commit 
<p align="center">
  <img src="../Image/image-36.png"/>
</p> 
<p align="center">
  <img src="../Image/image-37.png"/>
</p>

## Tạo nhánh mới
- Tạo mới nhánh
- Nhảy từ nhánh này sang nhánh khác
- Đổi tên nhánh
<p align="center">
  <img src="../Image/image-38.png"/>
</p>

## Gộp nhánh
<p align="center">
  <img src="../Image/image-39.png"/>
</p>