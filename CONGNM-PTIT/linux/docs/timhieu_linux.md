# Tìm hiểu về hệ thống Ubuntu-Linux và các lệnh cơ bản
### I.	Hệ điều hành Linux
1. Giới thiệu chung
2. Quá trình khởi động (Boot the System)
3. Cơ bản về LVM
4. Kiến trúc cây thư mục
5. Công cụ quản lí gói apt – tiện ích dpkg
6. Các thao tác với file
### II.	Thực hành


## 1. **Giới thiệu về Linux**

- Là hệ điều hành mã nguồn mở dựa trên UNIX
- Cấu trúc hệ điều hành gồm 3 phần chính:
    + Kernel: Phần lõi của Linux, quản lí tài nguyên hệ thống và tương tác trực tiếp với phần cứng
    + Shell: Giao diện dòng lệnh mà người dùng và lập trình viên sử dụng để tương tác với hệ điều hành
    + Các công cụ và tiện ích: Các chương trình nhỏ hỗ trợ quản trị viên quản lí hệ thống, thực thi các tác vụ và cung cấp dịch vụ cho người dùng
- Linux có nhiều bản phân phối khác nhau: Ubuntu, Fedora, CentOS, Debian,…
- Lí do sử dụng hệ điều hành Linux trên các hệ thống server:
    + Mã nguồn mở, không mất chi phí sử dụng
    + Dung lượng nhẹ hơn nhiều so với windowns (nếu dùng bản live server) để dành tài nguyên cho các ứng dụng khác
    + Cộng đồng hỗ trợ lớn, Cập nhật vá lỗi/lỗ hổng bảo mật thường xuyên


## 2. **Quá trình khởi động (Boot the System)**
2.1. _sysvinit_
- Quản lí hệ thống và dịch vụ của hệ điều hành Linux cũ VD: Centos 6 trở về trước
- Dựa trên các tập lệnh init
- Thứ tự khởi động hệ thống Linux

B1: Bật nguồn (Power on)<br>
B2: BIOS tải lên (Các hệ thống Linux hiện đại sử dụng EFI)<br>
B3: BIOS tìm và chuyển giao cho ‘boot sector’ của ổ đĩa chính (hoặc ổ đĩa được chọn)<br>
B4: Boot sector cung cấp MBR (Master Boot Record), nằm trong 512bytr đầu tiên của ổ đĩa hoạt động hoặc ổ đầu tiên, tùy thuộc vào quá trình cài đặt<br>
B5: Boot Loader được thực thi<br>
B6: Boot Loader LILO/GRUB/GRUB2 bắt đầu<br>
B7: Bước nhập dữ liệu người dùng: tùy thuộc vào cấu hình boot loader, người dùng có thể chọn từ menu các loại khởi động/kernel hoặc cho phép sử dụng mặc định<br> 
B8: Kernel Linux được đọc và thực thi<br>
B9: Khởi tạo thiết bị, tải các module và initial RAM disk (initrd)<br>
B10: File hệ thống gốc (/) được gắn vào Linux<br>
B11: Chương trình init được tải lên (trở thành PID 1 - /sbin/init)<br>
B12: /etc/inittab được đọc và các tập lệnh runlevel được chạy<br>   Debian/Ubuntu: /etc/init.d/rc#<br>  Red Hat/CentOS: /etc/rc.d/init.d/rc.sysinit<br>
B13: Các module được chỉ thị trong các tệp init được tải lên<br>
B14: Kiểm tra file hệ thống gốc (/)<br>
B15: Các file hệ thống cục bộ được gắn (mount) vào hệ điều hành Linux<br>
B16: Thiết bị mạng được khởi động<br>
B17: Các file system từ xa được gắn (mount) vào hệ điều hành nếu được cấu hình<br>
B18: init process đọc lại /etc/inittab và chuyển đến run level mặc định được chỉ định và thực thi các tập lệnh tương ứng<br>
B19: Thực thi các tập lệnh runlevel theo thứ tự số cho runlevel được chỉ định trong /etc/inittab<br>
B20: Các phiên tty được tải lên theo thứ tự được liệt kê trong /etc/inittab<br>
B21: Màn hình nhắc đăng nhập hiển thị.<br>


2.2. _Quá trình khởi động systemd_
- Mặc định trên hầu hết các bản phân phổi Linux hiện đại
- Được thiết kế để thay thế cho sysvinit
- Hiệu suất cao hơn và cho phép hoạt động chạy song song
- Ít các thành phần phụ thuộc hơn
- Ưu tiên và thứ tự ưu tiên các thành phần dịch vụ
- Khái niệm ‘units’ thay thế cho các tập lệnh khởi động rc#.d của sysvinit
- Các dịch vụ có đuôi .service thay vì chỉ là tên tập lệnh khởi động (init script)
- Các thành phần chính:
    + system: quản lí các dịch vụ và hệ thống
    + systemctl: lệnh chính để điều khiển start/stop/restart/status của dịch vụ
    + system-analyze: hiển thị thông tin về hiệu suất khởi động hệ thống, cho phép theo dõi thông tin cho việc gỡ lỗi
    + logind: thay thế consolekit, hỗ trợ các trình quản lí X windowns
    + consoled: dịch vụ console daemon, thay thế virtual terminals
    + journald: hệ thống ghi nhật kí nhị phân
    + networkd: dich vụ hỗ trợ mạng
- Các runlevel trong systemd:
    + 0: tắt hệ thống
    + 1: chế độ người dùng đơn
    + 2: không giao diện đồ họa, cho phép chạy mạng, đa người dùng
    + 3: không có giao diện đồ họa, đầy đủ mạng và đa người dùng
    + 4: không giao diện đồ họa, đầy đủ mạng và đa người dùng
    + 5: môi trường đồ họa đầy đủ, đa người dùng
    + 6: khởi động lại

2.3 _upstart_
- Quá trình khởi động và tắt bất đồng bộ
- Dễ triển khai, duy trì tính tương thích với sysvinit
- Dễ mở rộng
## 3. **Cơ bản về LVM (Logical Volume Manager)**
![LVM architecture](../images/lvm.png)

3.1 *Các khái niệm*
- PV (Physical Volume): Ổ đĩa vật lí – điểm khởi đầu hoặc đơn vị lưu trữ cơ bản
Tương ứng với một ổ đĩa hoặc một phân vùng trên hệ thống Linux (Có thể là cục bộ trên hệ thống như /dev/sda hoặc một thiết bị lưu trữ SAN)
- VG (Volume group): Nhóm ổ đĩa – Kết hợp một hoặc nhiều PV để tạo thành một nhóm lưu trữ có sẵn
- LV(Logical volume): Ổ đĩa logic – Mỗi nhóm ổ đĩa có thể được chia thành một hoặc nhiều Logical Volume có thể được định dạng với 1 loại filesystem cụ thể và sau đó được mount vào hệ điều hành


3.2. *Ưu điểm của LVM*
- Linh hoạt: tăng kích cỡ của một Logical Volume chỉ bằng cách điều chỉnh kích thước, lấy thêm dung lượng từ nhóm ổ đĩa(VG), nếu nhóm ổ đĩa đã đầy dữ liệu, chỉ cần cấp thêm ổ đĩa cho nhóm ổ đĩa, và thiết đặt nó vào logical volume
- Snapshots: Sao lưu trạng thái của bất kì Logical Volume nào có thể được sử dụng để sao lưu, khôi phục, kiểm tra, dịch chuyển,.. mà không ảnh hưởng trực tiếp đến LV


3.3 *Thực hành lab LVM*
- Chuẩn bị: 1 máy host gồm 3 ổ đĩa  
    ![Kiểm tra ổ đĩa](../images/lab1.1.png)
>Có thể thấy host hiện tại có 3 ổ đĩa sda, sdb, sdc cùng với các phân vùng con của nó. Ta sẽ tiến hành thực hiện tính năng LVM trên ổ sdb và sdc. Tạo các parttion nếu chưa có. <br>  

'NOTE: Vùng chứa /boot không thể được thực hiện LVM'  
- Các bước thực hiện  
    + B1: Tạo physical volume từ các phân vùng ổ đĩa  
    >pvcreate /dev/sdb1<br>
    >pvcreate /dev/sdb2<br>
    >pvcreate /dev/sdc1<br>
    >pvcreate /dev/sdc2<br>
    >pvcreate /dev/sdc3<br>

    Sau khi tạo xong có thể kiểm tra các pv bằng lệnh: pvs hoặc pvdisplay<br>
![Tạo physical volume](../images/lab1.2.png)<br>

    + B2: Tạo Volume group từ các physical volume<br>

>vgcreate [tên group] [pv1] [pv2] [...] [pv n] <br>
    Kiểm tra các volume group đã tạo bằng lệnh: vgs hoặc vgdisplay <br>

![Tạo volume group](../images/lab1.3.png)<br>

    + B3: Tạo các logical volume từ volume group  

>lvcreate -L [dung lượng cấp cho volume] -n [tên volume] [tên group]<br>

![Tạo logical volume](../images/lab1.4.png)<br>

    + B4: Định dạng filesystem cho logical volume vừa tạo và mount vào thư mục tùy chọn<br>

>Lệnh format volume: mkfs -t [filesystem type] [path to logical volume]<br>

![định dạng filesystem](../images/lab1.5.png)

>Lệnh gán: mount [path to logical volume] [thư mục được mount]<br>

![mount thư mục vào logical volume](../images//lab1.6.png)

- Tăng kích thước volume: Nếu volume group còn dung lượng khả dụng thì có thể mở rộng kích thước của logical volume. Nếu không, ta cần add thêm pv vào group.  
>Lệnh tăng dung lương lv: lvextend -L +[size] <path to logical volume>  
VD: lvextend -L +20G /dev/test-group/lv1  
'NOTE: sau khi mở rộng dung lượng, ta cần format lại định dạng filesystem cho logical volume<br> lệnh: resize2fs <path to logical volume>'  
- Giảm kích thước volume: Trước khi giảm dung lượng logical volume ta cần unmount thư mục đã gán trước đó  
>lệnh unmount: unmount [path to logical volume]  
VD: unmount /dev/test-group/lv1  
Tiến hành giảm dung lượng  
>lvreduce -L -[size] <path to lv>  
VD: lvreduce -L -10G /dev/test-group/lv1  

3.4 _Tổng hợp các lệnh thường sử dụng trong LVM_  

|   |create|view|remove|change|scan|resize|
|-|-|-|-|-|-|-|
|pv|pvcreate|pvs,pvdisplay|pvremove|pvchange|pvscan||
|vg|vgcreate|vgs,vgdisplay|vgremove|vgchange|vgscan|vgextend,vgreduce|
|lv|lvcreate|lvs, lvdisplay|vlremove|vlchange|lvscan|lvextend, lvreduce|

## 4. **Kiến trúc cây thư mục trong Linux**

4.1 _Hệ thống thư mục_ <br>

- Trong linux tệp được tổ chức thành các thư mục, theo mô hình phân cấp. Tham chiếu đến một tệp bằng tên và đường dẫn. Các câu lệnh thao tác tệp cho phép dịch chuyển, sao chép một tệp hay toàn bộ thư mục cùng với các thư mục con chứa trong nó…

- Có thể sử dụng các ký tự, dấu gạch dưới, chữ số, dấu chấm và dấu phảy để đặt tên tệp. Không được bắt đầu một tên tệp bằng dấu chấm hay chữ số. Những ký tự khác như ‘/’, ‘?’, ‘*’, là ký tự đặc biệt được dành riêng cho hệ thống. Chiều dài của tên tệp có thể tới 256 ký tự.

- Trong UNIX không có khái niệm phần tên mở rộng (file extensions) theo kiểu như của Windows. Do đó có thể đặt tên tệp tuỳ ý, kể cả tên chứa nhiều dấu chấm “.”

- Tệp có tên bắt đầu bằng dấu chấm là tệp ẩn.

- UNIX/Linux nhậy cảm với chữ HOA, chữ thường (case sensitive). Để tiện lợi, nhiều người thường dùng chữ thường (lower case). Ta cũng nên theo cách này.

- Tất cả các tệp trong linux có chung cấu trúc vật lý là chuỗi các byte (byte stream). Cấu trúc thống nhất này cho phép linux áp dụng khái niệm tệp cho mọi thành phần dữ liệu trong hệ thống. Thư mục cũng như các thiết bị được xem như tệp. Chính việc xem mọi thứ như các tệp cho phép linux quản lý và chuyển đổi dữ liệu một cách dễ dàng. Một thư mục chứa các thông tin về thư mục, được tổ chức theo một định dạng đặc biệt. Các thành phần được xem như các tệp, chúng được phân biệt dựa trên kiểu tệp: tệp thông thường (ordinary file), thư mục (directory) , thiết bị kiểu kí tự (character device file), và thiêt bị kiểu khối dữ liệu (block device file).
![Cây thư mục Linux](../images/dirct.png)

|Thư mục|Mô tả nội dung|
|-|-|
|/|	bắt đầu cấu trúc tệp, gọi là thư mục gốc (root)|
|/home|	chứa các thư mục nhà (home) của mọi tài khoản người dùng|
|/bin|	lưu trữ tất cả các câu lệnh chuẩn và các chương trình tiện ích|
|/usr|	chứa các tệp, câu lệnh được hệ thống sử dụng, thư mục này được chia thành các thư mục con khác|
|/usr/bin|	chứa các câu lệnh hướng người dùng và các chương trình tiện ích|
|/usr/sbin|	chứa các câu lệnh quản trị hệ thống|
|/usr/lib|	chứa thư viện cho các ngôn ngữ lập trình|
|/usr/doc|	chứa tài liệu của linux|
|/usr/man|	chứa các tệp hướng dẫn sử dụng cho các câu lệnh (man)|
|/sbin|	chứa các tệp hệ thống để khởi động hệ thống|
|/dev|	chứa giao diện cho các thiết bị như đầu cuối và máy in|
|/etc|	chứa tệp cấu hình hệ thống và các tệp hệ thống khác|


4.2 _Thuộc tính tệp_

- Mỗi tệp/thư mục có các thuộc tính quy định chủ định chủ sở hữu và mức khai thác được phép giành cho những chủ sở hữu, nhóm chủ sở hữu và những người dùng khác. Ngoài ra có một số thuộc tính khác như: là thư mục hay tệp, kích thước, ngày giờ khởi tạo, cập nhật,...

- Có ba mức khai thác được phép là đọc, viết và cho thực hiện, ký hiệu tương ứng là r, w, và x.

- Khi danh sách tệp trong một thư mục được đọc với câu lệnh ls –l, các thuộc tính sẽ được hiển thị lần lượt như sau:

    + Kí tự đầu tiên là “d” hoặc “-“ nghĩa là thư mục hay tệp (d= directory, - = tệp)
    + Các quyền (r, w, x) đối với chủ sở hữu, nhóm và other, tương ứng từng cụm 3 kí tự
    + Tên chủ sở hữu (owner),
    + Tên nhóm sở hữu,
    + Kích thước (theo byte),
    + Thời gian (timestamp),
    + Tên.

Ví dụ:

>-rwxr--r-- 1 user01 user01 2144 Mar 29 21:44 test.html

>drwxr-xr-x user01 user01 4096 Mar 29 21:55 xdir

- Thay đổi chủ sở hữu

>chown username filename

'Thay đổi chủ sở hữu tệp. Chỉ có tài khoản root có quyền thay đổi chủ sở hữu tệp.'

>chgrp groupname filename
'Thay đổi nhóm sở hữu tệp. Chỉ có root hoặc ngưòi tạo ra tệp đó (chủ sở hữu) có quyền thay đổi nhóm sở hữu tệp.'

- Thay đổi hạn chế khai thác tệp

>chmod [u|g|o][+|-][r|w|x ] <tên tệp>

- Thay đổi cách thức truy nhập tệp. Trong đó:

    ‘+’: Gán thêm quyền truy nhập tệp

    ‘-‘: Loại bỏ quyền truy nhập tệp

- Quyền truy nhập tệp được thể hiện bởi các ký tự: r (đọc), w (viết), x (thực hiện)
Nhóm người dùng được thể hiện bởi các ký tự: u (người dùng), g (nhóm), o (những người dùng khác).
Ví dụ:

>chmod g+rw mydata: gán cho nhóm có quyền đọc và thực hiện tệp mydata

>chmod o+r mydata: mọi ngưòi dùng có thể đọc tệp mydata

- Người dùng có thể gán thêm quyền cũng như loại bỏ quyền truy nhập tệp

>chmod o+r-wx mydata: mọi người dùng có thể đọc tệp, nhưng không được viết và thực hiện.

- Ngoài ra người dùng có thể thay các số thập phân cho các ký tự trên, gọi là phương thức tuyệt đối. Phương thức này sử dụng mặt nạ nhị phân thể hiện tất cả các quyền trong một phân mục. Ba phân mục (u,g,o) với 3 phân quyền chiếu theo một định dạng số bát phân. Khi chuyển sang số nhị phân (0/1), mỗi số hệ bát phân chuyển thành 3 số hệ nhị phân. Ba số bát phân chuyển hành 3 bộ số nhị phân (ví dụ: 777=111111111) , biễu diễn chính xác quyền truy nhập tệp.


## 5. Công cụ quản lí gói apt - tiện ích dpkg

5.1 _Kho chứa_
- Là nơi lưu trữ các gói cài đặt và cập nhật của Linux tại các máy chủ từ xa mà bạn có thể tải xuống và cài đặt các gói sử dụng chương trình quản lí gói
- /etc/apt/sources.list: file chứa một hoặc nhiều địa chỉ URL kho chứa từ xa để tải xuống và cài đặt gói cho máy tính Linux
- /etc/apt/sources.list.d: thư mục chứa các files riêng lẻ xác định một hoặc nhiều địa chỉ URL kho chứa từ xa để tải xuống và cài đặt gói. Mỗi file có thể chứa các dòng cho cả gói (file nhị phân) và mã nguồn gói
- apt (Advanced package tool): Một bộ công cụ (apt-*) để làm việc với các gói Debian đầy đủ và các thành phần phụ thuộc của chúng
- apt-get: Trình quản lí gói có thể cài đặt, cài đặt lại hoặc gỡ bỏ các gói và tất cả các thành phần phụ thuộc của chúng.
    + _update_ làm mới danh sách gói từ xa
    + _upgrade_ nâng cấp hệ thống linux hiện có lên phiên bản mới có sẵn trong kho chứa
    + _install_ [tên gói] cài đặt gói được chỉ định (không phải file .deb mà là tên gói chung) cùng với tất cả các thành phần phụ thuộc của nó
    + _dist-upgrade_ nâng cấp tất cả các gói có sẵn trong kho chứa trên hệ thống cùng 1 lúc trong phiên bản ubuntu/debian tiếp theo
    + _remove_ [tên gói] gỡ bỏ các files của gói **nhưng các file và thư mục cấu hình sẽ giữ nguyên**
    + _purge_ [tên gói] gỡ bỏ các file **và tất cả các file thư mục và cấu hình**
    + -d hoặc --download-only [tên gói] chỉ tải xuống gói *.deb* và lưu nó trong **/var/cache/apt/archives**
- apt-cache: cho phép tương tác với bộ nhớ cache các gói có sẵn từ tất cả các kho chứa được cấu hình
    + _search_ [giá trị] - tìm kiếm các gói phù hợp với giá trị được chỉ định
    + _show_ [tên gói] - hiện thị thông tin có sẵn về gói được chỉ định
    + _showpkg_ [tên gói] - thông tin kĩ thuật bổ sung về gói (các phụ thuộc, ..)

5.2 _Tiện ích dpkg_

5.3 _Thực hành_

## 6. Các thao tác với file

6.1 _Xử lí luồng văn bản_

6.2 _Quản lí cơ bản các file_

- Các lệnh và thuật ngữ cần biết: **ls, cd, pwd, mkdir, file, globbing, touch, stat, cp, mv, dd, rmdir, rm, find**

- Thao tác với file nén

- Thực hành:

6.3 _Trình soạn thảo vim_


