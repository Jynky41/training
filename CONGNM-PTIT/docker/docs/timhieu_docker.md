# Tìm hiểu Docker

### 1. Docker là gì? So sánh Docker & VM
### 2. Kiến trúc và các thành phần của Docker
### 3. Các thao tác với docker daemon
### 4. Xây dựng image bằng Dockerfile
### 5. Deploy ứng dụng bằng Docker Compose
### 6. Một số lưu ý khi làm việc với Docker

1. Docker là gì? So sánh Docker vs VM

- Docker là một công nghệ dựa trên ảo hóa. Giúp dễ dàng triển khai các ứng dụng, độc lập nền tảng bên dưới
- Docker cần một nhân Linux để có thể chạy.
- So sánh Docker và VM
![Docker vs VM](../images/docker-VM.png)

|Tiêu chí|Docker|VM|
|-|-|-|
|Công nghệ ảo hóa|Ảo hóa Hệ điều hành|Ảo hóa mức vật lý|
|Dung lượng|nhẹ (cỡ vài chục - vài trăm Mb)|nặng (Gb) do có hệ điều hành|
|Hiệu năng|nhanh (khởi động cỡ vài s)|chậm (do quá trình boot hệ điều hành)|
|Linh hoạt|Phụ thuộc vào Linux|Tùy chọn hđh với các .iso có sẵn|
|Điểm mạnh|Triển khai ứng dụng|Triển khai hạ tầng|

2. Kiến trúc và thành phần của docker

![Kiến trúc docker](../images/docker-arch.png)

- Client: sử dụng các CLI để tương tác với docker
- Docker Enginee: 
  + Cung cấp tập lệnh CLI cho client và các API giao tiếp với docker registry
  + Quản lí image, quản lí container, quản lí các tham số hệ thống khác
- Docker registry: Nơi lưu trữ các images sẵn sàng để sử dụng

3. Một số khái niệm trong docker

3.1 _network_

- Network giúp các container giao tiếp với nhau, giao tiếp giữa container và mạng bên ngoài, giữa container và cụm docker swarm. Các loại network thường dùng trong docker:

  + Bridge: (Mặc định của docker) Tạo ra một giao diện mạng từ 1 subnet chưa sử dụng của máy host. Các container triển khai theo mạng này được cấp địa chỉ ip trong phạm vi của subnet đó. Tuy nhiên các container *không trực tiếp biết về thông tin ip của nhau* mà cần sử dụng 'links' để các container có thể giao tiếp với nhau thông qua tên hoặc ip (or khai báo/join network). Đối với docker compose, mặc định sẽ tạo ra một _network_default_ cho các container khi được UP

  + Host: Các container sử dụng chung mạng(chung địa chỉ ip) với máy host docker. Điều này thuận tiện cho các ứng dụng cần giao tiếp với host mà không cần phải cấu hình mạng phức tạp. NOTE: khi kiểm tra ip của container thì kết quả trả về là "" tức là không có ip. Ở chế độ mạng này thì các container phân biệt với nhau bằng địa chỉ cổng (port), tên không còn ý nghĩa. *Việc ánh xạ cổng dịch vụ lúc này không còn ý nghĩa nữa*

  + None: Các container bị cô lập về mạng (Không có mạng)

  + Overlay: Thường được sử dụng trong docker swarm

  + Macvlan:

- Có 2 cách khai báo network trong docker-compose:
  + Sử dụng tham số *network_mode*
  + Sử dụng tham số *network <tên network>* 

- Một số lệnh thao tác với network:





3.2 _volume_

- Khái niệm volume trong docker được sinh ra để giải quyết vấn đề liên quan đến việc lưu trữ dữ liệu lâu dài và share các dữ liệu này cho các container khác
- Nó được thực hiện bằng việc ánh xạ một vùng chứa (fouder) nằm trên host docker lên một fouder nằm trên container nhằm thay thế fouder của container này
- Nếu tạo một container với một volume liên kết trong khi volume đó chưa có sẵn thì nó sẽ được Docker Engine tạo mới, nội dung của vị trí đích trong container sẽ được sao chép vào volume, sau đó volume mới được liên kết với container

- Cấp quyền truy cập cho volume: Mặc định ở chế độ rw, thay đổi quyền chỉ cho phép container đọc volume được mount (:ro)
- Sử dụng driver volume để mount các volume từ xa (sshfs,...)
- Backup và restore

3.3 _Docker enginee_
- Docker daemon là một phần của Docker Enginee. Docker Enginee bao gồm: 
    + Một tiến trình server chạy nền dockerd
    + Một tập API giao tiếp với daemon
    + giao diện CLI cho máy khách docker

4. Xây dựng image bằng Dockerfile

- Bước 1: Viết Dockerfile. Một Dockerfile demo sẽ trông như thế này

![](../images/dockerfile.png)

  + Đây là một Dockerfile viết cho ứng dụng java Springboot

  + Tối ưu Dockerfile (Docker layering):<br>
    *Image Layers*: Mỗi Docker image được tạo thành từ nhiều tầng lớp. Mỗi lệnh hoặc một tập hợp các lệnh trong một Dockerfile tạo ra một tầng lớp mới. Ví dụ, lệnh RUN, COPY, ADD trong Dockerfile sẽ tạo ra các tầng lớp mới.'

    *Union File System*: Docker sử dụng các hệ thống file union (như OverlayFS, AUFS, btrfs, và ZFS) để quản lý các tầng lớp này. UnionFS cho phép Docker chồng các tầng lớp này lên nhau và hiển thị chúng như một hệ thống file duy nhất.

    *Layer Caching*: Docker sử dụng cơ chế caching để tăng tốc quá trình build image. Nếu một tầng lớp đã tồn tại và không thay đổi, Docker sẽ sử dụng lại tầng lớp đó thay vì tạo mới. Điều này giúp giảm thời gian build và tài nguyên cần thiết.'

- Bước 2: Build image

>docker build <path to Dockerfile>

- Bước 3: Gắn tag cho image vừa tạo

>docker tag my-app:latest yourusername/my-app:latest

- Bước 4: Tạo repo và đẩy image lên lưu trữ lâu dài

>docker push yourusername/my-app:latest

'NOTE: Đăng nhập và tạo repo docker hub trước khi push'

**Thực hành tạo image bằng Dockerfile**


**Thực hành triển khai Prometheus & Grafana sử dụng docker compose** (VM3)

- Mô hình triển khai

![Mohinhtrienkhai](../images/mohinhlab1.png)

- Các bước triển khai:

  + Tạo docker-compose.yml
  + Tạo các volume chứa các file cấu hình cho từng hệ thống
  + Up and Test

- Các bước triển khai chi tiết (Thuyết trình)

- link source: [link project](.....)

- Tham khảo: 
    + https://github.com/vanduc95/mon_log_stack
    + https://github.com/thanhntdevops/monitoring-lab
    + https://hub.docker.com/r/prom/prometheus
    + https://hub.docker.com/r/prom/node-exporter
    + https://github.com/google/cadvisor
    + https://hub.docker.com/r/prom/alertmanager
    + https://laptrinhblockchain.net/huong-dan-su-dung-prometheus-va-grafana-de-giam-sat-he-thong-server/

    + https://velenux.wordpress.com/2022/09/12/how-to-configure-prometheus-alertmanager-to-send-alerts-to-telegram/  

    + https://prometheus.io/docs/guides/basic-auth/ 

- version '' : trong file docker-compose.yml có mối liên hệ như thế nào với docker-compose --version

- docker run/docker-compose up: Lỗi không thể kết nối đến docker daemon mặc dù nó đang active? sử dụng thêm sudo trước lệnh thì lại được?

'privileged: true
devices:
  - /dev/kmsg:/dev/kmsg' đặc quyền đặc biệt cho container (~root) cho phép truy cập thay đổi cấu hình host. devices ~ volume nhưng là mount phần cứng vào container


công việc ngày 8/7/2024:
+ Cấu hình alert rule cho prometheus (done)
+ viết file cấu hình alertmanager.yml (done)
+ viết file cấu hình haproxy.cfg (done)
+ chỉnh sửa lại file prometheus.yml (done)
+ docker-compose up (done)
+ test and fix (alert)
+ hoàn thiện docx (chưa hoàn thành)

công việc ngày 9/7/2024
+ viết lại file haproxy.cfg (done)
+ test alert email (bug) --> chuyển qua telegram (done)
+ viết template cho msg tele (done)
+ cấu hình basic auth cho prometheus (done)
+ đặt các biến môi trường port
+ setup grafana (done)
+ viết docx


công việc lab thầy Minh
+ tạo image project java + react + kafka + zookeeper tích hợp vào docker compose
+ viết hoàn chỉnh file cấu hình rules.yml (tìm hiểu sâu hơn về các mức độ cảnh báo + handmade mức độ cảnh báo)
+ đối với alert manager cần thực hiện chia rõ cảnh báo nào gửi cho ai
+ tích hợp hệ thống log 
+ commit project lên git + bàn giao team hệ thống 















