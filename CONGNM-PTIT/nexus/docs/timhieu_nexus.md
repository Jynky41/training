# Tìm hiểu về Nexus Repository OSS

## 1. Yêu cầu hệ thống

- Yêu cầu hệ thống nexus repository

    + Nexus Repository hỗ trợ các bản cài đặt trên cả Windowns, MacOS và Linux 

    + User: Sử dụng 1 user riêng cho các process nexus, user này phải có khả năng tạo 1 shell hợp lệ. 
    Vì lí do bảo mật, không nên sử dụng user root.

    + File descriptors: Mỗi 1 tiến trình được chạy, hệ điều hành sẽ cấp cho nó 1 chỉ số file descriptors.
    Chỉ số này đặt ra giới hạn các thao tác đọc/ghi/đóng file mà tiến trình đó có thể thực hiện. Nó kiểm soát
    việc tránh tình trạng 1 tiến trình làm cạn kiệt tài nguyên hệ thống 1 cách nhanh chóng. Nexus repo yêu cầu
    1 lượng lớn file descriptors do phải xử lí nhiều kết nối/file/các tài nguyên khác...Vì vậy, giới hạn mặc
    định có thể không đủ, nếu Nexus sử dụng vượt quá file descriptors, nó có thể dẫn đến việc dữ liệu bị hỏng
    hoặc mất dữ liệu. Do đó cần tăng chỉ số này lên đối với các tiến trình nexus

    + java: <br>
        java 8: hỗ trợ tất cả các bản nexus đã phát hành <br>
        java 11: hỗ trợ các phiên bản từ 3.67.0 trở lên <br>
        java 17: hỗ trợ 3.69.0 trở lên. Không hỗ trợ OrienDB<br>

    + CPU: <br>
        Tối thiểu 4 CPUs <br>
        Hiệu suất của Nexus repo bị giới hạn bởi hiệu năng IO (disk and networks) thay vì CPU<br>

    + Memory: <br>
        _JVM Heap_: Không tăng vùng nhớ heap lớn hơn khuyến nghị hoặc cấu hình min-max heap khác nhau, nó sẽ
        dẫn đến vấn đề về hiệu năng. Yêu cầu heap >= 2703M<br>
        _JVM Direct_: Chỉ yêu cầu bắt buộc khi làm việc với OrienDB, min=2703, max=(host physical/RAM * 2/3) - JVM max heap <br>
        _RAM_: min 8G, max nolimit, max heap + max direct memory <= RAM * 2/3<br>

    + Một vài đề xuất triển khai Nexus Repository theo profile size

|Profile size|Profile Description|CPU|Disk(Blob)|RAM|
|-|-|-|-|-|
|Small|<20 repositories<br> <20GB total blob store size <br> single repository format type|8|20GB|8GB|
|Medium|<50 repositories<br> <200GB total blob store size<br> a few repository formats|8+|200GB|16GB|
|Large|<200 repositories<br> > 200GB total blob size<br> diverse repository formats<br> NOTE: use a PostgreSQL database|12+|>200GB|32GB|
|Very Large|200+ repositories<br> 10TB total blob store size<br> diverse repository formats|16+|>10TB|64GB|

    + Dung lượng của gói cài đặt khoảng 330Mb. Khi sử dụng docker, con số này có thể lớn hơn 1 chút

    + Khi dung lượng ổ đĩa trống giảm xuống dưới 4GB, cơ sở dữ liệu sẽ chuyển sang chế độ chỉ đọc. Trong lần khởi động đầu tiên, trình quản lý kho lưu trữ sẽ tạo các tệp cơ sở cần thiết để hoạt động. Phần lớn dung lượng ổ đĩa sẽ được giữ bởi các tạo phẩm được triển khai và ủy quyền của bạn, cũng như mọi chỉ mục tìm kiếm. Điều này mang tính cài đặt cụ thể cao và sẽ phụ thuộc vào các định dạng kho lưu trữ được sử dụng, số lượng tạo phẩm được lưu trữ, quy mô nhóm và dự án, v.v. Tuy nhiên, tốt nhất nên lập kế hoạch cho nhiều định dạng như Docker và Maven có thể sử dụng dung lượng lưu trữ rất lớn.

    + File Systems: Nexus Repository yêu cầu sử dụng 1 trong 2 loại storage cơ bản: Embedded data (H2, OrientDB, Elastic Search) hoặc Blob storage
    + Hệ thống HA Nexus repository yêu cầu bản Pro

## 2. Một vài kiến trúc Nexus Repository tham khảo triển khai

## 3. Quản lí kho lưu trữ

### 3.1 Các tham số cấu hình 1 kho lưu trữ

- Name: Là mã định danh sẽ được sử dụng trong URL để truy cập kho lưu trữ. Name là duy nhất và là bắt buộc
- Format: Là định dạng mà nexus sử dụng để hiển thị kho lưu trữ cho các công cụ khác. VD: docker, apt, yum, raw,...
- Type: Loại repository triển khai: hosted, proxy, group
- URL: Hiển thị URL hướng đến người dùng cho kho lưu trữ này. Maven và các công cụ khác có thể truy cập kho lưu trữ trực tiếp tại URL đó. VD: http://192.168.56.20:8081/repository/ubuntu
- Online Checkbox: Xác định kho lưu trữ có khả dụng với các công cụ phía máy khách hay không. Đánh dấu tích vào để làm cho kho lưu trữ khả dụng, bỏ chọn để làm cho nó không khả dụng
- Blob Store: Cho biết blob store nào trong số các blob store khả dụng mà kho lưu trữ này sẽ sử dụng để lưu trữ các phần nhị phân của nó
- Strict Content Type Validation: Khi được chọn, nó sẽ xác thực kiểm tra xem loại MIME của tất cả các tệp được xuất bản vào kho lưu trữ có tuân thủ các loại được phép đối với định dạng kho lưu trữ đó hay không
- Cleanup Policies: Cung cấp các chính sách clean có thể được sử dụng đối với các kho hosted và proxy. Khi tác vụ được thực thi, nexus sẽ tôn trọng các chính sách clean mà bạn đã đặt ra trong cột Applied cho kho lưu trữ này và xóa các thành phần theo định nghĩa của các chính sách dọn dẹp đó. Cuối cùng, chính sách clean này cung cấp một cách để kiểm soát dung lượng đĩa của bạn và đảm bảo các mục không sử dụng sẽ được xóa khỏi Nexus Repository. Theo mặc định, phần Applied là trống.
- Deployment Policy: (Chỉ khả dụng cho hosted) Nó kiểm soát cách kho lưu trữ cho phép hoặc không cho phép triể khai các thành phần. Các cấu hình bao gồm:
    + Disable redeploy (default): Mỗi máy khách chỉ có thể triển khai một thành phần cụ thể 1 lần, bất kì nỗ lực triển khai lại thành phần nào cũng sẽ dẫn đến lỗi
    + Allow redeploy: Client có thể triển khai các thành phần vào kho lưu trữ này và ghi đè lên cùng thành phần đó trong các lần triển khai tiếp theo
    + Read-only: Không được phép triển khai
    + Deploy by replication only: Đây là thiết lập không khuyến khích sử dụng
- Remote Storage: (Chỉ khả dụng cho proxy) Chứa URL cho kho lưu trữ từ xa mà kho lưu trữ này sẽ proxy đến.

- Use the Nexus Repository Truststore (Chỉ khả dụng với các kho lưu trữ proxy sử dụng https). Khi được bật, Nexus sẽ quản lí chứng chỉ SSL của kho lưu trữ từ xa.

- Block: (Chỉ khả dụng cho kho lưu trữ proxy): Chọn true để ngăn Nexus gửi yêu cầu ra ngoài đến kho lưu trữ từ xa

- Auto Blocking Enabled: (Chỉ khả dụng cho các kho lưu trữ proxy) Khi được bật Nexus sẽ tự động chặn kho lưu trữ proxy nếu kho lưu trữ từ xa không khả dụng. Trong khi kho lưu trữ proxy bị chặn, các thành phần vẫn sẽ được phục vụ cho máy khách từ bộ nhớ cache cục bộ, nhưng Nexus sẽ không cố gắng định vị một thành phần trong kho lưu trữ từu xa. Nexus Repository sẽ kiểm tra lại kho lưu trữ từ xa theo định kì và bỏ chặn proxy khi kho lưu trữ từ xa khả dụng

- Maximum Component Age: (Chỉ khả dụng cho kho proxy) Khi proxy nhận yêu cầu cho một thành phần, nó không yêu cầu phiên bản mới từ kho lưu trữ từ xa cho đến khi thành phần hiện tại cũ hơn _số phút_ được yêu cầu trong trường này


- Maximum Metadata Age: (Chỉ khả dụng với kho proxy) Nexus repository sẽ chỉ truy xuất các bản cập nhật metadata từ kho dữ liệu từ xa sau _số phút_ được cấu hình trong trường này

- Not Found Cache Enable and Not Found Cache TTL: (Chỉ khả dụng cho các kho proxy) Khi bộ nhớ đệm được bật, Nexus sẽ lưu trữ bộ nhớ đệm các phản hồi cho nội dung không có trong kho lưu trữ proxy. Nó sẽ lưu trữ bộ nhớ đệm này trong vòng _số phút_ cấu hình trong trường _Not Found cache TTL_.

- HTTP Authentication: (Chỉ khả dụng cho kho lưu trữ proxy) Option - Cấu hình quyền truy cập vào kho lưu trữ từ xa thông qua việc cung cấp thông tin xác thực

- Một số cấu hình đặc biệt
    + APT Repository: <br>
        Distribution: Bản phân phối cho phiên bản ubuntu (binoic, jammy, focal,...)<br>
        Flat: chon true nếu kho lưu trữ từ xa có định dạng phẳng (không sử dụng phân cấp 'dists')<br>
        Signing Key và Passphrase: (chỉ khả dụng với hosted) Khóa kí GPG toàn vẹn thông tin và chống chối bỏ
    + Docker Repository: <br>
        Docker Registry API Support: <br>
        Docker Index<br>
        Writable Repository: (chỉ khả dụng cho kho group) Định tuyến cụ thể các yêu cầu Push tới các kho cụ thể trong group

## 4. Cleanup policies

- Nếu không dọn dẹp các thành phần cũ và không sử dụng, các kho lưu trũ sẽ phát triển nhanh chóng theo thời gian. Điều này sẽ gây ra một số vấn đề như:
    + Chi phí lưu trữ tăng
    + Hiệu suất bị ảnh hưởng
    + Thời gian tìm kiếm truy xuất tăng
    + Việc sử dụng hết dung lượng lưu trữ có sẵn dẫn đến lỗi máy chủ
- Có thể tạo các cleanup policies và gán chúng cho một hoặc nhiều kho lưu trữ để quản lí dọn dẹp theo lịch trình - tự động xóa mềm các components khỏi kho lưu trữ. Xóa mềm có nghĩa là các components chỉ được đánh dấu để xóa và chưa bị xóa khỏi đĩa cứng. Để khôi phục các components bị xóa mềm ta cần chạy tác vụ Admin-Compact blob store (Admin-Compact blob store không áp dụng với S3)

- Yêu cầu bảo mật:
    + Chỉ những người dùng có quyền quản trị (nexus:*) mới co thể **đưa ra** chính sách cleanup. 
    + Bất kì người dùng nào có quyền chỉnh sửa kho lưu trữ chỉ có thể **điều chỉnh** các chính sách mà kho lưu trữ đang sử dụng
    + Quyền chỉnh sửa cleanup là: _nx-tasks-update_ hoặc _nx-tasks-all_
### 4.1 Tạo chính sách cleanup

- Có thể tạo và gán một hoặc nhiều chính sách cleanup cho các kho lưu trữ (bao gồm cả kho hosted và proxy). Tiêu chí cảu từng chính sách sẽ được AND lại với nhau và chỉ quyết định dọn dẹp những thành phần thỏa mãn tất cả các điều kiện được đưa ra
    + Name và format là các trường bắt buộc. Name không thể chỉnh sửa sau khi tạo chính sách. Các tiêu chí dọn dẹp thì có thể thay đổi tùy ý
    + Component Age (Days): Tiêu chí này đặt ra thời gian lưu giữ các components trước khi chúng bị xóa mềm. Điều này được tính toán dựa trên thời điểm component được tải xuống lần đầu tiên (đối với kho proxy) và thời điểm nó được tải lên kho lưu trữ hosted. Nếu tiêu chí được đặt thành 30 ngày, thì chính sách dọn dẹp sẽ xóa mềm các thành phần không sửa đổi(không có ai triển khai lại nội dung vào cùng một đường dẫn) trong vòng 30 ngày qua
    + Component Usage (Days): Tiêu chí này thiết lập thời gian lưu giữ nội dung dựa trên thời điểm thành phần được tải xuống sử dụng lần cuối. Nếu component chưa bao giờ được tải xuống, chính sách sẽ sử dụng ngày xuất bản hoặc ngày cập nhật làm mốc thời gian
    + Release Type: Sử dụng tiêu chí này để đặt chính sách dọn dẹp thành PRELEASES hoặc RELEASES. PRELEASES được xác định khác nhau theo định dạng:
|Format|What is considered prerelease|
|-|-|
|Maven|Các phiên bản có chứa -SNAPSHOT|
|npm|Các phiên bản có chứa kí tự "-"|
|yum|Tiêu đề không phân biệt chữ hoa thường chứa một trong các thuộc tính sau: alpha, beta,rc, pre, prerelease, snapshot|

    + Asset name matcher: 


### 4.2 Cleanup Task
- Khi khởi động máy chủ với khả năng cleanup được bật, Nexus Repository sẽ tự dộng tạo một tác vụ có tên là Cleanup Service với loại Admin-Cleanup repository sử dụng các chính sách liên quan. Theo mặc định, tác vụ này được lên lịch hằng ngày vào lúc 1 giờ sáng theo giờ máy chủ. Nếu xóa tác vụ này, Nexus repository sẽ tự động tạo lại tác vụ đó khi khởi động lại máy chủ. Khi tác vụ này được chạy sẽ xóa mềm các thành phần dựa trên các chính sách dọn dẹp đã cấu hình

### 4.3 Chiến lược dọn dẹp Docker
- Docker có cách quản lí compoments và assets riêng. Do đó cần phải thiết kế lại chiến lược dọn dẹp. Bảng sau sẽ phác thảo những tác vụ sẽ xóa tạm thời cho kho lưu trữ docker:

|Optimal Order|Task Type|Feature|What it deletes|
|-|-|-|-|
|1|Docker-Delete incomplete uploads|Tasks|Xóa mềm các tải lên bị treo trong kho lưu trữ blob tạm thời chưa được tiếp tục|
|2|Admin-Cleanup repositories using their associated policies|Cleanup policies|Xóa mềm các thành phần docker cũ đã xuất bản hoặc tải xuống dựa trên tag|
|3|Docker-Delete unused manifests and images|Task|...|

## 5. Routing Rules
- Tạo hoặc chỉnh sửa routing rules cần quyền _nx-all_
- Mode:
    + BLOCK: Khi đường dẫn yêu cầu khớp với bộ matcher, yêu cầu sẽ bị chặn
    + ALLOW: Khi đường dẫn yêu cầu khớp với mộ matcher, yêu cầu sẽ được chấp nhận chuyển tiếp
- Matchers: Có thể gồm nhiều biểu thức chính quy hợp lệ được java cho phép

- Routing rules là công cụ cực kỳ hữu ích nhưng thường bị bỏ qua. Quản trị viên sử dụng routing rules để giới hạn các yêu cầu đến kho lưu trữ bên ngoài chỉ đối với các nội dung cần thiết từ proxy. Routing rules có thể giúp giải quyết tình trạng nhầm lẫn phụ thuộc như các cuộc tấn công vào namespace nội bộ của tổ chức. Routing rules sẽ tăng tốc thời gian truy cập cho kho lưu trữ group bằng cách giới hạn các yêu cầu chỉ đối với các proxy nơi các thành phần cần được truy xuất.

## 6. Blob Store
### 6.1 Giới thiệu
- Các giá trị nhị phân của một repository được lưu trữ trong blob store. Ta có thể cấu hình blob stores mới bằng cách điều hướng đến Adminitration -> Repository -> Blob store
- Yêu cầu quyền _nx-all_ hoặc _nx-blobstore_
![Blob Store](../images/blob_store1.png)
- Các trường sau đây xuất hiện trong phần hiển thị thông tin Blob store:
    + Name: Tên Blob store được hiển thị
    + Type: Loại backend blob store. Trong bản OSS khả dụng 2 loại đó là File và S3
    + State: Trạng thái của Blob store
        + Started: Cho biết nó đang chạy như mong đợi
        + Failed: Cho biết có sự cố về cấu hình và do đó, blob store không khởi tạo được
    + Blob Count: Số lượng blob hiện đang được lưu trữ trong blob store
    + Total Size: Đối với hầu hết các blob stores, đây là kích thước gần đúng trên disk (byte). Đối với S3, con số này sẽ không bao gồm các blob đối tượng được đánh dấu là hết hạn nhưng chưa bị xóa 
    + Available: Dung lượng lưu trữ còn lại của blob store
- Khuyến nghị: Nên đặt vị trí blob store bên ngoài thư mục $data-dir và cho phép nút có thể đọc/ghi
### 6.2 Tạo một Blob Store mới
- Các bước để tạo một Blob store mới
    + Bước 1: Chọn nút Create Blob Store
    + Bước 2: Chọn Type và cung cấp Name cho kho blob của bạn
    + Bước 3: Đối với Blob store dạng File, trong trường Path, hãy cung cấp đường dẫn tuyệt đối đến vị trí filesystem mong muốn. Tài khoản người dùng hệ điều hành đang chạy Nexus Repository phải có thể truy cập đầy đủ vào đường dẫn này
    + Đặt Soft Quota (Hạn ngạch mềm): Khi đặt hạn ngạch mềm, Nexus sẽ theo dõi kho lưu trữ blob và đưa ra cảnh báo khi vượt quá giới hạn đã cấu hình. Có 2 kiểu hạn ngạch được đưa ra cảnh báo đó là Space Used và Space Remaining. Khi hạn ngạch mềm bị vi phạm, thông tin đó sẽ có sẵn ở những vị trí sau:
        + WARN level trong logs
        + Administration -> Support -> Status
        + Điểm cuối REST API

![Create Blob Store](../images/create_blobstore1.png)  

- Sau khi tạo một blob store sẽ không thể sửa đổi nó. Cũng không thể xóa bất cứ Blob store nào mà một repository đang sử dụng
- Nhóm Blob store: 

## 7. Access Control
- Nexus sử dụng role-based access control (RBAC) để kiểm soát quyền truy cập của người dùng
### 7.1 Các khái niệm được sử dụng:
-  Privileges: Tập hợp các quyền hạn được gán cho role

    + Privilege Types:

|Type|Permission Segment|Applicable|Description|
|-|-|-|-|
|Application<br>application|nexus:{name}:{actions}|create,read,update,delete|Ex: nexus:blobstore:create,read|
|Repository Admin<br>repository-admin|nexus:repository-admin:{format}:{repository}:{actions}|browse,read,edit,add,delete|Ex: nexus:repository-admin:apt:apt-proxy:browse,add|
|Repository Content Selector<br>repository-content-selector|nexus:repository-content-selector:{selector}:{format}:{repository}:{actions}|browse,read,edit,add,delete|Ex: nexus:repository-content-selector:*:maven2: *:read|
|Repository View<br>view|nexus:repository-view:{format}:{repository}:{actions}|browse,read,edit,add,delete|Ex: nexus:repository-view:maven2:central:browse,read|
|Script<br>script|nexus:script:{scrip name}:{actions}|browse,read,edit,add,delete,run|Ex: nexus:script:*:read|
|Wildcard|*|*|Ex: nexus:*|

    + Privilege Permissions: Dánh sách các permissions

    + Privilege Actions

|Action|Description|
|-|-|
|add|cho phép thêm nội dung hoặc script vào kho lưu trữ|
|browse|Cho phép xem nội dung của kho lưu trữ (chỉ khả dụng trên webUI)|
|create|cho phép tạo/cấu hình áp dụng cho trình quản lí kho lưu trữ. Thường đi kèm với quyền read|
|delete|cho phép xóa cấu hình repository, nội dung và script trong repository. Thường đi kèm với quyền read|
|edit|sửa đổi script, nội dung trong repository và các cài đặt khác|
|read|cho phép đọc đa dạng các cấu hình và script. Nếu không có quyền read, các hành động liên quan như create, delete sẽ chỉ được xem tiêu đề(tên) chứ không phải nội dung của nó. Cho phép xem nội dung từ dòng lệnh|
|update|cho phép cập nhật cấu hình trình quản lí kho lưu trữ. Thường đi kèm với quyền read|


- Role: Tập hợp các privileges nhóm lại với nhau phục vụ một mục đích cụ thể. Để tạo 1 role mới:
    + Chọn tập hợp các privileges
![Applied Privileges](../images/role_setup1.png)
    + Kế thừa lại các role đã có
![Applied Roles](../images/role_setup2.png)

- User


## 3. Cài đặt Nexus Repository sử dụng docker image

- Image:   [sonatype/nexus3:3.70.1-java11-alpine](https://hub.docker.com/layers/sonatype/nexus3/3.70.1-java11-alpine/images/sha256-71ff1e39711d8f32f177b8c20306aceb14da6c0b22658b7564975e9b06a4a75f?context=explore)

- Lệnh run container: docker run -d --name nexus --network=host -u nexus:nexus -v /nexus-data:/sonatype-work sonatype/nexus3:3.70.1-java11-alpine

- Truy cập WebUI tại địa chỉ: <ip/hostname>:8081 (mặc định)
![Home Page](../images/homepage.png)

    + Tại đây, ta có thể xem xét các tham số thống kê như Tổng số repository(Components), Số lượng request cao nhất trong ngày/tháng, số lượng user logins, ngưỡng threshold, system health check,...
- Nexus hỗ trợ đa dạng các repository format như apt, yum, raw, maven, docker,... cùng với 3 kiểu triển khai: hosted, proxy, group.
    + Hosted: Repository kiểu private, phục vụ pull/push dữ liệu trong mạng LAN khi được cấp quyền
    + Proxy: Đóng vai trò như một máy chủ proxy chuyển tiếp các yêu cầu pull/push các file/gói/image/... ra bên ngoài internet [Tài liệu chi tiết](https://help.sonatype.com/en/proxy-repository-concepts.html)
    + Group: Sự kết hợp giữa hosted và proxy. Khi client yêu cầu một file/gói/image/... nó sẽ tìm kiếm trong kho hosted trước. Nếu có sẽ trả về cho client, nếu không sẽ sử dụng proxy đi ra ngoài internet tìm và lưu đệm lại để sử dụng cho lần sau. Điều này giúp làm giảm lưu lượng ra ngoài internet. Ngoài ra khi sử dụng group, ta chỉ cần khai báo 1 url duy nhất của group thay vì phải khai báo nhiều url đơn lẻ

### 3.1 Thực hành tạo docker repository

    Docker client không cho phép URL là một phần của đường dẫn đến repository, vì không gian tên và tên hình ảnh được nhúng trong các URL mà nó sử dụng. Đây là lý do tại sao các yêu cầu đến kho lưu trữ trên trình quản lý kho lưu trữ được phục vụ trên một cổng cụ thể và riêng biệt với phần còn lại của ứng dụng thay vì cách hầu hết các kho lưu trữ khác phục vụ nội dung thông qua một đường dẫn tức là <nexus-hostname>/<repositoryName>/<path to content>.

- Bước 1: Cấu hình Repository (docker proxy)

    + Giao diện cấu hình docker repository(proxy) như sau:
    ![Cấu hình UI docker proxy ảnh 1](../images/docker_proxy1.png)
    ![Cấu hình UI docker proxy ảnh 2](../images/docker_proxy2.png)

    + Repository Connectors: định nghĩa giao thức mà Repository triển khai (http hoặc https). Http dễ dàng triển khai hơn, tuy nhiên docker client lại yêu cầu https khi kết nối đến các kho. Bỏ qua điều này bằng cách cấu hình /etc/docker/daemon.json thêm dòng "insecure-registries": ["ip/hostname:port-repo"] để bỏ qua kết nối không an toàn. Hoặc có thể sử dụng một máy chủ proxy(nginx, apache,...) để trusted giải mã SSl và chuyển tiếp yêu cầu http đến kho lưu trữ. 
    Trường hợp sử dụng https cần cấu hình chứng chỉ SSL một cách thủ công
    + Allow anonymous docker pull: Cho phép người dùng ẩn danh tải về các image
    + Foreign Layer Caching: Cho phép repo lưu cache lại những image đã được truy cập từ ngoài internet. Cung cấp 1 regex url để kiểm tra match khi quyết định lưu trữ cache (mặc định là .*)
    + Stric Content Type Validation: Kiểm tra tính hợp lệ của định dạng content được up lên kho
    + Routing Rule

- Bước 2: Cấu hình docker client
    + Chỉnh sửa file /etc/docker/daemon.json thêm dòng "insecure-registries": ["ip/hostname:port-repo"], "registry-mirrors": ["http://<ip>:<repo-port>"]và khởi động lại docker daemon
    + Đăng nhập vào kho lưu trữ vừa tạo: _docker login <ip>:<repo_port>_ 
    + Kiểm tra hoạt động của docker proxy vừa tạo: _docker pull <ip>:<repo_port>/hello-world_. Nếu pull image thành công, kiểm tra trạng thái của repository trên webUI là *Online-Remote Available*

### 3.2 Thực hành tạo apt repository (hosted)



### 3.3 Thực hành tạo raw repository (proxy)



tìm hiểu cài đặt nexus repo oss
cấu hình VM ubuntu trỏ repo đến repo nexus (apt, dockerhub)




Docker Registry API v1/v2


Cấu hình auth cho apt host: 
- sửa file source.list 
- distribute phụ thuộc vào phiên bản linux VD: ubuntu 22.04 (jammy), 18.04 (bionic)
- fix authenticated : vi /etc/apt/auth.conf (note: machine <protocol>://<url>) (*)




APT repo 
- Demo tại controllnode và VM4

- download/install package lần đầu tiên có bị chậm do phải qua proxy(?) chậm hơn so với bình thường?

- Lỗi auth: khai báo 'machine' trong file auth.conf trong mỗi phiên bản ubuntu có sự khác nhau (chưa rõ nguyên nhân)
    với ubuntu 22.4 (ở đây là máy cái Nexus) cần thêm <protocol> trước hostname --> nếu không sẽ un author
    với ubuntu 18.4 (máy khách no intenet) không cần thêm protocol --> nếu thêm sẽ bị un auth (thử nghiệm trên vm4)

Docker repo

- sử dụng cổng thay vì path để phân biệt các repo docker khác nhau
- mặc định các docker client sẽ yêu cầu https đến các repo. Tuy nhiên mặc định docker repo của nexus triển khai http. Một số hướng giải quyết
    + cấu hình /etc/docker/daemon.json thêm dòng "insecure-registries": ["ip/hostname:port-repo"] để bỏ qua kết nối không an toàn
    + cấu hình rev proxy sẽ giải quyết 2 vấn đề: số lượng kết nối lớn/số lượng repo docker lớn + xử lí yêu cầu https giải cetificate và chuyển tiếp yêu cầu http
    đến nexus
    + docker login: lưu thông tin xác thực vào /root/.docker/config.json
- push/pull image
- docker content trust
- docker repo cache

- docker.service và docker.socket
- docker mirrors
- Demo docker proxy/hosted tại VM3 và controllnode