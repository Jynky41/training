# Kiến trúc Nexus Scale

## Trường hợp sử dụng
- Cải thiện hiệu năng hệ thống khi tải đọc lớn hoặc tăng đột biến
## Yêu cầu hệ thống
- Phiên bản mới nhất của Nexus Repository OSS hoặc PRO
- Triển khai tối thiểu 1 node chính và một vài node phụ đóng vai trò là proxy đến node chính
- Đề xuất về cấu hình nút chính:
    + CPUs: 16+
    + RAM: 64GB+
    + JVM: heap=6G, directmem=16G
    + Số luồng Jetty: 600
    + Kích thước lưu trữ
- Đề xuất cấu hình nút proxy:
    + CPUs: 16+
    + RAM: 32GB
    + JVM: heap=6G, directmem=16G
    + Số luồng Jetty: 400
    + Kích thước lưu trữ
- Bộ cân bằng tải: nginx, apache,...
- Lưu trữ riêng biệt cho mỗi nút phụ

## Kiến trúc triển khai

![Kiến trúc triển khai nexus scale](../images/nexus_scale_proxy.png)

- Node chính: Cấu hình bình thường. Giả sử đã tồn tại. La trung tâm đặt routing rules, giảm lượng cấu hình trên các node proxy
- Các node proxy:
    + Triển khai trong cùng một trung tâm dữ liệu với node chính
    + Các nút proxy là độc lập với các nút ngang hàng và với nút chính. Chúng không được chia sẻ blob store, thư mục ứng dụng hoặc cơ sở dữ liệu bên ngoài(nếu có)
    + Các node proxy sẽ tạo các repository proxy trỏ đến một hoặc một group repository trên nút chính
    + Cấu hình bộ nhớ đệm proxy: 
        + Maximum component age: thời gian lưu trữ tối đa của một artifact trong cache(khuyến nghị để mặc định)
        + Maximum metadata age: thời gian làm mới metadata(khuyến nghị sửa đổi thành 0 hoặc một giá trị dương nhỏ khác) để làm mới kho liên tục trong trường hợp node chính thường xuyên phát hành các phiên bản mới.
    + Cleanup policies: Đặt chính sách xóa tất cả các thành phần không được sử dụng trong vài tuần 
    + Số lượng node proxy: Phụ thuộc vào trường hợp sử dụng thực tế. Thông thường sẽ là 1 hoặc 2. Số lượng nút proxy nhiều hơn trong trường hợp ta muốn chia nhóm proxy cho từng nhóm repository cụ thể của node chính (điều này sẽ cải thiện hiệu năng truy cập vào bộ nhớ đệm). Ex: node proxy cho các yêu cầu apt, node proxy cho các yêu cầu docker,...
    + Xác thực: Nên sử dụng nhà cung cấp xác thực danh tính bên ngoài (LDAP, SAML) để thông tin xác thực được đồng bộ hóa cho từng người dùng
    + Các thay đổi (thêm, xóa) kho lưu trữ tại node chính cần phải cập nhật cho các node proxy. Đối với các node proxy tồn tại lâu dài, cách đơn giản nhất là sử dụng [Repositories REST API](https://help.sonatype.com/en/repositories-api.html) để thêm, sửa, xóa các cập nhật từ node chính.
    + Sao chép node proxy: Trong mô hình đề xuất, bất kì nút proxy mới nào được thêm vào cũng sẽ cần thời gian để cập nhật lại cache của mình. Điều này sẽ làm giảm hiệu suất chung của các máy khách khi truy xuất các nội dung trong kho(đặc biệt các kho lớn). Một kĩ thuật làm giảm thời gian chờ cache ổn định đó là copy một node proxy hiện có (bằng cách tạo Snapsort ở cấp độ VM hoặc storage) và sử dụng nó để khởi tạo 1 nút proxy mới.
    + Tỷ lệ hit và miss bộ nhớ cache có thể được tính toán từ _.service/metrics/data_. Tính toán các yêu cầu đến và đi thông qua node proxy
