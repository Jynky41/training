# Tìm hiểu cơ bản Prometheus

#### 1. Giới thiệu cơ bản về Prometheus

### 1.1. Monitoring là gì? Khác gì với Logging và Tracing

- Monitoring là một hệ thống dùng để thu thập thông tin về phần cứng và phần mềm của một hệ thống IT. Hệ thống monitoring có thể thu thập từ trạng thái của của server (CPU, memory, network, …) cho đến những thông tin chi tiết về cách server xử lý request đến từ người dùng (số lượng requests/s, thời gian server xử lý requests, …).

- Một hệ thống monitoring sẽ giúp chúng ta có một cái nhìn dễ dàng về chất lượng và những vấn đề xảy ra với sản phẩm. Nó sẽ tự động cảnh báo cho chúng ta ngay khi phát hiện ra điều bất thường. Bây giờ, thay vì được người dùng thông báo rằng server gặp trục trặc, chúng ta sẽ sớm phát hiện và giải quyết trước khi người dùng nhận ra. Hơn nữa, đối với các hệ thống đủ lớn và phức tạp, hệ thống monitoring còn giúp chúng ta biết được những thành phần nào hay gặp sự cố để có thể tập trung cải thiện chất lượng.

- Logging tập trung hơn vào việc ghi lại các sự kiện và thông báo cho mục đích phân tích, gỡ lỗi và giám sát. Mặt khác, Tracing tập trung vào việc theo dõi luồng yêu cầu hoặc giao dịch trong các hệ thống phân tán để hiểu các tương tác và chẩn đoán các vấn đề về hiệu suất.

### 1.2. Prometheus là gì

- Prometheus là một dịch vụ theo dõi và cảnh báo về hệ thống. Đây là một dịch vụ mã nguồn mở (Open source) hoàn toàn miễn phí. SoundCloud đã khởi đầu xây dựng Prometheus từ năm 2012. Prometheus đã được rất nhiều hệ thống tin tưởng áp dụng. Dự án có một cộng đồng người đóng góp, phát triển rất tích cực.

- Giờ đây Prometheus đã được tách khỏi SoundCloud và là một dự án mã nguồn mở độc lập. Năm 2016, Prometheus tham gia vào tổ chức CNCF (Cloud Native Computing Foundation) với vị trí được ưu tiên phát triển thứ hai sau K8s (Kubernetes).

- Tính năng quan trọng nhất của Prometheus là thu thập thông số, dữ liệu từ các mục tiêu (dịch vụ) được nhắm đến theo khoảng thời gian nhất định đã được cài đặt trước. Ngoài ra còn các API khác thể hiện được kết quả, đánh giá bằng biểu thức quy tắc và đưa ra cảnh báo. Prometheus còn cung cấp một ngôn ngữ truy vấn rất mạnh PromQL, cực kì hữu ích khi giao tiếp với các dịch vụ monitor khác.

- Một số tính năng của Prometheus
    + Mô hình dữ liệu Time series đặc biệt phù hợp để theo dõi số liệu theo thời gian.
    + Có ngôn ngữ truy vấn riêng PromQL rất mạnh mẽ.
    + Tích hợp tốt với rất nhiều nền tảng ứng dụng.
    + Yêu cầu hạ tầng để triển khai đơn giản.
    + Hỗ trợ Push Gateway (khi làm việc với các dịch vụ tồn tại trong thời gian ngắn và hủy sau khi hoàn thành).
    + Có hỗ trợ Service discovery hoặc cấu hình tĩnh để tìm, nhắm mục tiêu cần theo dõi.

- Các khái niệm trong Prometheus
    + Data model: Prometheus lưu trữ mọi thứ dưới dạng time series
    + Metric types: Có 4 kiểu metric được sử dụng trong prometheus:

        _counter_: là một bộ đếm tích lũy, được đặt về 0 khi restart. Ví dụ, có thể dùng counter để đếm số request được phục vụ, số lỗi, số task hoàn thành,... Không sử dụng cho gia trị có thể giảm như số tiến trình đang chạy.

        _gauge_: đại diện cho số liệu duy nhất, nó có thể lên hoặc xuống. Nó thường được sử dụng cho các giá trị đo

        _histogram_: lấy mẫu quan sát (thường là những thứ như là thời lượng yêu cầu, kích thước phản hồi). Nó cũng cung cấp tổng của các giá trị đó.

        _summary_: tương tự histogram, nó cung cấp tổng số các quan sát và tổng các giá trị đó, nó tính toán số lượng có thể cấu hình qua sliding time window.

    + Job and instance: Một instance là một nhãn (label) dùng để định danh duy nhất cho một target trong một job. Job là một tập hợp các target chung một nhóm mục đích. Ví dụ: giám sát một nhóm các dịch vụ database,… 

- Kiến trúc của Prometheus Stack

![Kiến trúc Prometheus](../images/prome-arch.png)

    + Các thành phần chính trong hệ thống Prometheus<br>

        Máy chủ Prometheus.<br>
        Thư viện kết nối đến máy chủ Prometheus cho các ứng dụng. (Có thể sử dụng dạng JSON nếu không có thư viện hỗ trợ)<br>
        Push Gateway Prometheus: sử dụng để hỗ trợ các job có thời gian thực hiện ngắn (tạm thời). Đơn giản là các tác vụ công việc này không tồn tại lâu đủ để Prometheus chủ động lấy dữ liệu. Vì vậy là mà các dữ liệu chỉ số (metric) sẽ được đẩy về Push Gateway rồi đẩy về Prometheus Server.<br>
        Exporter hỗ trợ giám sát các dịch vụ hệ thống và gửi về Prometheus theo chuẩn Prometheus mong muốn.
        AlertManager: dịch vụ quản lý, xử lý các cảnh báo (alert).<br>

- Nên sử dụng prometheus trong các dự án hệ thống phân tán như microservices, K8s,...do có hệ thống service discovery sịn
- Không nên sử dụng Prometheus trong các hạ tầng tĩnh, địa chỉ của các dịch vụ không thay đổi, các hệ thống yêu cầu lưu trữ dữ liệu lâu dài, các dịch vụ có số lượng metric lớn. Prometheus là một ứng dụng dành cho monitoring, *Không phải* là giải pháp cho Logging


### 2. Cấu hình Prometheus

- Chi tiết được tìm thấy tại [Trang chủ của Prometheus](https://prometheus.io/docs/prometheus/latest/configuration/)

#### 2.1 Cấu hình prometheus server

- /etc/prometheus/prometheus.yml là file cấu hình chính của hệ thống prometheus. Nó chứa các định nghĩa về các job, các instance trong 1 job, đường dẫn đến các file rules alert, khai báo các alertmanager để gửi thông báo,...
- Một file prometheus.yml cơ bản sẽ trông như sau

![](../images/prome-cfg.png)

#### 2.2 Rules file

- Rules file là một file cấu hình định dạng yml cho phép định nghĩa các recording rules
- Recording rule là một biểu thức tính toán để xác định các ngưỡng cảnh báo
- Một file alert.rules.yml cơ bản:

![](../images/prome-rules.png)

### 3. PromQL

- Các kiểu dữ liệu trả về:
    + Instant vector
    + Range vector
    + Scalar
    + String

>VD: http_requests_total{job="prometheus",group="canary"}[5m] > 1000

- Trong câu query trên: 
    + metric name: http_requests_total
    + lable : job="prometheus" , group="canary"
    + http_requests_total{job="prometheus",group="canary"} sẽ trả về instant vector
    + http_requests_total{job="prometheus",group="canary"}[5m] sẽ trả về range vector trong vòng 5 phút gần nhất
    + '>' toán tử so sánh (>=,<=, !=, ==, =~)
    + ngoài ra còn có thể kết hợp với các hàm toán học như sum(), avg(), abs(), max(), min(),....
    + phép toán số học +, -, *, /, %, ^
    + toán tử logic: and, or, unless
    + vector matching: group_right, group_left

### 4. Lable
- Lable là một cặp key-value sử dụng để lọc kết quả trả về từ một truy vấn

- Instrumentation là hành động tạo thêm các tham số đo (metrics name) nhằm tùy biến phù hợp với yêu cầu monitor đặt ra cho hệ thống. 

- Target label là một nhãn (label) được gắn vào mục tiêu cụ thể mà prometheus scrap dữ liệu. Nó sử dụng để nhận diện và phân loại nguồn dữ liệu trong kết quả của câu query. Có 2 loại target lables:
    + static label 
    + dynamic label: service discovery cung cấp hay còn gọi là relable

### 5. Lưu trữ

*Prometheus hỗ trợ 2 loại lưu trữ:*

- Local storage: Mặc định prometheus lưu trữ dữ liệu của mình trên ổ đĩa của server.
    + Ưu điểm:<br>
        Tốc độ truy cập nhanh
        Dễ triển khai
    + Nhược điểm:<br>
        Khó mở rộng khi dữ liệu scale

- Remote storage: Sử dụng hệ thống lưu trữ bên ngoài
    + Ưu điểm:<br>
        Lưu trữ dữ liệu không giới hạn
        Hỗ trợ scale, backup, restore
        Đa dạng hệ thống remote storage: Thanos, Cortex,...
    + Nhược điểm:<br>
        Trễ truy cập
        Cấu hình triển khai phức tạp

*Tính toán sizing*
- Bước 1: Xác định số target, số metrics được sinh ra từ mỗi target
- Bước 2: Xem xét cấu hình khoảng thời gian lấy mẫu --> Tính được số mẫu phải lưu trữ trong 1 ngày/1 tháng/1 năm/ ...
- Bước 3: Tìm hiểu dung lượng lưu trữ trung bình của một mẫu --> Nhân với số lượng mẫu trong bước 2 để tìm ra sizing
- Bước 4: Tính toán tối ưu dung lượng: VD nén dữ liệu,...


### 6. Alert Manager

- Alertmanager nhận các thông tin cảnh báo tới từ prometheus khi nó kích hoạt các rule. Một cảnh báo thường gồm: tên cảnh báo, nhãn, giá trị, và các chú thích
- Alertmanager nhóm các cảnh báo tương tự nhau lại thành 1 group và gửi đi, tránh tình trạng gửi quá nhiều thông báo giống nhau
- Alertmanager đóng vai trò người chuyển tiếp cảnh báo này đến với người giám sát (có thể là các ứng dụng gmail, webhook, ...)
- Alertmanager có khả năng xử lí các cảnh báo giả (inhibit)

- Silence Alerts trong Alertmanager là một cách để tạm thời ngăn chặn các cảnh báo không cần thiết hoặc không mong muốn. Đây là một tính năng hữu ích để tránh bị làm phiền bởi các cảnh báo trong các tình huống nhất định, chẳng hạn như trong quá trình bảo trì hệ thống hoặc khi bạn biết rằng có một vấn đề đang được giải quyết và không cần thêm thông báo.

- Cách Thức Hoạt Động của Silence Alerts: Silence là một quy tắc mà bạn tạo ra để ngăn chặn các cảnh báo dựa trên các nhãn (labels). Khi một cảnh báo trùng khớp với quy tắc của Silence, cảnh báo đó sẽ không được gửi đi. Silence có thể được tạo ra với thời gian bắt đầu và kết thúc cụ thể, và có thể bao gồm một lời nhắn để giải thích lý do của việc ngăn chặn.

- Cấu hình silence trên giao diện web của alert manager


### 7. Service Discovery

- Vào mỗi khoảng thời gian được định nghĩa (refesh time), Prometheus sẽ gửi một request GET đến SD yêu cầu cập nhật danh sách target
- SD nhận được request sẽ phải trả về cho prometheus server 1 response với mã thông báo 200 chứa danh sách các địa chỉ của các target.
- Nếu nhận được phản hồi, prometheus server sẽ cập nhật các target mà nó đang theo dõi, nếu không, nó sẽ tiếp tục sử dụng địa chỉ của các target cũ.
- Phản hồi của SD sẽ trông như thế này
![](../images/http-sd.png)



### 8. Prometheus federation

- Cho phép trao đổi thông tin metrics từ prometheus server này đến prometheus server khác
- Lợi ích của federation:
    + giám sát tập trung
    + dễ dàng mở rộng
    + phân chia tải cho các prom server

















