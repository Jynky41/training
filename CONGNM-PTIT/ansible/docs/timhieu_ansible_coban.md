# Tìm hiểu cơ bản về Ansible

### 1. Lí thuyết

- tags 

- dynamic inventory

- module debug and '- debug' ?

- changed_when and failed_when

- vault

- asynchronous task

- ansible galaxy

### 2. Thực hành: Sử dụng ansible triển khai prometheus và grafana

Lab trên Ansible controlnode
- Chuẩn bị:
    + 2 host triển khai prometheus
    + 1 host triển khai grafana
    + 2 host cài node_exporter

NOTE: 
- module command: toán tử & vẫn sẽ thực hiện lệnh song song chứ không tuần tự? ignore_errors?
- module shell hỗ trợ mở rộng shell trong khi command thì không
- tham số become: yes Thực hiện task bằng người dùng root
