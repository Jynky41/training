# Tìm hiểu về Git

## _I. Git là gì_

Git là một hệ thống quản lý phiên bản phân tán (Distributed Version Control System – DVCS), nó là một trong những hệ thống quản lý phiên bản phân tán phổ biến nhất hiện nay. Git cung cấp cho mỗi lập trình viên kho lưu trữ (repository) riêng chứa toàn bộ lịch sử thay đổi.

## _II. Git làm được những gì_
Git sẽ giúp bạn
- Lưu lại được các phiên bản khác nhau của mã nguồn dự án phần mềm
- Khôi phục lại mã nguồn từ một phiên bản bất kỳ
- Dễ dàng so sánh giữa các phiên bản
- Phát hiện được ai đã sửa phần nào làm phát sinh lỗi
- Khôi phục lại tập tin bị mất
- Dễ dàng thử nghiệm, mở rộng tính năng của dự án mà không làm ảnh hưởng đến phiên bản chính (master branch)
- Giúp phối hợp thực hiện dự án trong nhóm một cách hiệu quả

## _III. Các khái niệm cơ bản trong Git_
### 1. Repository

Là một cấu trúc lưu trữ chứa toàn bộ lịch sử thay đổi của một dự án. Repository giúp quản lý mã nguồn, theo dõi các phiên bản, và hỗ trợ công việc cộng tác của nhiều lập trình viên trên cùng một dự án.

_Các khái niệm liên quan_

#### a. Local Repository

Là kho lưu trữ trên máy tính của mình, không nằm trên máy chủ nào. Nó bao gồm:
- Working Directory: Nơi ta làm việc trực tiếp với các tệp tin.
- Staging Area (Index): Khu vực trung gian nơi ta tạm thời lưu trữ các thay đổi trước khi commit và repository.
- Repository: Kho lưu trữ chứa tất cả lịch sử commit của dự án.

#### b. Remote Repository

Kho lưu trữ nằm trên máy chủ hoặc dịch vụ trực tuyến (như GitHub, GitLab, Bitbucket), cho phép cộng tác và chia sẻ mã nguồn với người khác.

### 2. Branch

Nhánh là một dòng phát triển riêng của dự án. Mỗi nhánh có thể phát triển độc lập với các nhánh khác, cho phép ta tạo ra các tính năng mới, sửa lỗi hoặc thử nghiệm mà không ảnh hưởng đến mã nguồn chính. Khi công việc trên một nhánh hoàn tất, ta có thể gộp nhánh (Merge) với nhánh chính hoặc nhánh khác.

### 3. Git Work-flow

Là một bộ quy trình và quy tắc giúp tổ chức và quản lý cách làm việc với Git trong một dự án. Có nhiều loại Git workflow khác nhau, mỗi loại phù hợp với những tình huống và yêu cầu khác nhau của dự án.

#### Một số git work-flow phổ biến

##### a. Centralized Workflow

Đây là workflow đơn giản nhất và thường được sử dụng bởi các nhóm nhỏ hoặc những người mới bắt đầu với Git.

_Các bước:_
- Clone repository: Mọi người trong nhóm clone repository từ máy chủ trung tâm.
- Làm việc trên nhánh chính: Mọi người làm việc trực tiếp trên nhánh chính (main hoặc master).
- Commit và Push: Mọi thay đổi được commit và push trực tiếp lên máy chủ trung tâm.
- Pull trước khi push: Trước khi push, mỗi người cần pull các thay đổi mới nhất từ repository trung tâm.

##### b. Feature Branch Workflow

Workflow này tạo ra các nhánh cho mỗi tính năng mới, giúp tách biệt các công việc khác nhau và cho phép nhiều người làm việc song song trên các tính năng khác nhau.

_Các bước:_
- Tạo nhánh tính năng mới.
- Làm việc trên nhánh tính năng: Thực hiện các thay đổi và commit trên nhánh tính năng.
- Merge vào nhánh chính: Khi hoàn thành, merge nhánh tính năng vào nhánh develop hoặc main.
- Push thay đổi lên repository trung tâm.

##### c. Git Flow

Git Flow là một workflow phức tạp hơn, phù hợp với các dự án lớn có nhiều giai đoạn phát triển, như phát triển tính năng, chuẩn bị phát hành và sửa lỗi khẩn cấp.

_Các nhánh chính:_
- main: Chứa mã nguồn đã được phát hành.
- develop: Chứa mã nguồn đang được phát triển.
- feature: Dùng để phát triển tính năng mới.
- release: Dùng để chuẩn bị phát hành.
- hotfix: Dùng để sửa lỗi khẩn cấp.

_Các bước:_
- Tạo nhánh tính năng.
- Hoàn thành nhánh tính năng.
- Tạo nhánh phát hành.
- Hoàn thành nhánh phát hành.
- Tạo nhánh sửa lỗi khẩn cấp.
- Hoàn thành nhánh sửa lỗi khẩn cấp.

##### d. Forking Flow

Workflow này thường được sử dụng trong các dự án mã nguồn mở, nơi mà các contributor làm việc trên các fork riêng của repository chính.

_Các bước:_
- fork repository: Contributor fork repository chính vào tài khoản GitHub cá nhân.
- Clone repository đã fork.
- Tạo nhánh tính năng.
- Làm việc trên nhánh tính năng: Thực hiện các thay đổi và commit.
- Push nhánh tính năng lên repository đã fork.
- Tạo pull request: Contributor tạo pull request để yêu cầu merge nhánh tính năng vào repository chính.

##### e. GitHub Flow
GitHub Flow là một workflow đơn giản và linh hoạt, được thiết kế để sử dụng cùng với GitHub.

_Các bước:_
- Tạo nhánh tính năng từ nhánh chính.
- Làm việc trên nhánh tính năng: Thực hiện các thay đổi và commit.
- Push nhánh tính năng lên GitHub.
- Tạo pull request: Tạo pull request để yêu cầu merge nhánh tính năng vào main.
- Code review và merge: Sau khi pull request được review và chấp nhận, merge nhánh tính năng vào main.
- Triển khai: Tự động triển khai hoặc triển khai thủ công các thay đổi từ main.

## _IV. So sánh Git & GitHub & GitLab_

### 1. Điểm giống nhau

Cả 3 khái niệm đều chỉ trình quản lí mã nguồn, cho phép lưu lại lịch sử sửa đổi mã nguồn trên repository.

### 2. Điểm khác nhau

Bảng dưới đây nêu lên đặc điểm của Git, Github và Gitlab trên các phương diện:

|Thuộc tính | Git | Github | Gitlab |
|---------- | --- | ------ | ------ |
|Loại       | VCS | Dịch vụ đám mây | Dịch vụ đám mây + Tự quản lý |
|Giao diện web | Không có | Có | Có |
|Tính năng cộng đồng | Không có | Có | Có |
|CI/CD tích hợp | Không có | Có (pull request, issues) | Có (merge request, issue) |
|Triển khai tự quản lý | Không | Không | Có |


## _V. Các trạng thái có thể có trong Git_
### 1. Untracked

Là trạng thái mà file mới được tạo và chưa được thêm vào Index. 

### 2. Modified

Là trạng thái mà một file đã được thêm vào Index sau đó bị thay đổi nhưng chưa được commit lên Repo.

### 3. Unmodified

Là trạng thái mà file trong working directory không có sự thay đổi hay khác biệt so với phiên bản commit gần nhất.

### 4. Staged


Là trạng thái mà các thay đổi trong file đã được add lên khu vực Index hay Staging Area và sẵn sàng để được commit lên repo.


## _VI. Thao tác với Git_
### 1. Git config

Git config được sử dụng để cấu hình các thiết lập cho Git trên máy tính của bạn. Nó cho phép ta thiết lập và truy xuất các thông tin như tên người dùng, địa chỉ email, cài đặt mặc định cho Git, và nhiều thiết lập khác như remote repository, proxy, credential cache, v.v.

_Ví dụ:_

**Git config user.name**

Hiện tên người dùng hiện tại

![Git-config-username](images/Git-config-username.png)

Thay đổi tên người dùng 

![Git-config-username-change](images/Git-config-username-change.png)

**Git config user.email**

Hiện email của người dùng hiện tại

![Git-config-useremail](images/Git-config-useremail.png)

Thay đổi email người dùng

![Git-config-useremail-change](images/Git-config-useremail-change.png)

**Git config default branch**

![Git-config-default-branch](images/Git-config-default-branch.png)

**Git config --global --list**

Hiện toàn bộ config hiện tại trong bản Git nằm trên máy tính

![Git-config-global-list](images/Git-config-global-list.png)

### 2. Thao tác với Repository

#### a. Git init
Tạo 1 kho lưu trữ mới trên Working directory hiện tại

_Ví dụ:_

![Git-init](images/Git-Init.png)

Working directory ở đây là Test1 và đã có một repo ở đây.
Một thư mục mới có tên là ".git" được sinh ra để lưu trữ các thông tin của git nhưng các commit, người thực hiện, các branch, ...

#### b. Thêm một file lên repo
Ta có một ví dụ sau: 

![Git-new-file](images/Git-new-file.png)

_Giải thích các bước_

- Đầu tiên ta kiểm tra trạng thái của repo hiện tại qua lệnh

```bash
git status
```
Trạng thái của repo hiện ra: Ta đang làm việc trên nhánh main và chưa có commit nào được tạo.

- Ta tạo 1 file Hello.txt với nội dung là Hello.
- Kiểm tra lại trạng thái. Lúc này có hiện 1 dòng Untracked files với Hello.txt có màu đỏ. Điều này nói rằng file Hello.txt đang ở trạng thái Untracked và chưa được đưa lên Index.
- Ta thực hiện lệnh 
```bash
git add Hello.txt
```
Lệnh này đưa file Hello.txt lên Index.

Kiểm tra lại trạng thái, lúc này ta đã thấy dòng chữ Hello.txt đã có màu xanh tức nó đã nằm trên Index và đang chờ được commit.

- Ta thực hiện lệnh 
```bash
git commit -m "1"
``` 
Lúc này các file đang nằm trên index đã được đưa lên Repo và một commit có tên là 1 đã được tạo ra.

#### c. Chỉnh sửa file

Ta có ví dụ sau:

![Git-change-file](images/Git-change-file.png)

_Giải thích về các bước:_
- Đầu tiên ta kiểm tra trạng thái của git: các file đang ở trạng thái unmodified nên không có gì để commit.
- Ta viết thêm chữ "World" vào nội dung của file Hello.txt.
- Kiểm tra lại trạng thái: lúc này file Hello.txt đã bị sửa đổi nhưng chưa được đưa lên Index. Nó ở trạng thái Modified và có chữ đỏ.
- Ta thực hiện lệnh 
```bash
git restore Hello.txt
```
Lệnh này sẽ xóa đi các thay đổi ta đã thực hiện trên file Hello.txt. Kiểm tra lại trạng thái git, file Hello.txt đã ở trạng thái unmodified.
- Ta làm lại thao tác thêm chữ và nội dung của file Hello.txt. Lần này, ta thực hiện thêm lệnh
```bash
git add Hello.txt
```
Lúc này file Hello.txt đã được đưa lên Index. Kiểm tra trạng thái, Hello.txt đang ở trạng thái modified và có chữ xanh.
- Chưa đưa lên commit mà ta thực hiện lệnh
```bash
git restore --staged Hello.txt
``` 
Lệnh này sẽ bỏ đi các thay đổi ta đã thực hiện với file Hello.txt trên Index tức là trên Index không có thông tin về các thay đổi của file này. Kiểm tra trạng thái, Hello.txt đang ở trạng thái modified và có chữ đỏ tức chưa đưa lên Index.
-  Thực hiện lệnh
```bash
git add .
```
Các thay đổi trong working directory đã được đưa lên Index. Tiếp tục thực hiện lệnh:
```bash
git commit -m "2"
```
Thay đổi trên file Hello.txt đã được đưa lên repo. Kiểm tra lịch sử commit bằng lệnh
```bash
git log --oneline
```
"HEAD" trỏ về "main" tức branch hiện tại đang là main. Từ trên xuống dưới là 2 commit 2 và 1.

#### d. Thao tác với branch trong git
Ta có ví dụ sau

![Git-make-branch](images/Git-make-branch.png)

_Giải thích:_
- Đầu tiên ta kiểm tra nhánh bằng lệnh
```bash
git branch
```
Ta thấy main có dấu sao tức lúc này ta đang làm việc với nhánh main.
- Thực hiện lệnh
```bash
git brach test1
```
để tạo nhánh "test1" sau đó thực hiện lệnh
```bash
git switch test1
```
để chuyển nhánh.

Ta thực hiện lệnh 
```bash
git branch -l
```
để in ra các nhánh.

Ta thấy rằng ta đang làm việc với nhánh "test1".

- Ta tạo 1 commit mới là commit "3". Kiểm tra các commit, ta thấy nhánh "main" dừng tại commit "2", nhánh test1 được HEAD chỉ vào chứng tỏ nhánh làm việc đang là "test1" và đồng thời nó đang dừng ở commit "3". Kiểm tra lại nội dun của Hello.txt, ta thấy có số 2 ở dòng cuối cùng.
- Chuyển sang nhánh "main" và kiểm tra lại file Hello.txt, lúc này không còn số 2, tức nội dung của Hello.txt sẽ dựa vào nội dung của file này trong commit gần nhất mà nhánh "main" gắn tới tức là commit "2".

***Lệnh cherry-pick**
Lệnh có cú pháp như sau:

```txt
git cherry-pick [commiy-hash]
```

Lệnh này sẽ giúp ta thêm các commit không thuộc nhánh hiện tại vào nhánh này dựa vào hash-code của chúng. Điều này cho phép ta chọn những commit cần thiết hay hợp lí để đưa vào nhánh.


Có một lưu ý khi dùng cherry-pick đó là khi chọn lọc sẽ xảy ra xung đột giữa những commit thuộc nhánh hiện tại với các commit được thêm. Để có thể tiếp tục cherry-pick, ta cần xử lý xung đột, đưa lên Index bằng lệnh "git add" và sử dụng lệnh.
```bash
git perry-pick --continue
```

***Detached HEAD**

Ta có ví dụ

![Git-detech-head](images/Git-detech-head.png)



_Giải thích:_
- Đầu tiên ta tạo một nhánh mới có tên là "test2" và tạo 1 commit mới là commit "4". Thực hiện kiểm tra bằng lệnh
```bash
git log --oneline
```
Để xem thông tin về các commit hiện tại.
- Tiếp đó ta chuyển nhánh hoạt động sang "test1". Ta xóa nhánh "test2". Lúc này có thông báo xóa thành công nhánh "test2". Bây giờ ta thử chạy tới commit "4" thông qua mã hash của nó qua lệnh
```bash
git checkout
```
Ta kiểm tra thông tin về các commit. Lúc này ta thấy HEAD không trỏ vào bất kì nhánh nào mà trỏ trực tiếp và commit "4". Lúc này git của chúng ta đang ở chế độ Detached HEAD. Trong chế độ này ta vẫn có thể tạo các commit tuy nhiên các commit này sẽ không gắn và bất kì nhánh nào trong repo.

#### e. Merge, rebase, reset

##### e.1. Merge

Lệnh git merge được sử dụng để kết hợp các thay đổi từ một nhánh khác vào nhánh hiện tại. Đây là một trong những thao tác chính trong việc quản lý nhánh (branch) và hợp nhất các thay đổi của nhiều nhánh trong một repository.

_Ví dụ:_

Hiện ta có các nhánh như sau bao gồm:
- main

![main](images/main.png)
- test1

![test1](images/test1.png)
- test2

![test2](images/test2.png)

Giờ ta thực hiện lệnh merge nhánh "main" với nhánh "test1":

![mergetest1](images/mergetest1.png)

như vậy ta đã ghép được nhánh "main" và "test1" lại.

Chuyển sang trường hợp khác ta sẽ merge "test1" với "test2":

![mergetest2](images/mergetest2.png)

Ta nhận được dòng thông báo rằng không tự động merge được. Khác với lần trước, trong lịch sử commit của "test1" có chứa "main", vì vậy mà "test1" có thể được tự động merge với "main". Tuy nhiên khi merge "test2" với "test1" thì vì "test1" không nằm trên lịch sử commit của "test2" nên không thể tự động merge. Để có thể merge 2 nhánh này, ta phải mở file Hello.txt và sửa đổi.

Sau khi sửa đổi file Hello.txt thì ta cần tạo 1 commit khác để merge 2 nhánh này lại là commit "M". Kiểm tra bằng lệnh
```bash
git log --oneline --graph
```
Ta được như sau

![mergetest2-2](images/mergetest2-2.png)

Như vậy ta đã gộp dc nhánh "test2" vào trong nhánh "test1". Chúng giao nhau tại commit "M".

##### e.2. Rebase

Lệnh rebase sẽ biến 1 nhánh thành base của nhánh khác. Base sẽ được tính từ những commit giống nhau của 2 nhánh.

Hãy giả sử ta có các nhánh giống với khi thực hiện merge. Ta chuyển sang làm việc với nhánh main và thực hiện lệnh
```bash
git rebase test1
```

Việc này cũng giống khi thực hiện lệnh merge do nhánh "test1" có chứa "main". 

Thực hiện việc rebase test1 với test2:

![rebasetest2](images/rebasetest2.png)

Thông báo hiện rằng đã có xung đột xảy ra trong file Hello.txt vì vậy ta cần mở ra và thay đổi nó. Giống với merge, ta cũng cần tạo 1 commit mới, ở đây commit mới tên là 3'. Sau khi có commit mới, ta thực hiện lệnh
```bash
git rebase --contimue
```
Kiểm tra lại, ta được test1 có lịch sử commit như sau:

![rebasetest1](images/rebasetest1.png)

##### e.3. Reset

Trong git, reset có thể có những công dụng như sau:
- Loại bỏ file khỏi Index: Nếu 1 file được đưa lên index và chưa được commit, khi thực hiện lệnh reset, nó sẽ bỏ file đó khỏi Index, giống với git restore --staged.
- Đưa HEAD đến 1 commit: khi ta thực hiện lệnh

```txt
git reset [commit-hash]
```
Ta sẽ đưa con trỏ HEAD đến với commit mà có hash-code là "commit-hash". Có nhiều cách để sử dụng reset theo cách này thông qua các option như:
- reset --mixed : là lệnh reset mặc định. Khi sử dụng lệnh này, con trỏ HEAD chỉ được đưa đến commit, xóa cả index nhưng những thay đổi trong working directory thì được giữ lại.
- reset --soft : lệnh này thực hiện sẽ đưa con trỏ đến commit nhưng không xóa thay đổi trong index và working directory.
- reset --hard : lệnh này đưa con trỏ đến commit đồng thời làm thay đổi working directory cũng như index.

#### f. Remote

Git remote là các phiên bản từ xa của repository, thường được lưu trữ trên các máy chủ hoặc dịch vụ lưu trữ mã nguồn như GitHub, GitLab, Bitbucket, hoặc máy chủ nội bộ của công ty. Mỗi remote là một bản sao của repository chứa các nhánh (branches), commit, và các thông tin khác. Remote giúp các nhà phát triển có thể đồng bộ hóa mã nguồn, chia sẻ và tích hợp các thay đổi từ các thành viên khác trong nhóm.

Để thêm một remote vào git local, ta thực hiện lệnh
```txt
git remote add [name] [repo-URL]
```
trong đó [name] là tên mà ta đặt để phân biệt các remote, [repo-URL] là link tới repo mà ta muốn.

***Các thao tác với remote**
- Đổi tên remote: lệnh đổi tên sẽ là
```txt
git remote rename [old-name] [new-name]
```

Lệnh này giúp ta đổi tên một remote có tên là [old-name] sang tên là [new-name]

- Fetch: Lệnh fetch sẽ cho phép ta nhận các thay đổi ở trên remote về với local.
- Pull: là sự kết hợp giữa fetch và merge, nó sẽ giúp ta nhận các thay đổi trên remote và hợp nhất chúng với local.
- Push: lệnh push sẽ đưa các thay đổi ta thực hiện lên bên trên của remote.

#### g. Stash

Khi ta đang thay đổi các thay đổi trên working directory cũng như trên index nhưng ta muốn làm việc với một nhánh khác, ta có thể sử dụng stash. Cú pháp của stash như sau:
```txt
git stash save [name]
```
với name là tên ta muốn để phân biệt các phiên bản stash. Các stash sẽ được đánh số index từ 0 và lưu lại. 

Muốn xem các stash hiện có, ta thực hiện lệnh

```bash
git stash list
```

Khi muốn thay đổi các file trên index và working directory theo một stash, ta thực hiện lệnh
```txt
git stash apply stash@[index]
```

với [index] là chỉ số và stash đó được đánh.

Khi muốn xóa một stash, ta có lệnh
```txt
git stash drop stash@[index]
```

Khi xóa tất cả stash, ta dùng lệnh
```bash
git stash clear
```



