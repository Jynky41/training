# Tìm hiểu về Docker
**_Thực hiện bởi: ĐỖ QUỐC VIỆT_**
## I. Giới thiệu
### A. Khái niệm

Docker là một nền tảng phần mềm cho phép ta dựng, kiểm thử và triển khai ứng dụng một cách nhanh chóng. Docker đóng gói phần mềm vào các đơn vị tiêu chuẩn được gọi là container có mọi thứ mà phần mềm cần để có thể chạy, trong đó có thư viện, công cụ hệ thống, mã và thời gian chạy. Bằng cách sử dụng Docker, ta có thể nhanh chóng triển khai và thay đổi quy mô ứng dụng vào bất kỳ môi trường nào và biết chắc rằng mã của bạn sẽ chạy được.

### B. Cách thức hoạt động của Docker
#### 1. Ảo hóa

Ảo hóa (virtualization) là một công nghệ giúp tạo ra phiên bản ảo của các tài nguyên máy tính, chẳng hạn như phần cứng, hệ điều hành, lưu trữ, hay mạng. Công nghệ này cho phép nhiều hệ thống ảo hoạt động trên một phần cứng vật lý duy nhất, giúp tối ưu hóa việc sử dụng tài nguyên và nâng cao hiệu quả quản lý hệ thống.

Docker là một mã nguồn mở sử dụng các container để triển khai ứng dụng. Khác với việc ảo hóa trực tiếp phần cứng như các máy ảo, container chạy trực tiếp trên kernel của hệ điều hành và chỉ chứa các thành phần cần thiết của ứng dụng, giúp tiết kiệm tài nguyên và tăng tốc độ khởi động.
#### 2. Docker hoạt động như thế nào

Docker hoạt động bằng cách cung cấp phương thức tiêu chuẩn để chạy mã của bạn. Docker là hệ điều hành dành cho container. Cũng tương tự như cách máy ảo ảo hóa (loại bỏ nhu cầu quản lý trực tiếp) phần cứng máy chủ, các container sẽ ảo hóa hệ điều hành của máy chủ. Docker được cài đặt trên từng máy chủ và cung cấp các lệnh đơn giản mà bạn có thể sử dụng để dựng, khởi động hoặc dừng container.

![Docker-hoatdong](images/Docker-hoatdong.png)

### C. Lý do nên sủ dụng Docker

Việc sử dụng Docker cho phép bạn vận chuyển mã nhanh hơn, tiêu chuẩn hóa hoạt động của ứng dụng, di chuyển mã một cách trơn tru và tiết kiệm tiền bằng cách cải thiện khả năng tận dụng tài nguyên. Với Docker, bạn sẽ được nhận một đối tượng duy nhất có khả năng chạy ổn định ở bất kỳ đâu. Cú pháp đơn giản và không phức tạp của Docker sẽ cho bạn quyền kiểm soát hoàn toàn. Việc đưa vào áp dụng rộng rãi nền tảng này đã tạo ra một hệ sinh thái bền vững các công cụ và ứng dụng có thể sử dụng ngay đã sẵn sàng sử dụng với Docker.

Tóm lại, ưu điểm lớn của Docker sẽ là:

1. Vận chuyển ứng dụng nhiều hơn và nhanh hơn.
2. Tiêu chuẩn hóa quá trình vận hành.
3. Di chuyển trơn tru.
4. Tiết kiệm tiền bạc.

## II. Thao tác với Docker
### A. Cài đặt Docker

Có nhiều cách để cài đặt Docker trên máy của bản thân như:
- Cài đặt Docker Desktop theo hệ điều hành.
- Cài đặt Docker Engine từ Docker's apt reposility.
- Cài đặt thủ cồn Docker.

Dưới đây sẽ trình bày cách cài đặt Docker theo Docker engine trên Ubuntu:

#### 1. Cài đặt Docker's apt reposility
Thêm key GPG của Docker:
```bash
apt-get update -y
apt-get install ca-certificates curl -y
install -m 0755 -d /etc/apt/keyrings
```
Thêm kho lưu trữ vào Apt

```bash
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
Cập nhật lại Apt

```bash
apt-get update -y
```

2. Cài đặt các gói Docker
```bash
apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
```

### B. Các lệnh của Docker

#### 1. Docker's Images
Images giống như một khuôn mẫu của các container. Ta có thể lấy một Image từ một reposility hoặc có thể tự tạo ra một Image theo cách mà bản thân muốn.

Khi muốn kiểm tra các image đang có, ta thực hiện lệnh
```bash
docker images
```

Ở đây, chúng ta chưa có một image nào

![Docker-0-images](images/Docker-0-images.png)


##### a. Lấy một images trên Docker Hub

Docker Hub là một nền tảng dịch vụ cung cấp kho lưu trữ và chia sẻ các Docker image. Nó cho phép các nhà phát triển lưu trữ, tìm kiếm và quản lý các container image một cách dễ dàng. Docker Hub đóng vai trò quan trọng trong việc hỗ trợ phát triển, phân phối và triển khai ứng dụng container hóa.

Để lấy một images trên Docker Hub, ta thực hiện lệnh
```txt
docker pull [IMAGE-NAME]
```
[IMAGE-NAME] là tên của image mà ta muốn.

_Ví dụ:_

![Docker-pull](images/Docker-pull.png)

##### b. Build một image

Ta có thể sử dụng các thiết lập được lưu trong một file tên là "Dockerfile" để build một image.

- Đầu tiên ta sẽ tạo một thư mục để lưu trữ các file cần thiết để tạo image và di chuyển đến thư mục này
```bash
mkdir build
cd build
```
- Ta sử dụng trình soạn thảo để tạo một file có tên là "Dockerfile". Trong "Dockerfile" ta sẽ sử dụng các lệnh để thiết lập image. Các syntax ta có thể xem ở trên link [Docker-reference](https://docs.docker.com/reference/dockerfile/). Ở đây, ta có nội dung của "Dockerfile" như sau:

![Dockerfile](images/Dockerfile.png)

_*Lưu ý:_ docker.cow là một file đã được tạo từ trước trong thư mục build.
- Ta sử dụng lệnh sau để build image
```bash
docker image build -t test-whalesay .
```

Tham số "-t" để gắn tag cho image là "test-whalesay". Kiểm tra các images hiện có, ta được:


![Docker-build](images/Docker-build.png)

#### 2. Docker's Containers

- Để tạo một container, ta có thể thực hiện lệnh
```txt
docker run [Tham số] [IMAGE-NAME] [Lệnh]
```

[Tham số] là các thành phần để tùy chọn cho container như thêm tag, xóa ngay khi thoát.
[IMAGE-NAME] là tên của image mà ta muốn tạo container.
[Lệnh] sẽ là lệnh mà ta muốn thực hiện trên container đó.

_Ví dụ:_

![Docker-container](images/Docker-container.png)

- Để kiểm tra các container hiện tại, ta có lệnh
```bash
docker container ls -a
```
hoặc lệnh 
```bash
docker ps -a
```

- Để dừng một container, ta thực hiện lệnh
```bash
docker container stop [CONTAINER-ID]
```
Trong đó [CONTAINER-ID] là id hoặc tên của container mà ta muốn dừng.

- Để khởi động một container, ta có lệnh
```bash
docker container start [CONTAINER-ID]
```
Trong đó [CONTAINER-ID] là id hoặc tên của container.

#### 3. Volumes

Volumes trong Docker là một cơ chế quản lý dữ liệu mà Docker cung cấp để lưu trữ và chia sẻ dữ liệu giữa các container và máy chủ. Volumes cung cấp một cách an toàn và hiệu quả để lưu trữ dữ liệu mà không bị ảnh hưởng khi container bị xóa hoặc thay đổi.

Để tạo một volume, ta dùng lệnh
```txt
docker volume create [VOLUME-NAME]
```
với [VOLUME-NAME] là tên của volume.

Để sử dụng volume với container, ta dùng lệnh
```txt
docker run -v [VOLUME-NAME]:[PATH-IN-CONTAINER] [IMAGE-NAME]
```
với [VOLUME-NAME] là tên volume, [PATH-IN-CONTAINER] là đường dẫn tới file mà ta muốn kết nối với máy chủ, [IMAGE-NAME] là tên của image.