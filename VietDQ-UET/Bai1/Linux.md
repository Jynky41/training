# Linux

## _Lịch sử của linux_

Linux ra đời vào năm 1991 khi Linus Torvalds, một sinh viên tại Đại học Helsinki, Phần Lan, bắt đầu phát triển một hệ điều hành dựa trên kiến trúc Unix. Linus muốn tạo ra một hệ điều hành miễn phí và có thể chạy trên các máy tính cá nhân sử dụng bộ vi xử lý Intel x86. Phiên bản đầu tiên của Linux kernel (0.01) được phát hành vào tháng 9 năm 1991. Linux nhanh chóng thu hút sự quan tâm của cộng đồng lập trình viên trên toàn thế giới, trở thành nền tảng mã nguồn mở mạnh mẽ và phổ biến.

## _Tại sao Linux được ưa chuộng trong ngành công nghệ thông tin_

### 1. Mã nguồn mở

Linux là mã nguồn mở, nghĩa là mọi người có thể xem, sửa đổi và phân phối mã nguồn. Điều này mang lại sự linh hoạt và khả năng tùy chỉnh cao, phù hợp với nhiều nhu cầu khác nhau của các tổ chức và cá nhân.


### 2. Bảo mật
Linux được coi là một trong những hệ điều hành bảo mật nhất. Mô hình quyền truy cập của Unix, cộng với sự kiểm tra liên tục từ cộng đồng mã nguồn mở, giúp phát hiện và sửa chữa các lỗ hổng bảo mật nhanh chóng.


### 3. Hiệu suất và ổn định
Linux nổi tiếng với hiệu suất cao và tính ổn định. Nó có thể chạy mượt mà trên cả những phần cứng cũ và hạn chế, đồng thời có khả năng xử lý tải công việc lớn trên các máy chủ mạnh mẽ.


### 4. Chi phí thấp:
Linux và hầu hết các phần mềm đi kèm đều miễn phí, giúp giảm chi phí bản quyền và hỗ trợ. Điều này đặc biệt hấp dẫn đối với các công ty khởi nghiệp và các tổ chức phi lợi nhuận.


### 5. Hỗ trợ cộng đồng
Cộng đồng Linux rất lớn và hoạt động tích cực, với nhiều diễn đàn, mailing list và nhóm người dùng giúp giải quyết các vấn đề kỹ thuật và cung cấp tài liệu phong phú.


### 6. Đa dạng và tùy biến
Có nhiều bản phân phối (distribution) Linux khác nhau, mỗi bản phục vụ cho những mục đích và đối tượng người dùng khác nhau. Điều này giúp người dùng lựa chọn phiên bản phù hợp nhất với nhu cầu của họ.


### 7. Hỗ trợ phát triển và DevOps
Linux là nền tảng ưa thích cho các nhà phát triển phần mềm và các chuyên gia DevOps. Nó hỗ trợ nhiều ngôn ngữ lập trình, công cụ phát triển và môi trường ảo hóa, làm cho việc phát triển, kiểm thử và triển khai phần mềm trở nên dễ dàng và hiệu quả.


### 8. Hệ sinh thái phần mềm phong phú
Linux có một hệ sinh thái phần mềm phong phú với hàng ngàn ứng dụng và công cụ mã nguồn mở có sẵn, từ các ứng dụng văn phòng, đồ họa, đến các công cụ quản trị hệ thống và phát triển phần mềm.

Những yếu tố này đã khiến Linux trở thành một lựa chọn hấp dẫn và được ưa chuộng trong ngành công nghệ thông tin, từ máy chủ web, hệ thống nhúng, đến các siêu máy tính và các thiết bị di động.


# **Các lệnh cơ bản trong Linux**

## In ra một chuỗi kí tự:

Để in ra một chuỗi kí tự ta có thể sử dụng lệnh echo với cú pháp như sau:
```txt 
echo [STRING]
```
[STRING] là nội dung của chuỗi muốn in ra.

_Ví dụ:_


```sh
echo "Hello"
```
Kết quả: Hello

## Hiển thị danh sách các thư mục và file trong thư mục: 
Lệnh ls cho phép ta xem các file hay thư mục trong một thư mục nào đó theo cú pháp:

```txt
ls [ARGUMENT] [PATH-TO-DIRECTORY]
```

[ARGUMENT] là tham số để tùy chỉnh lệnh theo mong muốn;

[PATH-TO-DIRECTORY] là đường dẫn tới thư mục ta muốn.

_Ví dụ:_

```sh
ls
```
=> Danh sách các thư mục và file trong thư mục hiện tại sẽ hiện ra.

\* _Note:_ Ta có xem một danh sách đầy đủ hơn bao gồm cả các quyền như quyền truy cập hay quyền chỉnh sửa bằng lệnh:
```sh
ls -l
```


## Di chuyển đến 1 thư mục:

Lệnh cd là một lệnh được sử dụng để di chuyển để một thư mục nào đó theo cú pháp

```txt
cd [PATH-TO-DIRECTORY]
```

[PATH-TO-DIRECTORY] là đường dẫn tới thư mục ta muốn tới.

Nếu ta muốn di chuyển đến một thư mục con của thư mục hiện tại, ta không cần đường dẫn của nó mà ta chỉ cần tên của thư mục đó.

_Ví dụ:_

```sh
cd Hello
```
=> Di chuyển đến thư mục Hello

_Các trường hợp đặc biệt_

```sh
cd ..
```
Di chuyển đến thư mục cha

```sh
cd 
```
Di chuyển đến thư mục gốc của user 

## Tạo thư mục, xóa thư mục hay di chuyển file:
### _Tạo thư mục_
Để tạo thư mục ta dùng cú pháp

```txt
mkdir [NAME]
```
[NAME] là tên thư mục con mà ta muốn tạo.

\* _Lưu ý:_ Ta không thể sử dụng lệnh mkdir để tạo một thư mục đã tồn tại trong thư mục gốc.


_Ví dụ:_

Ta muốn tạo một thư mục tên là "Dir1" trong thư mục hiện tại, ta dùng lệnh:

```sh
mkdir Dir1
```
=> Như vậy nếu chưa có thư mục có tên là Dir1 trong thư mục hiện tại, một thư mục Dir1 mới sẽ được tạo.

### _Xóa thư mục và file_

Để xóa một file trong thư mục hiện hành, sử dụng lệnh:

```txt
rm [FILE-NAME]
```

[FILE-NAME] là tên của file muốn xóa.

Để xóa một thư mục, sử dụng lệnh:

```txt
rm -r [DIRECTORY-NAME]
```

[DIRECTORY-NAME] là tên của thư mục muốn xóa.

### _Di chuyển một file_
Lệnh mv cho phép ta di chuyển 1 file trong thư mục hiện tại đến 1 thư mục khác:

```txt
mv [FILE-NAME] [PATH-TO-DIRECTORY]
```

[FILE-NAME] là tên file ta muốn chuyển đi;
[PATH-TO-DIRECTORY] là đường dẫn của thư mục ta muốn chuyển file tới.

_Ví dụ:_

Hiện tại trong thư mục có 1 file "Hello.txt" và 1 thư mục trống tên là "Dir1". Ta muốn di chuyển file "Hello.txt" đến thư mục "Dir1" ta dùng lệnh:

```sh
mv Hello.txt Dir1
```

\* _Bonus:_

Lệnh mv có thể được dùng để thay đổi tên của 1 file hay 1 thư mục theo cú pháp:

```txt
mv [OLD-NAME] [NEW-NAME]
```

[OLD-NAME] là tên hiện tại;
[NEW-NAME] là tên ta muốn đổi.

_Ví dụ:_

Đổi tên thư mục "Dir1" thành thư mục "Dir2" ta dùng lệnh:
```sh
mv Dir1 Dir2
```
## Xem nội dung của một file văn bản

Ta có thể dùng lệnh cat để xem nội dung của 1 file:

```txt
cat [FILE-NAME]
```
[FILE-NAME] là tên của file ta muốn xem.

_Ví dụ:_

Ta có thư mục "Hello.txt" với nội dung:

```txt
Hello World
```

Sử dụng lệnh:

```sh
cat Hello.txt
```

Ta thu được kết quả: Hello World

## Tạo một file văn bản
Có rất nhiều cách để tạo một file văn bản trong Linux, dưới đây là một số cách có thể được dùng để tạo 1 văn bản mới:


### _Sử dụng lệnh cat_

Cú pháp như sau:
```txt
cat > [FILE-NAME] [FILE-CONTENT]
```

[FILE-NAME] là tên của file ta muốn tạo;
[FILE-CONTENT] là nội dung của file.

\* _Lưu ý:_ Khi nhập xong nội dung thì cần nhấn tổ hợp `Ctrl` + `D` để kết thúc.

### _Sử dụng lệnh echo_

Cú pháp như sau: 
```txt
echo [FILE-CONTENT] > [FILE NAME]
```

[FILE-CONTENT] là nội dung file.

[FILE-NAME] là tên file.

### _Sử dụng trình soạn thảo nano_

Cú pháp như sau:

```txt
nano [FILE-NAME]
```

[FILE-NAME] là tên của file.

Khi thực hiện lệnh trên trình soạn thảo sẽ mở ra và ta nhập nội dung ta muốn vào. Sau đó ta nhấn tổ hợp `Ctrl` + `S` để lưu file và `Ctrl` + `X` để thoát. Nếu ta không muốn lưu file, sử dụng `Ctrl` + `X` và sau đó nhấn phím `N`.

\* _Lưu ý:_ Nếu file chưa tồn tại và ta để nội dung của file trống thì file sẽ không được tạo.

### _Sử dụng trình soạn thảo vim_

Cú pháp như sau:

```txt 
vim [FILE-NAME]
```

[FILE-NAME] là tên của file.

Khi thực hiện lệnh trên trình soạn thảo sẽ mở ra. Để nhập nội dung, ta nhấn `I` và nhập nội dung vào. Sau khi nhập xong ta nhấn phím `Esc`. Tiếp tục nhân `:` và `x` để lưu.

\* _Lưu ý:_ Giống với nano, nếu file chưa tồn tại và ta để nội dung của file trống thì file sẽ không được tạo.




    
