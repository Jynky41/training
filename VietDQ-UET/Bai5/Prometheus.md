# Tìm hiểu về Prometheus

**_Thực hiện bởi: ĐỖ QUỐC VIỆT_**

## A. Giới thiệu
### 1. Monitoring
Monitoring: Là quá trình theo dõi và thu thập thông tin về hệ thống, ứng dụng hoặc dịch vụ để đảm bảo chúng hoạt động đúng cách và phát hiện sớm các vấn đề.

Điểm khác nhau so với Logging và Tracing: Monitoring giống như sự kết hợp của Logging và Tracing với mục tiêu xử lí các sự cố trong hệ thống một cách sớm nhất.
### 2. Prometheus
Prometheus là hệ thống giám sát đang được sử dụng trong nhiều hệ sinh thái giám sát của nhiều tổ chức hiện nay. 

Prometheus có một vài điểm đáng lưu ý như sau:
- Thu thập số liệu: Prometheus thu thập và lưu trữ các số liệu thời gian thực trong một cơ sở dữ liệu chuỗi thời gian (time series database). Số liệu có thể được thu thập từ các dịch vụ và máy chủ thông qua HTTP.

- Mô hình dữ liệu: Dữ liệu trong Prometheus được tổ chức thành các chuỗi thời gian với các nhãn (labels) để dễ dàng phân loại và truy vấn. Mỗi chuỗi thời gian bao gồm một cặp giá trị (giá trị số liệu và dấu thời gian).

- Ngôn ngữ truy vấn PromQL: Prometheus có ngôn ngữ truy vấn mạnh mẽ tên là PromQL, cho phép người dùng viết các truy vấn để phân tích và trích xuất dữ liệu từ cơ sở dữ liệu.

- Hệ thống cảnh báo: Prometheus có hệ thống cảnh báo tích hợp cho phép người dùng định nghĩa các quy tắc cảnh báo dựa trên dữ liệu số liệu thu thập được. Các cảnh báo có thể được gửi đến nhiều kênh thông báo khác nhau, như email, Slack, hoặc PagerDuty.

- Kiến trúc Pull: Prometheus sử dụng mô hình "pull" để thu thập số liệu. Các dịch vụ và máy chủ cần phải xuất số liệu qua HTTP endpoint mà Prometheus có thể truy cập và lấy về.

- Tích hợp với các công cụ khác: Prometheus dễ dàng tích hợp với các công cụ giám sát và trực quan hóa khác như Grafana để tạo ra các dashboard hiển thị dữ liệu số liệu.

- Cộng đồng và mở rộng: Prometheus là mã nguồn mở và có cộng đồng phát triển mạnh mẽ, với nhiều thành phần và tích hợp được phát triển bởi cộng đồng.

****Trường hợp nào phù hợp để sử dụng Prometheus***
- Sử dụng khi cần giám sát hệ thống, ứng dụng, và dịch vụ.
- Đặc biệt phù hợp cho môi trường đám mây động.
- Không nên sử dụng khi cần giám sát phân tán hoặc không thể cài đặt các agent trên các máy chủ.
- Không sử dụng cho các dịch vụ yêu cầu độ chính xác 100%.

#### a. Các khái niệm cơ bản
##### a.1. Data model
Prometheus lưu trữ dữ liệu dưới dạng chuỗi thời gian (time series), mỗi chuỗi thời gian được xác định bởi một tên số liệu (metric name) và một tập các nhãn (labels). Nhãn là các cặp khóa-giá trị cho phép phân loại và tổ chức các chuỗi thời gian một cách linh hoạt.

##### a.2. Metric type
- Counter: counter là kiểu dữ liệu chỉ tăng dần hoặc được đưa lại về giá trị 0.
- Gauge: là số liệu có thể tăng hoặc giảm.
- Histogram: thể hiện sự phân phối các giá trị.
- Summary: cũng đo lường sự phân phối của các giá trị và cung cấp các percentiles (phân vị) như P50, P90, P99.

##### a.3. Jobs và Instances

Trong Prometheus, job và instance là hai khái niệm chính để tổ chức và giám sát các mục tiêu (targets).
- Jobs: Job là một tập hợp các instances thực hiện cùng một công việc. Mỗi job đại diện cho một loại dịch vụ hoặc ứng dụng cụ thể.
- Instances: Instance là một thực thể cụ thể của một job. Mỗi instance thường là một máy chủ hoặc một container đang chạy một dịch vụ.

#### b. Kiến trúc của Prometheus Stack

Kiến trúc của Prometheus có thể xem dưới hình dưới đây

![prometheus-stack-architecture](images/architecture2.png)

Trong đó ta sẽ chú ý tới:
- Prometheus Server: Prometheus server định kỳ truy vấn các mục tiêu (targets) để thu thập số liệu.

- Service Discovery: Tự động phát hiện các dịch vụ chạy trong Kubernetes cluster và thêm chúng vào danh sách mục tiêu của Prometheus dựa trên các tệp cấu hình tĩnh để định nghĩa các mục tiêu.

- Prometheus Targets: Các ứng dụng và dịch vụ xuất số liệu mà Prometheus có thể thu thập.

- Pushgateway: Pushgateway sẽ đẩy số liệu khi cần mà không đợi Prometheus thực hiện Pull.
- Prometheus Alerting: Xử lý các cảnh báo từ Prometheus server, quản lý và gửi thông báo cảnh báo đến các kênh khác nhau như Email, PagerDuty, v.v.
- Data Visualization and Export: Giao diện web của Prometheus để truy vấn và trực quan hóa số liệu.


## B. Thao tác với Prometheus
### 1. Cài đặt Prometheus

Để cài đặt Prometheus trên hệ điều hành Ubuntu, ta sử dụng lệnh sau:
```txt
wget [FILE-URL]
```
[FILE-URL] là URL của file prometheus. Ta có thể chọn phiên bản Prometheus mà ta muốn dựa trên các URL được lưu trữ tại [Prometheus-download](https://prometheus.io/download/).

Sau khi đã tải xuống thành công, ta tiến hành giải nén cho file Prometheus. Sau khi giải nén, ta điều hướng đến thư mục Prometheus và sẽ có một file có tên prometheus.yml. File này sẽ cho phép ta cấu hình cho hệ thống về thời gian lấy mẫu, thời gian tính toán, targets, ...

Sau khi đã cấu hình xong, ta chạy Prometheus bằng lệnh:
```bash
./prometheus
```

### 2. Thao tác với cụm Prometheus Stack

Ta sẽ thực hiện một cụm Prometheus để giám sát số ram đang sử dụng thông qua node exporter. Ta tạo ra một thư mục tên "project1" trong đó chứa hai thư mục con tên là "prometheus" và "alertmanager". Thư mục "project1" sẽ chứa file compose.yml để cấu hình các containers với mục đích tạo hai containers, một cho hệ thống giám sát "prometheus" và một cho "alertmanager".

Trong thư mục "prometheus" sẽ chứa file "prometheus.yml" để cấu hình cho hệ thống về các targets. Bên cạnh đó, sẽ có một file "rules.yml" để lưu trữ rules (các đặc điểm mà hệ thống muốn giám sát theo một quy tắc hoặc một công thức).

Trong thư mục "alertmanager" sẽ chứa file "alertmanager.yml" để cấu hình cho Alert Manager các receivers (người nhận cảnh báo) dựa trên các rules. 

Cây thư mục của "project1" sẽ như sau:

![dir-tree](images/dir-tree.png)

Ngoài thư mục này ra, ta sẽ chạy node exporter trên máy cục bộ của ta, không thông qua container nào. Một docker container sẽ chứa grafana.

#### a. Cấu hình Prometheus
##### a.1. Cấu hình rules

File "rules.yml" sẽ có nội dung như sau:

![rules-yml](images/rules-yml.png)

Giải thích các tham số:
- Groups: các nhóm cảnh bảo và được đặt tên thông qua "name" là "Ram usage".
- Rules: liệt kê các rules thuộc về group.
- Record: tên của tham số ta muốn giám sát. Ở đây đặt tham số là "ram_using".
- Expr: công thức cho tham số bên trên. Công thức ở đây được sử dụng là tỉ số giữa hiệu của tổng lượng ram với ram khả dụng với tổng lượng ram.
- Alert: tên cảnh báo. Cảnh báo tên là "High"
- Expr: công thức cho cảnh báo. Cảnh bảo xuất hiện khi "ram_using" lớn hơn 0.5. For: thời gian giám sát (5s).
- Labels: "severity" là loại nhãn ứng với tên là "critical".

##### a.2. Cấu hình prometheus

File "prometheus.yml" có nội dung như sau:

![prometheus-yml](images/prometheus-yml.png)

Giải thích các tham số:
- Scrape_interval: thời gian mỗi lần lấy các số liệu.
- Scrape_configs: cấu hình các targets. Có hai targets là "prometheus" (để giám sát chính nó) và "node_exporter" để giám sát thông qua node_exporter. Hai targets này đều được cấu hình tĩnh thông qua các cổng trên local host.
- Rule_files: file chứa cấu hình rules.
- Alerting: cấu hình của Alert manager. Alert manager sẽ được đặt trên cổng 9093.

#### b. Cấu hình Alert manager

File "alertmanager.yml" được viết như sau:

![alert-manager](images/alertmanager2-yml.png)

Giải thích các tham số:

- Resolve_timeout: chỉ định thời gian mà Alertmanager chờ đợi trước khi coi một cảnh báo đã được giải quyết sau khi nó không còn nhận được từ Prometheus. Ở đây ta đặt là 5s.
- Route: là nơi thiết lập cho nơi nhận cảnh báo. Ở đây bất kì cảnh báo nào cũng sẽ người nhận "default".
- Receivers: thiết lập người nhận. Phần email_configs để cấu hình email cho người nhận. Ở đây người nhận "default" sử dụng SMTP của gmail, mail sẽ được gửi từ "from" đến "to". SMTP của gmail được xác thực bằng tài khoản gmail và mật khẩu ứng dụng gồm 16 kí tực được tạo trước.

#### c. Cấu hình Docker compose

File "compose.yml" được viết như sau:

```txt
services:
    prometheus:
        image: prom/prometheus
        volumes:
            - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
            - ./prometheus/rules.yml:/etc/prometheus/rules.yml
        ports:
            - "9090:9090"
        network_mode: "host"


    grafana:
        image: grafana/grafana
        environment:
            - GF_SECURITY_ADMIN_PASSWORD=admin
        volumes:
            - data:/var/lib/grafana
        ports:
            - "3000:3000"
        network_mode: 'host'

    alertmanager:
        image: prom/alertmanager
        volumes:
            - ./alertmanager/alertmanager.yml:/etc/alertmanager/alertmanager.yml
        ports:
            - "9093:9093"
        network_mode: 'host'
        develop:
        watch:
            - action: sync
              path: ./alertmanager/
              target: /etc/alertmanager/

volumes:
    data:
```

Giải thích các tham số:

- Services: Khai báo các dịch vụ. Ở đây bao gồm "prometheus", "grafana" và "alertmanager".
- Các dịch vụ đều được sử dụng chế độ network là "host" để có thể kết nối với mạng trên máy cục bộ. Các cổng cho "prometheus", "granfana", "alertmanager" lần lượt là 9090, 9100 và 3000. Các cổng ánh xạ tương ứng với các cổng trên máy cục bộ.
- Volumes: "data" được dùng để lưu trữ giá trị cho granfana.

Sau khi đã viết xong cấu hình cho các file,ta điều hướng đến thư mục của dự án và dùng lệnh:
```bash
docker compose up
```

#### d. Kết quả

![prometheus](images/prometheus.png)

Ở đây đã có một cảnh báo. Cảnh báo này đã được gửi tới gmail. 

![alert](images/alert.png)











