# Tìm hiểu Prometheus phần 2
**_Thực hiện bởi: ĐỖ QUỐC VIỆT_**

## I. Các hàm sử dụng
### 1. Rate() và Increase()

Đây là 2 hàm thao tác với kiểu dữ liệu counter. Cụ thể về thuật toán của các hàm này như sau:

Đầu tiên ta sẽ có một counter trong một khoảng thời gian (lấy ví dụ là 5 phút). Nếu trong 5 phút này, counter bị reset, nó sẽ lấy giá trị lấy được lần đầu sau reset rồi cộng với giá trị cuối cùng trước reset để được một chuỗi các giá trị tăng tuyến tính.

![SampleBefore](images/SampleBefore.png)

_Các mẫu trước khi được xử lý_

![SampleLater](images/SampleLater.png)

_Các mẫu sau khi được xử lý_

#### 1.1. Hàm Rate()

Hàm Rate() sẽ tính sự chênh lệch giữa 2 giá trị đầu cuối và khoảng cách thời gian giữa chúng rồi từ đó chia cho nhau. 

_Ví dụ:_
Giá trị đầu là 300 tại 5.2 giây, giá trị cuối là 570 tại 9.7 phút. Như vậy chênh lệch giá trị sẽ là 270 và khoảng cách thời gian sẽ là 4.5 phút. Như vậy rate() sẽ cho kết quả 270 / 4.5 = 60 mẫu/phút.

#### 1.2. Increase()

Hàm Increase() sẽ được tính bằng hàm Rate() nhân với 1 khoảng cách thời gian mới. Khoảng cách thời gian này sẽ được tạo ra bằng cách di chuyển giá trị đầu hoặc giá trị cuối đến biên của khoảng thời gian mà ta xem xét ban đầu. Giả sử thời gian xem xét là từ 4.5 phút cho đến 9.5 phút, giá trị lần đầu xuất hiện tại 4.65 và giá trị cuối là tại 9.15. Các giá trị này có hàm Rate() là 60. Khoảng cách giá trị đầu cuối là 4.5 và khoảng cách giá trị mà hàm Increase() dùng để tính toán là 4.8 (từ giá trị đầu cuối mở rộng ra 2 bên 0.15). Như vậy hàm Increase() sẽ được tính là 60 x 4.8 = 288.

### 2. Hàm Irate()

Nếu như targets của ta bị reset, hàm Irate cũng sẽ có cơ chế xử lý mẫu giống với 2 hàm trên đó là cộng giá trị đầu sau reset với giá trị đầu reset. Tuy nhiên hàm Irate sẽ chỉ quan tâm đến 2 giá trị cuối cùng trong khoảng thời gian mà ta xét và sau đó tính toán như hàm Rate.

![Irate-works](images/irate-works.png)

### 3. Hàm Holt_winters()

Hàm holt_winters() sẽ áp dụng thuật toán holt winters cho một véc tơ kiểu Gauge trong khoảng thời gian delta(t) với 2 chỉ số là sf - tượng trưng cho mức độ làm mịn - và tf - tượng trưng cho các xu thế. 2 chỉ số sf và tf sẽ có giá trị trong khoảng (0,1).

Nói về thuật toán holt winters, đây là thuật toán dự đoán các giá trị trong tương lai dựa vào các giá trị trong quá khứ. Các giá trị được thu nhận trong quá khứ sẽ được sử dụng để tính toán các thành phần là trend và seasonal dựa trên các chỉ số tf và sf. Các thành phần này lại kết hợp với các chỉ số tf và sf để dự đoán các giá trị trong tương lai. 

Trong Premetheus, người ta thường sử dụng holt_winters() như là một cách để đánh giá các giá trị đã thu thập được trong một khoảng thời gian.

_Ví dụ:_

![raw](images/raw.png)
_Giá trị gốc_


![holt_winters](images/holt_winters.png)
_Giá trị sử dụng holt_winters với sf và tf là 0.1_

### 4. Hàm Label_join()

Hàm Label_join() sẽ nhóm các nhãn lại với nhau để tạo một nhãn mới. Label_join() được sử dụng trong file cấu hình của Prometheus để ta có thể theo dõi một cách thuận tiện hơn dựa trên nhãn đã tạo ra.

_Ví dụ:_

*Trước khi sử dụng label_join():

![label_join_before](images/label_join_before.png)

Ở đây ta tìm record tên là hello thì sẽ không có query nào.

*Sau khi sử dụng label_join():

- Đầu tiên ta sẽ phải thêm rules vào trong file rules của prometheus:

![label_join_rules](images/label_join_rules.png)

Ta tạo 1 record tên là "hello" và label_join để nối các label là "instance" và "job" lại, tạo thành "label1". Các label được chia cách nhau bởi dấu "-". Khi tìm kiếm query, ta sẽ gắn label1 = "a-b" với a là giá trị của instance và b là giá trị của job.
- Kết quả tìm kiếm sẽ như sau:

![label_join_after](images/label_join_after.png)

Lúc này ta có thể tìm kiếm record hello dựa trên label1.

### 5. Hàm Label_replace()

Hàm label_replace() cho phép ta tạo ra nhãn dán mới từ nhãn dán đã có dựa trên biểu thức chính quy và giá trị thay thế. Với các giá trị trong nhãn gốc mà khớp với biểu thức chính quy thì sẽ tạo ra một giá trị thuộc nhãn dán mới thỏa mãn biểu thức của giá trị thay thế.

_Ví dụ:_

Ta thiết lập biểu thức trong file rules.

![label_replace_rules](images/label_replace_rules.png)

Ở đây ta sẽ áp dụng label_replace() lên vector "node_memory_MemTotal_bytes", tạo ra nhãn "label2" dựa trên nhãn gốc là "instance". Tất cả giá trị nào thỏa mãn có dấu ":" trong nhãn "instacne" sẽ tạo ra giá trị cho nhãn mới là giá trị đứng trước dấu ":". Ví dụ nếu instance = "localhost:9090" thì label2 = "localhost".

Kết quả:

![label_replace](images/label_replace.png)

### 6. Hàm Predict_linear()

Hàm Predict_linear() sẽ dự đoán các giá trị của vector trong khoảng thời gian t giây sắp tới dựa trên thuật toán "Simple Linear Regression" (hồi quy tuyến tính đơn giản).

Hàm này chỉ được áp dụng cho dữ liệu kiểu Gauges. Hàm này sẽ cần vector có ít nhất 2 giá trị. Nếu có giá trị hướng tới vô cực trong vector đó trong khoảng thời gian mà ta xét thì hàm này sẽ trả về giá trị NaN.

Hàm này sẽ trả giá trị tmax giây là thời gian xét vector, dù t ta muốn có lớn hơn tmax.

_Ví dụ:_

Instant vector:
![predict_linear_instant_vector](images/predict_linear_instant_vector.png)

Giá trị của Predict_linear() với vector trong range 10 phút và t = 600 giây:

![predict_linear_result](images/predict_linear_result.png)

## II. Các cách matching vector
Matching các vector sẽ là không thể nếu chúng có các nhãn khác nhau. Để có thể matching chúng, ta sẽ cần những keyword.
Thông thường matching 2 vector sẽ có dạng:

```txt
vector1 <operator> vector2
```

### 1. Ignoring

Ignoring sẽ bỏ qua các nhãn bên trong nó để matching 2 vector.

_Ví dụ:_
Có các vector với giá trị như sau:

```txt
method_code:http_errors:rate5m{method="get", code="500"}  24
method_code:http_errors:rate5m{method="get", code="404"}  30
method_code:http_errors:rate5m{method="put", code="501"}  3
method_code:http_errors:rate5m{method="post", code="500"} 6
method_code:http_errors:rate5m{method="post", code="404"} 21

method:http_requests:rate5m{method="get"}  600
method:http_requests:rate5m{method="del"}  34
method:http_requests:rate5m{method="post"} 120
```

Thực hiện phép toán: 
```txt
method_code:http_errors:rate5m{code="500"} / ignoring(code) method:http_requests:rate5m
```

Ta sẽ được kết quả:
```txt
{method="get"}  0.04            //  24 / 600
{method="post"} 0.05            //   6 / 120
```

Giải thích:

Ở vector bên phải nó có chứa nhãn "method" và "code" nhưng ở vector bên trái chỉ có nhãn "method". Để có thể nối chúng với nhau, ta sẽ cần bỏ qua nhãn là nhãn "code". Bên trên vector bên phải ta có ghi {code="500"}, điều này là để với mỗi giá trị nhãn "method" thì có 1 giá trị của vector, như vậy mới có thể matching được (đây là kiểu matching one-to-one vectors).


### 2. On

On sẽ matching 2 vector dựa trên các nhãn bên trong nó.

_Ví dụ:_

Lấy ví dụ bên trên, nếu ta sử dụng phép toán:

```txt
method_code:http_errors:rate5m{code="500"} / on(method) method:http_requests:rate5m
```

Ta cũng sẽ thu được kết quả:

```txt
{method="get"}  0.04            //  24 / 600
{method="post"} 0.05            //   6 / 120
```

### 3. Group_left và Group_right

2 từ khóa này sẽ được gán đằng sau của Ignoring hoặc On khi thực hiện matching vectors theo kiểu many-to-one hoặc one-to-many. Group_left hoặc Group_right được sử dụng dựa vào bên nào có số lượng nhiều hơn.

_Ví dụ:_

Ta lấy ví dụ trên phần Ignoring, ta thấy "method_code:http_errors:rate5m" có nhiều nhãn hơn. Khi ta thực hiện phép toán:

```txt
method_code:http_errors:rate5m / ignoring(code) group_left method:http_requests:rate5m
```

Kết quả thu được sẽ là

```txt
{method="get", code="500"}  0.04            //  24 / 600
{method="get", code="404"}  0.05            //  30 / 600
{method="post", code="500"} 0.05            //   6 / 120
{method="post", code="404"} 0.175           //  21 / 120
```

## III. Giải thích ý nghĩa các bảng giám sát của Node_exporter

### 1. CPU Basic
Bảng giám sát CPU Basic hiển thị các số liệu cơ bản về hiệu suất của CPU.

#### 1.1. CPU Usage (Sử dụng CPU):

"node_cpu_seconds_total": thời gian mà mỗi CPU dành cho từng mode.
"node_cpu_guest_seconds_total": thời gian mà mỗi CPU dành cho các máy ảo cho từng mode.

*Các mode:
- user: Thời gian CPU dành cho các tiến trình của người dùng (không phải của hệ điều hành).
- system: Thời gian CPU dành cho các tiến trình của hệ thống (kernel).
- idle: Thời gian CPU không hoạt động (không có tiến trình nào chạy).
- iowait: Thời gian CPU chờ đợi I/O (chờ đợi các hoạt động đọc/ghi đĩa hoặc mạng).

#### 1.2. CPU Load (Tải CPU):

Load Average: Tải trung bình của hệ thống trong khoảng thời gian 1 phút, 5 phút và 15 phút. Đây là một chỉ số về số lượng tiến trình đang chờ được xử lý hoặc đang chạy. Giá trị càng cao, hệ thống càng bận rộn.

Để xem giá trị tải, có các query là "node_load1", "node_load5", "node_load15", lần lượt cho việc xem tải trung bình trong thời gian 1 phút, 5 phút, 15 phút.

#### 1.3. Context Switches


Context Switch Rate: Số lượng chuyển đổi ngữ cảnh (context switch) mỗi giây. Chuyển đổi ngữ cảnh xảy ra khi CPU chuyển từ một tiến trình này sang tiến trình khác. Số lượng chuyển đổi ngữ cảnh cao có thể chỉ ra rằng hệ thống đang xử lý nhiều tiến trình cùng lúc.

Để xem giá trị này, ta sử dụng query "node_context_switches_total"

### 2. Memory Basic

Bảng giám sát Memory Basic hiển thị các số liệu cơ bản về việc sử dụng bộ nhớ của hệ thống.

#### 2.1. Tổng bộ nhớ (Total Memory)

Thể hiện tổng dung lượng bộ nhớ vật lý (RAM) của hệ thống thông qua query "node_memory_MemTotal_bytes"

#### 2.2. Bộ nhớ đã sử dụng (Used Memory)

Thể hiện bộ nhớ đã được sử dụng bởi các tiến trình và hệ thống. Bộ nhớ đã sử dụng có thể được tính bằng cách lấy tổng bộ nhớ trừ đi bộ nhớ trống, bộ nhớ buffer và bộ nhớ cache.

Xem giá trị này, ta dùng query "node_memory_MemTotal_bytes - node_memory_MemFree_bytes - node_memory_Buffers_bytes - node_memory_Cached_bytes"

#### 2.3. Bộ nhớ trống (Free Memory)

Thể hiện bộ nhớ vật lý chưa được sử dụng. Query thể hiện là node_memory_MemFree_bytes.

#### 2.4. Bộ nhớ buffer và cache (Buffers,cached)

- Buffers: Bộ nhớ được sử dụng bởi kernel để lưu trữ dữ liệu tạm thời trong quá trình I/O. Query là "node_memory_Buffers_bytes".
- Cached: Bộ nhớ được sử dụng để lưu trữ các trang nhớ được đọc từ đĩa để tăng tốc độ truy cập dữ liệu. Query là "node_memory_Cached_bytes".

#### 2.5. Bộ nhớ Swap (Swap memory)

- Total Swap: Tổng dung lượng không gian swap. Query tượng trưng cho giá trị này là "node_memory_SwapTotal_bytes".
- Used Swap: Dung lượng swap đang được sử dụng. Query tượng trưng cho giá trị này là "node_memory_SwapTotal_bytes - node_memory_SwapFree_bytes".
- Free Swap: Dung lượng swap còn trống. Query tượng trưng cho giá trị này là "node_memory_SwapFree_bytes".

#### 2.6. Sử dụng bộ nhớ (Memory Utilization)

- Active Memory: Bộ nhớ đang được sử dụng một cách tích cực bởi các tiến trình. Query tượng trưng cho giá trị này là "node_memory_Active_bytes".

- Inactive Memory: Bộ nhớ đã được sử dụng nhưng hiện không còn được sử dụng tích cực. Query tượng trưng cho giá trị này là "node_memory_Inactive_bytes".

- Bộ nhớ khả dụng (Available Memory): Bộ nhớ khả dụng là bộ nhớ thực sự có sẵn cho các ứng dụng mới mà không cần phải đẩy bất kỳ nội dung nào của cache/buffer ra ngoài. Query tượng trưng cho giá trị này là "node_memory_MemAvailable_bytes".

=> Sử dụng bộ nhớ có thể được tính theo phần trăm như sau: 
((node_memory_MemTotal_bytes - node_memory_MemFree_bytes - node_memory_Buffers_bytes - node_memory_Cached_bytes) / node_memory_MemTotal_bytes) * 100.

#### *BẢNG QUERY

**CPU BASIC**

| Giá trị         | Query                              |
|-----------------|------------------------------------|
| CPU Usage - User| node_cpu_seconds_total{mode="user"}|
| CPU Usage - System| node_cpu_seconds_total{mode="system"}|
| CPU Usage - Idle|node_cpu_seconds_total{mode="idle"}|
| CPU Usage - I/O Wait| node_cpu_seconds_total{mode="iowait"|
| Load Average (1m)| node_load1|
| Load Average (5m)| node_load5|
| Load Average (15m)|node_load15|
| Context Switches | node_context_switches_total[5m]|

**MEMORY BASIC**

|Giá trị|Query|
|------|------|
|Total Memory| node_memory_MemTotal_bytes|
|Used Memory| node_memory_MemTotal_bytes - node_memory_MemFree_bytes - node_memory_Buffers_bytes - node_memory_Cached_bytes|
|Free Memory| node_memory_MemFree_bytes|
|Buffers|node_memory_Buffers_bytes|
|Cached|node_memory_Cached_bytes|
|Total Swap| node_memory_SwapTotal_bytes|
|Used Swap|node_memory_SwapTotal_bytes - node_memory_SwapFree_bytes|
|Free Swap|node_memory_SwapFree_bytes|
|Active Memory| node_memory_Active_bytes|
|Inactive Memory| node_memory_Inactive_bytes|
|Available Memory| node_memory_MemAvailable_bytes|
|Memory Utilization|node_memory_MemTotal_bytes - node_memory_MemFree_bytes - node_memory_Buffers_bytes - node_memory_Cached_bytes) / node_memory_MemTotal_bytes) * 100|

## IV. Xây dưng giám sát bất thường 

Công thức giám sát: So sánh lượng tăng trong vòng 5 phút tại thời điểm hiện tại với lượng tăng trong vòng 5 phút tại 5 phút trước của tổng lượng tăng thời gian sử dụng cpu của tất cả các mode. Sử dụng phép chia giữa 2 giá trị này. Nếu kết quả lớn hơn 2 thì sẽ sinh ra cảnh báo.

Cấu hình file rules:

```yaml
- name: 'CPU up'
    rules:
    - record: cpu_up
      expr: sum by (instance) (increase(node_cpu_seconds_total[5m])) / sum by (instance) (increase(node_cpu_seconds_total[5m] offset 5m))
    - alert: High CPU
      expr: cpu_up > 2
      for: 5s
      labels:
        severity: high
```

Kết quả:

![cpu_up](images/cpu_up.png)

## V. Tìm hiểu các regex

### 1. Regex với chuỗi:

Có một vài regex như là:
- ^String.*: Bắt đầu là String.
- .*String: Có chứa String.
- .*String$: Kết thúc là String.
- (.\*)String(.\*): Chuỗi String nằm giữa.

### 2. Regex khi lọc theo nhãn:

Có một vài regex như là: 
- = "String": nhãn có giá trị là "String".
- != "String": nhãn có các giá trị khác "String".
- =~ "ListOfString": nhãn có các giá trị nằm trong "ListOfString".
- !~ "ListOfString": nhãn có các giá trị không nằm trong "ListOfString".

_Ví dụ:_

Tìm kiếm các instance bắt đầu bằng local

```txt
{instance =~ "^local.*"}
```

Tìm kiếm các job không kết thúc là search

```txt
{job !~ ".*search$"}
```

Tìm kiếm instance có giá trị là 192.168.33.10:9100

```txt
{instance = "192.168.33.10:9100"}
```

Tìm kiếm các job không có giá trị là node_exporter

```txt
{job != "node_exporter"}
```

